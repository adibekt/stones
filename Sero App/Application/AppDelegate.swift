//
//  AppDelegate.swift
//  Sero App
//
//  Created by Roman Nekliukov on 8.11.21.
//

import Foundation
import Firebase
import GoogleSignIn
import UIKit
import AVFoundation

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                     launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        do {
            try AVAudioSession.sharedInstance().setCategory(
                AVAudioSession.Category.playback,
                mode: AVAudioSession.Mode.default,
                options: [
                    AVAudioSession.CategoryOptions.duckOthers
                ]
            )
        } catch {
            print("Failed to set audio session category.")
        }

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        MainModel.shared().completeLatestSet()
        MainModel.shared().completeLatestInterval()
    }
}

extension AppDelegate {
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        var handled: Bool

        handled = GIDSignIn.sharedInstance.handle(url)
        if handled {
            return true
        }

        return false
    }
}
