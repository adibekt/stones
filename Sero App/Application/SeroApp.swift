//
//  SeroApp.swift
//  Sero App
//
//  Created by Таир Адибек on 02.11.2021.
//

import SwiftUI

@main
struct SeroApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @StateObject var appState = AppState()
    @StateObject var clientState = ClientState()
    @StateObject var therapistState = TherapistOptionState()

    let router = MainRouter(factory: MainFactory())

    var body: some Scene {
        WindowGroup {
            NavigationView {
                ZStack {
                    router.toAuthView().modifier(NoNavbarModifier())

                    NavigationLink(destination: getViewByRole().modifier(NoNavbarModifier()),
                                   isActive: $appState.isAuthorized) {
                    }.isDetailLink(false)
                }
            }
            .navigationViewStyle(.stack)
            .accentColor(Color(Asset.Colors.darkGreen.name))
            .environmentObject(appState)
            .environmentObject(clientState)
            .environmentObject(therapistState)
            .onReceive(appState.$isAuthorized, perform: { value in
                print("is authorized state changed = \(value)")
            })
            .onAppear(perform: {
                router.mainFactory.isUserAuthorized { result in
                    appState.isAuthorized = result
                }
            })
        }
    }

    private func getViewByRole() -> AnyView {
        switch AppSettings.getRole() {
        case .therapist:
            return AnyView(router.toTherapistOptionScreen())
        case .client:
            return AnyView(router.toRequirementsView())
        case .none:
            return AnyView(router.toRoleScreen())
        }
    }
}

class TherapistOptionState: ObservableObject {
    @Published var option: TherapistOption?
}

class AppState: ObservableObject {
    @Published var isAuthorized: Bool = AppSettings.authorizationType != .none
    @Published var roleSelected: Bool = AppSettings.getRole() != .none
}

class ClientState: ObservableObject {
    @Published var enterConnectionMode: Bool = false
}

enum AuthorizationState {
    case none
    case authenticated
    case roleSelected
}
