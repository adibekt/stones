//
//  AppSettings.swift
//  Sero App
//
//  Created by Roman Nekliukov on 9.11.21.
//

import Foundation

struct AppSettings {
    @UserDefaultsWrapper(key: AppDataKeys.authorizationType.rawValue, defaultValue: nil)
    static var authorizationType: AuthorizationType?

    @UserDefaultsWrapper(key: AppDataKeys.isAppFirstLaunch.rawValue, defaultValue: true)
    static var isAppFirstLaunch: Bool

    @UserDefaultsWrapper(key: AppDataKeys.currentUser.rawValue, defaultValue: nil)
    static var currentUser: User?

    @UserDefaultsWrapper(key: AppDataKeys.lastTherapy.rawValue, defaultValue: nil)
    static var lastTherapy: Data?
}

enum AppDataKeys: String {
    case authorizationType
    case isAppFirstLaunch
    case lastTherapy
    case currentUser
}
