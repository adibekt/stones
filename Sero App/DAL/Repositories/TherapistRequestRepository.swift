//
//  TherapistRequestRepository.swift
//  Sero App
//
//  Created by Roman Nekliukov on 25.12.21.
//

import Foundation
import FirebaseFirestore

protocol ITherapistRequestRepository {
    func createRequest(connectionRequest: TherapistConnectionRequest, completed: StringClosure?, failed: StringClosure?)
    func listenForConnection(accessCode: String, completed: UserClosure?, failed: StringClosure?)
    func listenForAcceptance(code: String, accepted: ConnectionResultClosure?,
                             dismissed: VoidClosure?,
                             failed: StringClosure?)
    func stopListening()
    func dismissTherapist(code: String, completed: VoidClosure?, failed: StringClosure?)
    func confirmFromTherapist(therapistId: String, code: String, completed: VoidClosure?, failed: StringClosure?)
    func confirmFromPatient(patientId: String, code: String, therapyID: String,
                            completed: VoidClosure?, failed: StringClosure?)
    func clearAccessCodes(patientId: String, completion: @escaping VoidClosure)
}

final class TherapistRequestRepository: ITherapistRequestRepository {
    private let collectionName = "therapistConnectionRequest"
    private let accessCodeField = "accessCode"
    private let patientIdField = "patientId"
    private let therapySessionIdField = "therapySessionId"
    private let therapistIdField = "therapistId"
    private let expiresAtField = "expiresAt"

    private let firestoreDb = Firestore.firestore()
    private let usersRepository = UsersRepository()

    private var snapshotListener: ListenerRegistration?

    func createRequest(connectionRequest: TherapistConnectionRequest,
                       completed: StringClosure?,
                       failed: StringClosure?) {
        self.clearAccessCodes(patientId: connectionRequest.patientId) {
            self.createAccessCode { [weak self] accessCode in
                guard let self = self else {
                    return
                }

                self.firestoreDb.collection(self.collectionName).document(accessCode).setData([
                    self.accessCodeField: accessCode,
                    self.patientIdField: connectionRequest.patientId,
                    self.therapySessionIdField: connectionRequest.therapySessionId,
                    self.therapistIdField: connectionRequest.therapistId,
                    self.expiresAtField: connectionRequest.expiresAt
                ]) { error in
                    if let error = error {
                        print("Error adding document: \(error.localizedDescription)")
                        failed?(error.localizedDescription)
                    } else {
                        print("Document added with access code: \(accessCode)")
                        completed?(accessCode)
                    }
                }
            }
        }
    }

    func confirmFromTherapist(therapistId: String, code: String, completed: VoidClosure?, failed: StringClosure?) {
        firestoreDb.collection(collectionName).document(code).updateData([
            therapistIdField: therapistId
        ]) { error in
            if let error = error {
                print("Error updating document: \(error.localizedDescription)")
                failed?(error.localizedDescription)
            } else {
                print("Document with ID: \(code) successfully updated")
                completed?()
            }
        }
    }

    func confirmFromPatient(patientId: String, code: String, therapyID: String,
                            completed: VoidClosure?, failed: StringClosure?) {
        firestoreDb.collection(collectionName).document(code).updateData([
            patientIdField: patientId,
            therapySessionIdField: therapyID
        ]) { error in
            if let error = error {
                print("Error updating document: \(error.localizedDescription)")
                failed?(error.localizedDescription)
            } else {
                print("Document with ID: \(code) successfully updated")
                completed?()
            }
        }
    }

    func listenForConnection(accessCode: String, completed: UserClosure?, failed: StringClosure?) {
        snapshotListener = firestoreDb.collection(collectionName)
            .whereField(accessCodeField, isEqualTo: accessCode)
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots. \(error?.localizedDescription ?? "")")
                    return
                }

                guard let userData = snapshot.documents.first?.data(),
                      let therapistId = userData[self.therapistIdField] as? String else {
                    failed?("Therapist Id not found")
                    return
                }

                if !therapistId.isEmpty {
                    self.usersRepository.getUser(id: therapistId) { user in
                        completed?(user)
                    }
                }
        }
    }

    func listenForAcceptance(code: String, accepted: ConnectionResultClosure?,
                             dismissed: VoidClosure?, failed: StringClosure?) {
        snapshotListener = firestoreDb.collection(collectionName)
            .whereField(accessCodeField, isEqualTo: code)
            .addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots. \(error?.localizedDescription ?? "")")
                    return
                }

                guard let userData = snapshot.documents.first?.data(),
                      let patientId = userData[self.patientIdField] as? String,
                      let therapistId = userData[self.therapistIdField] as? String,
                      let therapyId = userData[self.therapySessionIdField] as? String else {
                    failed?("Patient Id not found")
                    return
                }

                if !therapyId.isEmpty && !patientId.isEmpty {
                    print("Client accepted therapist request")
                    accepted?((therapyID: therapyId, patientID: patientId))
                }

                if therapistId.isEmpty {
                    print("Client dismissed therapist request")
                    dismissed?()
                }
        }
    }

    func stopListening() {
        guard let snapshotListener = snapshotListener else {
            return
        }

        snapshotListener.remove()
    }

    func dismissTherapist(code: String, completed: VoidClosure?, failed: StringClosure?) {
        firestoreDb.collection(collectionName).document(code).updateData([
            therapistIdField: ""
        ]) { error in
            if let error = error {
                print("Error updating document: \(error.localizedDescription)")
                failed?("")
            } else {
                print("Document with ID: \(code) successfully updated")
                completed?()
            }
        }
    }

    public func clearAccessCodes(patientId: String, completion: @escaping VoidClosure) {
        firestoreDb
            .collection(collectionName)
            .whereField(patientIdField, isEqualTo: patientId)
            .getDocuments { snapshot, _ in
                guard let documents = snapshot?.documents else {
                    print("Unable to cleanup existing codes for user with ID = \(patientId)")
                    return
                }

                documents.forEach {
                    $0.reference.delete()
                    print("Room entity with access code \($0.documentID) successfully deleted.")
                }

                completion()
        }
    }

    private func createAccessCode(completion: @escaping StringClosure) {
        let codeDigitsNumber = 4
        let minValue = 1
        let maxValue = 9999

        firestoreDb.collection(collectionName).getDocuments { snapshot, _ in
            let range = Array(minValue...maxValue)

            guard let ids = snapshot?.documents.map({ $0.documentID }).compactMap({ Int($0) }),
                  let uniqueCode = range.filter({ !ids.contains($0) }).randomElement() else {
                return
            }

            completion(String(withInt: uniqueCode, leadingZeros: codeDigitsNumber))
        }
    }
}
