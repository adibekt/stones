//
//  UsersRepository.swift
//  Sero App
//
//  Created by Roman Nekliukov on 18.11.21.
//

import Foundation
import FirebaseStorage
import FirebaseFirestore
import FirebaseFirestoreSwift

protocol IUsersRepository {
    func getUser(email: String, provider: String, completion: @escaping UserClosure)
    func getUser(id: String, completion: @escaping UserClosure)
    func createUser(user: User, completion: @escaping ResultClosure)
    func updateUser(user: UserUpdate, completed: VoidClosure?, failed: StringClosure?)
}

final class UsersRepository: IUsersRepository {
    private let collectionName = "users"
    private let emailField = "email"
    private let firstNameField = "firstName"
    private let lastNameField = "lastName"
    private let phoneNumberField = "phoneNumber"
    private let imageUrlField = "imageUrl"
    private let providerField = "provider"
    private let userRoleField = "role"

    private let firestoreDb = Firestore.firestore()

    func createUser(user: User, completion: @escaping ResultClosure) {
        do {
            guard let userId = user.id else {
                print("User id not found.")
                completion(.failure(.signupFailed))
                return
            }

            try firestoreDb.collection(collectionName).document(userId)
                .setData(from: user) { error in
                    if let error = error {
                        print("Error adding document: \(error.localizedDescription)")
                        completion(.failure(.signupFailed))
                        return
                    }

                    print("Document added with ID: \(userId)")
                    completion(.success(()))
                }
        } catch {
            completion(.failure(.signupFailed))
        }
    }

    func updateUser(user: UserUpdate, completed: VoidClosure?, failed: StringClosure?) {
        firestoreDb.collection(collectionName).document(user.id).updateData([
            userRoleField: user.role.rawValue
        ]) { error in
            if let error = error {
                print("Error updating document: \(error)")
                failed?("")
            } else {
                print("Document with ID: \(user.id) successfully updated")
                completed?()
            }
        }
    }

    func getUser(id: String, completion: @escaping UserClosure) {
        firestoreDb
            .collection(collectionName)
            .document(id)
            .getDocument { (querySnapshot, error) in
                do {
                    let userDocument = try querySnapshot?.data(
                        as: User.self,
                        decoder: Firestore.Decoder.init())
                    completion(userDocument)
                } catch {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }
    }

    func getUser(email: String, provider: String, completion: @escaping UserClosure) {
        firestoreDb
            .collection(collectionName)
            .whereField(emailField, isEqualTo: email.lowercased())
            .whereField(providerField, isEqualTo: provider.lowercased())
            .getDocuments { (querySnapshot, error) in
                do {
                    let document = querySnapshot?.documents.first
                    let userDocument = try document?.data(
                        as: User.self,
                        decoder: Firestore.Decoder.init())
                    userDocument?.id = document?.documentID
                    completion(userDocument)
                } catch {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }
    }
}
