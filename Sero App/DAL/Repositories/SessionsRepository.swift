//
//  FirebaseService.swift
//  Sero App
//
//  Created by Таир Адибек on 22.11.2021.
//

import Foundation
import Firebase
import FirebaseFirestore
import RxRelay

class SessionsRepository {
    let therapySessionCollection = "therapySessions"
    let usersCollection = "users"
    let dataBase = Firestore.firestore()
    static var share = SessionsRepository()

    static func shared() -> SessionsRepository {
        return share
    }

    func addDocument<T: Encodable>(with path: String,
                                   document: T,
                                   completion:
                                   @escaping (Result<DocumentReference, AppError>) -> Void) {
        do {
            var ref: DocumentReference?
            ref = try dataBase.collection(path)
                .addDocument(from: document) { error in
                    guard let reference = ref else {
                        completion(.failure(.addFailed))
                        print(error?.localizedDescription ?? "")
                        return
                    }
                    completion(.success(reference))
                }
        } catch {
            completion(.failure(.addFailed))
        }
    }

    func updateDocument(with path: String, newValues: [String: Any]) {
        var ref: DocumentReference?
        ref = dataBase
            .document(path)
        ref?.updateData(newValues)
    }

    func setUpSession(_ therapySession: TherapySession) {
        guard let userID = AppSettings.currentUser?.id else {
            return
        }
        do {
            try dataBase.collection(usersCollection + "/\(userID)/" + therapySessionCollection)
            .document(therapySession.documentID).setData(from: therapySession)
        } catch {
            print(error.localizedDescription)
        }
    }
}
