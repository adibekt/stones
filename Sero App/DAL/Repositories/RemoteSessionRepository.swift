//
//  RemoteSessionRepository.swift
//  Sero App
//
//  Created by tair on 24.01.22.
//

import Foundation
import RxRelay
import Firebase
import FirebaseFirestore

protocol IRemoteSessionRepository {
    func observeSession<T: Codable>(connection: ConnectionResult) -> BehaviorRelay<T?>
    func observe<T: Codable>(path: String, filter: FirebaseFilter) -> BehaviorRelay<T?>
    func observeQueue(in command: CommandsQueue) -> BehaviorRelay<[RemoteSessionCommand]>
    func removeCommand(theraySessionID: String, commandID: String, deleted: @escaping VoidClosure)
    func reset()
}

class RemoteSessionRepository: IRemoteSessionRepository {

    private let dataBase = Firestore.firestore()
    private var snapshotListener: ListenerRegistration?

    func observeSession<T: Codable>(connection: ConnectionResult) -> BehaviorRelay<T?> {
        let relay = BehaviorRelay<T?>(value: nil)
        dataBase
            .collection(SessionsRepository.shared().usersCollection)
            .document(connection.patientID)
            .collection(RemoteCommandsService.therapySessionsCollection)
            .document(connection.therapyID)
            .addSnapshotListener(includeMetadataChanges: true) { snapshot, error in
                do {
                    let doc = try snapshot?.data(as: T.self, decoder: Firestore.Decoder.init())
                    relay.accept(doc)
                } catch {
                    print(error.localizedDescription)
                }
        }
        return relay
    }

    func observe<T: Codable>(path: String, filter: FirebaseFilter) -> BehaviorRelay<T?> {
        let relay = BehaviorRelay<T?>(value: nil)
        dataBase
            .collection(path)
            .whereField(filter.key, isEqualTo: filter.value)
            .addSnapshotListener(includeMetadataChanges: false) { snapshot, error in
                do {
                    let doc = try snapshot?.documents.first?.data(as: T.self, decoder: Firestore.Decoder.init())
                    relay.accept(doc)
                } catch {
                    print(error.localizedDescription)
                }
        }
        return relay
    }

    func observeQueue(in command: CommandsQueue) -> BehaviorRelay<[RemoteSessionCommand]> {
        let relay = BehaviorRelay<[RemoteSessionCommand]>(value: [])
        guard let commandID = command.id else {
            return relay
        }
        dataBase
            .collection(RemoteCommandsService.path)
            .document(commandID)
            .collection(RemoteCommandsService.queuePath)
            .addSnapshotListener(includeMetadataChanges: false) { snapshot, error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                if let array = snapshot?.documents.compactMap({ try? $0.data(as: RemoteSessionCommand.self,
                                                                          decoder: Firestore.Decoder.init()) }) {
                    relay.accept(array)
                }
        }
        return relay
    }

    func removeCommand(theraySessionID: String, commandID: String,
                       deleted: @escaping VoidClosure) {
        dataBase
            .collection(RemoteCommandsService.path)
            .whereField(RemoteCommandsService.therapySessionIDField, isEqualTo: theraySessionID)
            .getDocuments { snapshot, error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                if let id = snapshot?.documents.first?.documentID {
                    self.dataBase.collection(RemoteCommandsService.path)
                        .document(id)
                        .collection(RemoteCommandsService.queuePath)
                        .document(commandID)
                        .delete { error in
                            if let errorText = error?.localizedDescription {
                                print(errorText)
                            }
                            deleted()
                        }
                }
            }
    }

    func reset() {
        if let snapshotListener = snapshotListener {
            snapshotListener.remove()
        }
    }
}
