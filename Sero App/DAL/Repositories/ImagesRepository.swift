//
//  ImagesRepository.swift
//  Sero App
//
//  Created by Roman Nekliukov on 18.11.21.
//

import Foundation
import FirebaseStorage

protocol IImagesRepository {
    func addImageForUser(imageData: Data, metadata: StorageMetadata, userId: String,
                         addUser: @escaping ((String) -> Void ))
}

final class ImagesRepository: IImagesRepository {
    private let usersStoragePath = "users/"

    private let storage = Storage.storage()
    private let storageRef = Storage.storage().reference()

    func addImageForUser(imageData: Data, metadata: StorageMetadata, userId: String,
                         addUser: @escaping ((String) -> Void )) {
        let storageUsersRef = storageRef.child(usersStoragePath + userId)
        storageUsersRef.putData(imageData, metadata: metadata) { (_, error) in
            if error == nil {
                storageUsersRef.downloadURL { (url, _) in
                    if let url = url {
                        addUser(url.absoluteString)
                        return
                    } else { addUser("") }
                }
            } else { addUser("") }
        }
    }
}
