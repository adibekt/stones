//
//  UserUpdate.swift
//  Sero App
//
//  Created by Roman Nekliukov on 12.12.21.
//

import Foundation

struct UserUpdate: Decodable {
    var id: String
    var role: UserRole
}
