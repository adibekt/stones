//
//  CommandsQueue.swift
//  Sero App
//
//  Created by tair on 10.01.22.
//

import Foundation
import FirebaseFirestoreSwift

struct CommandsQueue: Codable {
    @DocumentID var id: String?
    var patientID: String
    var therapySessionID: String
    var currentIntensity: UInt8?
    var currentDuration: UInt16?
    var lastUpdated: Date?
    var isInBackground: Bool?
    var queue: [RemoteSessionCommand]?
}
