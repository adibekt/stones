//
//  TherapistConnectionRequest.swift
//  Sero App
//
//  Created by Roman Nekliukov on 25.12.21.
//

import Foundation

struct TherapistConnectionRequest: Codable {
    var accessCode: String = ""
    var patientId: String = ""
    var therapySessionId: String = ""
    var therapistId: String = ""
    var expiresAt: Date
}
