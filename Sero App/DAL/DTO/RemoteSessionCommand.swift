//
//  RemoteSessionCommand.swift
//  Sero App
//
//  Created by Таир Адибек on 27.12.2021.
//

import Foundation
import FirebaseFirestoreSwift

struct RemoteSessionCommand: Codable {
    @DocumentID var id: String?
    var action: ActionType
    var newIntensity: UInt8?
    var newDuration: UInt16?
    var createdAt: Date
}
