//
//  Set.swift
//  Sero App
//
//  Created by Таир Адибек on 23.11.2021.
//

import Foundation

class Set: Codable, ManualIdentified {
    var documentID: String
    var stoneId: String
    var startTime: Date
    var timeElapsed: String
    var intervals: [Interval]
    var pairsCount: Int

    init(documentID: String, stoneId: String, startTime: Date,
         timeElapsed: String, intervals: [Interval], pairsCount: Int) {
        self.documentID = documentID
        self.stoneId = stoneId
        self.startTime = startTime
        self.timeElapsed = timeElapsed
        self.intervals = intervals
        self.pairsCount = pairsCount
    }
}
