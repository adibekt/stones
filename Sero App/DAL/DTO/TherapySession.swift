//
//  TherapySession.swift
//  Sero App
//
//  Created by Таир Адибек on 23.11.2021.
//

import Foundation

protocol ManualIdentified {
    var documentID: String { get }
}

class TherapySession: Codable, ManualIdentified {
    var documentID: String
    var lastUpdated: Date?
    var currentIntensity: String
    var currentSpeed: String
    var stoneId: String
    var sets: [Set]
    var userID: String
    var batteryLevel: String?
    var errorText: String?
    var isFinished: Bool?
    var isFirstStoneVibrating: Bool?
    var isSecondStoneVibrating: Bool?

    init(documentID: String, currentIntensity: String, currentSpeed: String,
         stoneId: String, sets: [Set], userID: String) {
        self.documentID = documentID
        self.currentIntensity = currentIntensity
        self.currentSpeed = currentSpeed
        self.stoneId = stoneId
        self.sets = sets
        self.userID = userID
    }
}
