//
//  User.swift
//  Sero App
//
//  Created by Roman Nekliukov on 18.11.21.
//

import Foundation

class User: Codable {
    var id: String?
    var email: String
    var firstName: String
    var lastName: String
    var role: UserRole
    var provider: String
    var phoneNumber: String?
    var imageUrl: String = ""

    init(id: String, email: String, firstName: String,
         lastName: String, phoneNumber: String, provider: String,
         role: UserRole = UserRole.none, imageUrl: String = "") {
        self.id = id
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.provider = provider
        self.imageUrl = imageUrl
        self.role = role
    }
}
