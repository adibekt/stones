//
//  Interval.swift
//  Sero App
//
//  Created by Таир Адибек on 23.11.2021.
//

import Foundation

class Interval: Codable, ManualIdentified {
    var documentID: String
    var intensity: String
    var speed: String
    var startTime: Date
    var timeElapsed: String

    init(documentID: String, intensity: String, speed: String,
         startTime: Date, timeElapsed: String) {
        self.documentID = documentID
        self.intensity = intensity
        self.speed = speed
        self.startTime = startTime
        self.timeElapsed = timeElapsed
    }
}
