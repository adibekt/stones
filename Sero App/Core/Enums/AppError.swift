//
//  AppError.swift
//  Sero App
//
//  Created by Roman Nekliukov on 16.01.22.
//

enum AppError: Error {
    case signupFailed
    case signinFailed
    case signinCancelled
    case signoutFailed
    case accountNotFound
    case emailAlreadyInUse
    case resetPasswordFailed
    case addFailed

    var localizedDescription: String {
        switch self {
        case .signupFailed:
            return L10n.Signup.Error.unableToSignup
        case .signinFailed:
            return L10n.Login.unableToSignin
        case .accountNotFound:
            return L10n.Common.Errors.accountNotFound
        case .signoutFailed:
            return L10n.Common.Errors.signout
        case .emailAlreadyInUse:
            return L10n.Signup.Error.emailInUse
        case .resetPasswordFailed:
            return L10n.ForgotPassword.error
        case .addFailed:
            return L10n.ForgotPassword.error
        default:
            return ""
        }
    }
}
