//
//  ActionType.swift
//  Sero App
//
//  Created by Таир Адибек on 27.12.2021.
//

import Foundation

enum ActionType: String, Codable {
    case startInterval
    case stopSet
    case startSet
    case startSession
    case finishSession
}
