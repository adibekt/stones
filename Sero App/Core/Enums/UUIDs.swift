//
//  UUIDs.swift
//  Sero App
//
//  Created by Таир Адибек on 18.11.2021.
//

import Foundation

enum UUIDs: String {
    case tapService = "5cfb3249-5937-4555-805f-745a12dd773b"
    case tapDuration = "77f47f52-2434-4fbc-9190-dc0939a911ed"
    case tapStrenght = "284fc312-2af9-45e3-9771-be5cc7bed57a"
    case tap = "83a6d561-7921-4e33-bd9b-3c8a02a3a1af"
    case batteryService = "180F"
    case batteryLevel = "2A19"
}
