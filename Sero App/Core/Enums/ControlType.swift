//
//  ControlType.swift
//  Sero App
//
//  Created by Таир Адибек on 22.11.2021.
//

import Foundation

enum ControlType {
    case intensity
    case speed
}
