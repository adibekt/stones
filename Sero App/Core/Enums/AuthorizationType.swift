//
//  AuthorizationType.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

enum AuthorizationType: String, Codable {
    case email
    case google
    case apple

    var title: String {
        switch self {
        case .email:
            return L10n.Authorization.emailSignUp
        case .google:
            return L10n.Authorization.googleSignUp
        case .apple:
            return L10n.Authorization.appleSignUp
        }
    }

    var imageName: String {
        switch self {
        case .email:
            return Asset.Images.envelope.name
        case .google:
            return Asset.Images.googleLogo.name
        case .apple:
            return Asset.Images.appleLogo.name
        }
    }

    var isCustomType: Bool {
        return self == .email
    }
}
