//
//  UserRole.swift
//  Sero App
//
//  Created by Roman Nekliukov on 11.12.21.
//

enum UserRole: String, Codable {
    case none
    case client
    case therapist

    var imageName: String {
        switch self {
        case .client:
            return Asset.Images.clientRole.name
        case .therapist:
            return Asset.Images.therapistRole.name
        case .none:
            return ""
        }
    }

    var title: String {
        switch self {
        case .client:
                return L10n.Role.client
        case .therapist:
                return L10n.Role.therapist
        case .none:
            return "none"
        }
    }
}
