//
//  AppleAuthorizationManager.swift
//  Sero App
//
//  Created by Roman Nekliukov on 10.11.21.
//
import Foundation
import Firebase
import AuthenticationServices

final class AppleAuthorizationManager: NSObject, ASAuthorizationControllerDelegate {
    var authCompletedClosure: VoidClosure?
    var authFailedClosure: StringClosure?

    private var nonce: String?

    let firebaseAuthenticationService: FirebaseAuthenticationService

    init(firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        authFailedClosure?("")
        print(error)
    }

    func authorizationController(controller: ASAuthorizationController,
                                 didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIdCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = nonce else {
                authFailedClosure?("")
                print("Invalid state: A login callback was received, but" +
                      " no login request was sent.")
                return
            }

            guard let appleIDToken = appleIdCredential.identityToken else {
                authFailedClosure?("")
                print("Invalid state: A login callback was received, but " +
                            "no login request was sent.")
                return
            }

            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: " +
                        "\(appleIDToken.debugDescription)")
                authFailedClosure?("")
                return
            }

            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)

            firebaseAuthenticationService.externalSignIn(
                credential: credential,
                provider: .apple,
                fullname: FullName(appleIdCredential.fullName?.givenName,
                                   appleIdCredential.fullName?.familyName)) { [weak self] result in
                    switch result {
                    case .success:
                        self?.authCompletedClosure?()
                    case .failure(let error):
                        self?.authFailedClosure?(error.localizedDescription)
                    }
                }
        }
    }

    public func performAppleSignIn() {
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()

        let nonce = EncryptionService.randomNonceString()
        self.nonce = nonce

        request.requestedScopes = [.fullName, .email]
        request.nonce = EncryptionService.sha256(nonce)
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.performRequests()
    }
}
