//
//  FirebaseAuthenticationService.swift
//  Sero App
//
//  Created by Roman Nekliukov on 12.11.21.
//

import Foundation
import FirebaseAuth
import AuthenticationServices
import Firebase
import FirebaseFirestore
import FirebaseStorage

typealias FullName = (firstName: String?, secondName: String?)

final class FirebaseAuthenticationService {
    private let firebaseAuth = Auth.auth()
    private let imagesRepository: IImagesRepository
    private let usersRepository: IUsersRepository

    init(usersRepository: UsersRepository,
         imagesRepository: ImagesRepository) {
        self.usersRepository = usersRepository
        self.imagesRepository = imagesRepository
    }

    public func externalSignIn(credential: AuthCredential,
                               provider: AuthorizationType,
                               fullname: FullName? = nil,
                               completion: @escaping ResultClosure) {
        firebaseAuth.signIn(with: credential) { authResult, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(.signinFailed))
                return
            }

            guard let authResult = authResult else {
                completion(.failure(.signinFailed))
                return
            }

            print("\(String(describing: Auth.auth().currentUser?.uid))")
            print("User signed in to firebase. Email = \(authResult.user.email ?? "Not found")")

            guard let userEmail = authResult.user.providerData.first?.email else {
                print("\(String(describing: error?.localizedDescription))")
                completion(.failure(.signinFailed))
                return
            }

            self.usersRepository.getUser(email: userEmail, provider: provider.rawValue) { user in
                if user != nil {
                    if let user = user {
                        AppSettings.currentUser = user
                    }
                    completion(.success(()))
                    return
                }

                let data = authResult.user
                var firstName: String = ""; var lastName: String = ""

                if let displayName = data.displayName {
                    if !displayName.isBlank {
                        let nameParts = displayName.components(separatedBy: " ")
                        if nameParts.count >= 1 {
                            firstName = nameParts[0]
                        }

                        if nameParts.count >= 2 {
                            lastName = nameParts[1]
                        }
                    }
                } else if let givenName = fullname?.firstName,
                          let familyName = fullname?.secondName {
                    firstName = givenName
                    lastName = familyName
                }

                let linkedAccount = LinkedAccountModel(id: data.uid, email: userEmail,
                                                       firstName: firstName, lastName: lastName,
                                                       phoneNumber: data.phoneNumber ?? "",
                                                       imageUrl: data.photoURL, provider: provider)

                self.linkAuthenticatedAccount(linkedAccount: linkedAccount, completion: completion)
            }
        }
    }

    public func manualSignIn(withCredentials loginModel: LoginModel, completion: @escaping ResultClosure) {
        Auth.auth().signIn(withEmail: loginModel.email, password: loginModel.password) { (_, error) in
            if let error = error {
                print(error)
                completion(.failure(.signinFailed))
                return
            }

            self.usersRepository.getUser(email: loginModel.email, provider: AuthorizationType.email.rawValue) { user in
                guard let user = user else {
                    completion(.failure(.accountNotFound))
                    return
                }

                AppSettings.currentUser = user

                print("User signed in to firebase by email. Email = \(loginModel.email)")
                completion(.success(()))
            }
        }
    }

    public func resetPassword(email: String, completion: @escaping ResultClosure) {
        usersRepository.getUser(email: email, provider: AuthorizationType.email.rawValue) { user in
            guard let user = user else {
                let error = AppError.accountNotFound
                completion(.failure(error))
                print(error.localizedDescription)
                return
            }

            Auth.auth().sendPasswordReset(withEmail: user.email) { error in
                if let error = error {
                    print(error.localizedDescription)
                    completion(.failure(AppError.resetPasswordFailed))
                    return
                }

                print("Reset password message sent!")
                completion(.success(()))
            }
        }
    }

    public func manualSignup(with signupModel: SignupModel, completion: @escaping ResultClosure) {
        Auth.auth().createUser(withEmail: signupModel.email,
                               password: signupModel.password) { [self] (authResult, error) in
            if let error = error {
                let errorCode = AuthErrorCode(rawValue: error._code)
                if errorCode == AuthErrorCode.emailAlreadyInUse {
                    print("User with such email \(signupModel.email) does not exists.")
                    completion(.failure(.emailAlreadyInUse))
                } else {
                    print("Unable to create user with provided credentials.")
                    completion(.failure(.signupFailed))
                }

                return
            }

            guard let user = authResult?.user, let email = user.email else {
                print("Unable to retrieve user authentication data.")
                completion(.failure(.signupFailed))
                return
            }
            print("User signed up to firebase by email. Email = \(email)")
            let userEntity = User(id: user.uid,
                                  email: email,
                                  firstName: signupModel.firstName,
                                  lastName: signupModel.lastName,
                                  phoneNumber: signupModel.phoneNumber,
                                  provider: AuthorizationType.email.rawValue)

            AppSettings.currentUser = userEntity

            guard let imageInfo = self.getImageData(image: signupModel.image) else {
                self.usersRepository.createUser(user: userEntity, completion: completion)
                return
            }

            let addUserWithImage = { (imageUrl: String) -> Void in
                userEntity.imageUrl = imageUrl
                self.usersRepository.createUser(user: userEntity, completion: completion)
            }

            self.imagesRepository.addImageForUser(imageData: imageInfo.imageData,
                                                  metadata: imageInfo.meta,
                                                  userId: user.uid,
                                                  addUser: addUserWithImage)
        }
    }

    public func signOut(completion: ResultClosure? = nil) {
        do {
            try firebaseAuth.signOut()
            completion?(.success(()))
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
            completion?(.failure(AppError.signoutFailed))
            return
        }
    }

    public func isAuthorized(completion: @escaping BoolClosure) {
        if AppSettings.isAppFirstLaunch {
            // Sign out user which still logged in even after app deletion in
            // persisted session
            signOut()
            AppSettings.isAppFirstLaunch = false
            completion(false)
            return
        }

        guard let currentUser = firebaseAuth.currentUser else {
            return completion(false)
        }

        currentUser.reload { error in
            if let error = error {
                print(error.localizedDescription)
                completion(false)
            } else {
                completion(self.firebaseAuth.currentUser != nil)
            }

            return
        }
    }

    private func linkAuthenticatedAccount(linkedAccount: LinkedAccountModel, completion: @escaping ResultClosure) {
        let userEntity = User(id: linkedAccount.id, email: linkedAccount.email,
                              firstName: linkedAccount.firstName, lastName: linkedAccount.lastName,
                              phoneNumber: linkedAccount.phoneNumber, provider: linkedAccount.provider.rawValue)

        var imageData: Data?

        linkedAccount.imageUrl.map {
            do {
                imageData = try Data(contentsOf: $0)
            } catch {
                print(error.localizedDescription)
            }
        }

        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"

        let successClosure = {
            AppSettings.currentUser = userEntity
            completion(.success(()))
        }

        guard let imageData = imageData else {
            usersRepository.createUser(user: userEntity) { result in
                switch result {
                case .success:
                    successClosure()
                case .failure(let error):
                    completion(.failure(error))
                }
            }

            return
        }

        let addUserWithImage = { [weak self] (imageUrl: String) -> Void in
            userEntity.imageUrl = imageUrl
            self?.usersRepository.createUser(user: userEntity) { result in
                switch result {
                case .success:
                    successClosure()
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }

        imagesRepository.addImageForUser(imageData: imageData, metadata: metadata,
                                         userId: linkedAccount.id, addUser: addUserWithImage)
    }

    private func getImageData(image: UIImage?) -> (imageData: Data, meta: StorageMetadata)? {
        guard let image = image,
              let imageData = image.jpegData(compressionQuality: 0.4) else {
            return nil
        }

        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"

        return (imageData, metadata)
    }
}
