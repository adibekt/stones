//
//  GoogleAuthorizationManager.swift
//  Sero App
//
//  Created by Roman Nekliukov on 8.11.21.
//
import GoogleSignIn
import Firebase

final class GoogleAuthorizationManager {
    private let signInConfig = GIDConfiguration.init(clientID: FirebaseApp.app()?.options.clientID ?? "")

    let firebaseAuthenticationService: FirebaseAuthenticationService

    init(firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    public func signIn(completion: @escaping ResultClosure) {
        guard let presenter = UIApplication.shared.windows.first?.rootViewController else {
            completion(.failure(.signinFailed))
            return
        }

        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: presenter) { user, error in
            if error != nil {
                completion(.failure(.signinCancelled))
                return
            }

            guard let authentication = user?.authentication,
                  let idToken = authentication.idToken else {
                      completion(.failure(.signinFailed))
                      return
            }

            let credential = GoogleAuthProvider.credential(
                withIDToken: idToken, accessToken: authentication.accessToken)

            self.firebaseAuthenticationService.externalSignIn(credential: credential,
                                                              provider: .google,
                                                              completion: completion)
        }
    }
}
