//
//  RemoteCommandsService.swift
//  Sero App
//
//  Created by Таир Адибек on 27.12.2021.
//

import Foundation

class RemoteCommandsService {
    static let waitingAwakenessTime = 15
    static let path = "remoteSessionCommands"
    static let queuePath = "queue"
    static let currentIntensityField = "currentIntensity"
    static let currentDurationField = "currentDuration"
    static let therapySessionIDField = "therapySessionID"
    static let lastUpdatedField = "lastUpdated"
    static let therapySessionsCollection = "therapySessions"
    static let isInBackgroundField = "isInBackground"
    private var isBusy = false
    let repo = SessionsRepository()
    var currentQueuePath: String?

    func initEmptyQueue(connectionResult: ConnectionResult) {
        if isBusy || currentQueuePath != nil {
            return
        }
        isBusy = true
        let queue = CommandsQueue(patientID: connectionResult.patientID,
                                  therapySessionID: connectionResult.therapyID,
                                  currentIntensity: nil, currentDuration: nil,
                                  queue: nil)
        repo.addDocument(with: RemoteCommandsService.path, document: queue) { result in
            if let docID = try? result.get().documentID {
                self.currentQueuePath = RemoteCommandsService.path + "/" + docID
            }
        }
    }

    func add(command: RemoteSessionCommand) {
        guard let currentQueuePath = currentQueuePath else {
            return
        }
        repo.addDocument(with: currentQueuePath + "/queue", document: command) { _ in }
    }

    func updateCurrentValues(intensity: UInt8, duration: UInt16) {
        guard let currentQueuePath = currentQueuePath else {
            return
        }
        repo.updateDocument(with: currentQueuePath, newValues: [RemoteCommandsService.currentIntensityField: intensity,
                                                                RemoteCommandsService.currentDurationField: duration])
    }

    func updateNetworkStatus() {
        guard let currentQueuePath = currentQueuePath else {
            return
        }
        repo.updateDocument(with: currentQueuePath, newValues: [RemoteCommandsService.lastUpdatedField: Date()])
    }

    func reset() {
        isBusy = false
        currentQueuePath = nil
    }
}
