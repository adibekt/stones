//
//  StoneBluetoothService.swift
//  Sero App
//
//  Created by Таир Адибек on 16.12.2021.
//

import Foundation
import UIKit

final class StoneBluetoothService {

    private var timer: Timer?
    private var backgroundTask = UIBackgroundTaskIdentifier.invalid
    private var intensity, newStrengthValue: UInt8?
    private var duration, newDurationValue: UInt16?
    private var isFirstStoneVibrating = true
    private var isNewInterval = false
    private var setStarted = false
    private var intervalStartTime, sessionStartTime: Date?
    private var pairFinished = false

    func startSet(intensity: UInt8, duration: UInt16) {
        self.intensity = intensity
        self.duration = duration
        setStarted = true
        isFirstStoneVibrating = true
        sessionStartTime = Date()
        intervalStartTime = Date()
        runSet()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                                     selector: #selector(fireTimer),
                                     userInfo: nil, repeats: true)
    }

    func startInterval(intensity: UInt8, duration: UInt16) {
        if setStarted {
            newStrengthValue = intensity
            newDurationValue = duration
            isNewInterval = true
        } else {
            startSet(intensity: intensity, duration: duration)
        }
    }

    func pause() {
        isFirstStoneVibrating = true
        setStarted = false
        timer?.invalidate()
        timer = nil
    }

    @objc private func fireTimer() {
        guard let startTime = intervalStartTime,
              let sessionStartTime = sessionStartTime,
              // Mind using intensity if needed
              // let intensity = self.intensity,
              let duration = self.duration else {
            return
        }
        let now = Date()
        let sessionDelta = sessionStartTime.distance(to: now)
        TherapistLocalSessionService.shared().updateElapsedTime(with: sessionDelta.string())
        let numberOfSecs = startTime.distance(to: now)
        if Int(numberOfSecs) == 0 {
            return
        }
        if (Int(numberOfSecs.rounded(.toNearestOrEven)) % Int(getDurationInSeconds(invert(duration: duration)))) == 0 {
            if !isFirstStoneVibrating {
                pairFinished = true
                TherapistLocalSessionService.shared().increaseNumberOfPairs()
            }
            isFirstStoneVibrating = !isFirstStoneVibrating
            TherapistLocalSessionService.shared().vibrateStone(isFirst: isFirstStoneVibrating)
            runNextStone()
        }
    }

    private func runNextStone() {
        guard let intensity = intensity, let duration = duration else {
            return
        }

        if isFirstStoneVibrating && isNewInterval {
            self.intensity = newStrengthValue
            self.duration = newDurationValue
            intervalStartTime = Date()
            TherapistLocalSessionService.shared().addNewInterval(intensity: intensity, duration: duration)
            isNewInterval = false
        }
        BLEService.shared.setData(strength: intensity,
                                  duration: invert(duration: duration),
                                  firstStone: isFirstStoneVibrating)
    }

    private func runSet() {
        guard let intensity = intensity, let duration = duration else {
            return
        }
        BLEService.shared.setData(strength: intensity, duration: invert(duration: duration), firstStone: true)
        TherapistLocalSessionService.shared().vibrateStone(isFirst: true)
    }

    private func getDurationInMs(_ value: Int) -> UInt16 {
        return UInt16(value * 1000)
    }

    private func getDurationInSeconds(_ value: UInt16) -> Double {
        return Double(value / 1000)
    }

    /// Duration stores in  range of 1000 - 10000, thats why we need this function to invert its value
    private func invert(duration: UInt16) -> UInt16 {
        return 11000 - duration
    }
}
