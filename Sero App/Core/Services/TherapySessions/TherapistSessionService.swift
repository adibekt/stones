//
//  TherapistSessionService.swift
//  Sero App
//
//  Created by Таир Адибек on 15.12.2021.
//

import Foundation
import RxRelay
import RxSwift
import Network

protocol ITherapistSessionService {
    func observeCurrentSession(connection: ConnectionResult) -> BehaviorRelay<TherapySession?>
    func startSession(intensity: UInt8, duration: UInt16,
                      connection: ConnectionResult?) -> BehaviorRelay<TherapySession?>
    func addSet(intensity: UInt8, duration: UInt16)
    func addInterval(intensity: UInt8, duration: UInt16)
    func addNewInterval(intensity: UInt8, duration: UInt16)
    func stopSet()
    func finishSession()
    func increaseNumberOfPairs()
    func updateElapsedTime(with secs: String)
    func initEmptySession() -> TherapySession?
    func getCurrentSession() -> TherapySession?
    func vibrateStone(isFirst: Bool)
    func notifyUpdatesAcceptance()
    func connectionInterrupted(with text: String)
    func reset()
}

class TherapistLocalSessionService: ITherapistSessionService {

    let currentSession = BehaviorRelay<TherapySession?>(value: nil)
    var stoneService = StoneBluetoothService()
    private let bag = DisposeBag()
    private let repo = SessionsRepository.shared()
    static private var share = TherapistLocalSessionService()
    static func shared() -> ITherapistSessionService {
        return share
    }

    func observeCurrentSession(connection: ConnectionResult) -> BehaviorRelay<TherapySession?> {
        return currentSession
    }

    func initEmptySession() -> TherapySession? {
        let session = TherapySession(documentID: getNewID(), currentIntensity: "5",
                                     currentSpeed: "5",
                                     stoneId: BLEService.shared.getStoneId(),
                                     sets: [], userID: AppSettings.currentUser?.id ?? "")
        currentSession.accept(session)
        observeBatteryLevel()
        listenToUpdates()
        return session
    }

    func startSession(intensity: UInt8, duration: UInt16,
                      connection: ConnectionResult? = nil) -> BehaviorRelay<TherapySession?> {
        if currentSession.value == nil {
            let interval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                    speed: "\(duration)",
                                    startTime: Date(),
                                    timeElapsed: "00:00")
            let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                          startTime: Date(),
                          timeElapsed: "00:00", intervals: [interval],
                          pairsCount: 0)
            let session = TherapySession(documentID: getNewID(), currentIntensity: "\(intensity)",
                                         currentSpeed: "\(duration)",
                                         stoneId: BLEService.shared.getStoneId(),
                                         sets: [set], userID: AppSettings.currentUser?.id ?? "")
            currentSession.accept(session)
            stoneService.startInterval(intensity: intensity, duration: duration)
            observeBatteryLevel()
            listenToUpdates()
        } else {
            addSet(intensity: intensity, duration: duration)
        }
        return currentSession
    }

    func addSet(intensity: UInt8, duration: UInt16) {
        guard let session = currentSession.value else {
            return
        }
        let interval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                speed: "\(duration)",
                                startTime: Date(),
                                timeElapsed: "00:00")
        let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                      startTime: Date(),
                      timeElapsed: "00:00",
                      intervals: [interval], pairsCount: 0)
        session.sets.append(set)
        stoneService.startInterval(intensity: intensity, duration: duration)
        currentSession.accept(session)
    }

    /// Call this method from VM, it calls stone service's vibration
    func addInterval(intensity: UInt8, duration: UInt16) {
        guard currentSession.value != nil else {
            return
        }
        stoneService.startInterval(intensity: intensity, duration: duration)
    }

    /// Call this method from Stone service to write data to Firebase
    func addNewInterval(intensity: UInt8, duration: UInt16) {
        guard let session = currentSession.value else {
            return
        }
        completeLatestInterval()
        let newInterval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                   speed: "\(duration)", startTime: Date(), timeElapsed: "00:00")
        session.currentIntensity = "\(intensity)"
        session.currentSpeed = "\(duration)"
        session.sets.last?.intervals.append(newInterval)
        currentSession.accept(session)
    }

    func stopSet() {
        guard let session = currentSession.value,
              let set = session.sets.last else {
                  return
              }
        let difference = set.startTime.distance(to: Date()).string()
        set.timeElapsed = difference
        completeLatestInterval()
        currentSession.accept(session)
        stoneService.pause()
        pauseStones()
    }

    func finishSession() {
        guard let session = currentSession.value else {
            return
        }
        session.isFinished = true
        currentSession.accept(session)
        stopSet()
        currentSession.accept(nil)
        stoneService.pause()
    }

    func increaseNumberOfPairs() {
        guard let session = currentSession.value else {
            return
        }
        session.sets.last?.pairsCount += 1
        currentSession.accept(session)
    }

    func updateElapsedTime(with secs: String) {
        guard let session = currentSession.value else {
            return
        }
        session.sets.last?.timeElapsed = secs
        currentSession.accept(session)
    }

    func getCurrentSession() -> TherapySession? {
        return currentSession.value
    }

    func vibrateStone(isFirst: Bool) {
        guard let session = currentSession.value else {
            return
        }

        if isFirst {
            session.isFirstStoneVibrating = true
            session.isSecondStoneVibrating = false
        } else {
            session.isSecondStoneVibrating = true
            session.isFirstStoneVibrating = false
        }

        currentSession.accept(session)
    }

    func connectionInterrupted(with text: String) {
        guard let session = currentSession.value else {
            return
        }

        session.errorText = text
        currentSession.accept(session)
        finishSession()
    }

    private func observeBatteryLevel() {
        BLEService.shared.currentBatteryLevel.subscribe { [weak self] currentBatteryLevel in
            guard let session = self?.currentSession.value else {
                return
            }
            session.batteryLevel = currentBatteryLevel
            self?.currentSession.accept(session)
        } onError: { error in
            print(error.localizedDescription)
        } .disposed(by: bag)
    }

    private func pauseStones() {
        guard let session = currentSession.value else {
            return
        }

        session.isFirstStoneVibrating = false
        session.isSecondStoneVibrating = false
        currentSession.accept(session)
    }

    private func completeLatestInterval() {
        guard let session = currentSession.value,
              let currentInterval = session.sets.last?.intervals.last else {
                  return
              }
        let difference = currentInterval.startTime.distance(to: Date()).string()
        currentInterval.timeElapsed = difference
        currentSession.accept(session)
    }

    private func getNewID() -> String {
        return UUID().uuidString
    }

    private func listenToUpdates() {
        currentSession.subscribe { session in
            guard let session = session else {
                return
            }
            session.lastUpdated = Date()
            self.repo.setUpSession(session)
        } onError: { error in
            print(error.localizedDescription)
        }.disposed(by: bag)
    }

    func reset() {
        currentSession.accept(nil)
    }
}
