//
//  TherapistRemoteSessionService.swift
//  Sero App
//
//  Created by Таир Адибек on 27.12.2021.
//

import Foundation
import RxRelay
import RxSwift

class TherapistRemoteSessionService: ITherapistSessionService {

    var currentSession = BehaviorRelay<TherapySession?>(value: nil)
    let commandsService = RemoteCommandsService()
    let repo = RemoteSessionRepository()

    static private var share = TherapistRemoteSessionService()
    static func shared() -> ITherapistSessionService {
        return share
    }

    func startSession(intensity: UInt8, duration: UInt16,
                      connection: ConnectionResult?) -> BehaviorRelay<TherapySession?> {
        commandsService.updateCurrentValues(intensity: intensity, duration: duration)
        return currentSession
    }

    func observeCurrentSession(connection: ConnectionResult) -> BehaviorRelay<TherapySession?> {
        commandsService.initEmptyQueue(connectionResult: connection)
        currentSession = repo.observeSession(connection: connection)
        return currentSession
    }

    func addSet(intensity: UInt8, duration: UInt16) {
        let command = RemoteSessionCommand(action: .startSet,
                                          newIntensity: intensity,
                                          newDuration: duration,
                                          createdAt: Date())
        commandsService.add(command: command)
    }

    func addInterval(intensity: UInt8, duration: UInt16) {
        commandsService.updateCurrentValues(intensity: intensity, duration: duration)
    }

    func stopSet() {
        let command = RemoteSessionCommand(action: .stopSet,
                                          newIntensity: nil,
                                          newDuration: nil,
                                          createdAt: Date())
        commandsService.add(command: command)
    }

    func finishSession() {
        let command = RemoteSessionCommand(action: .finishSession,
                                          newIntensity: nil,
                                          newDuration: nil,
                                          createdAt: Date())
        commandsService.add(command: command)
    }

    func getCurrentSession() -> TherapySession? {
        return currentSession.value
    }

    func notifyUpdatesAcceptance() {
        commandsService.updateNetworkStatus()
    }

    func reset() {
        currentSession.accept(nil)
        repo.reset()
        commandsService.reset()
    }
}

extension ITherapistSessionService {
    func addNewInterval(intensity: UInt8, duration: UInt16) {
        // this is a empty implementation to allow this method to be optional
    }
    func increaseNumberOfPairs() {
        // this is a empty implementation to allow this method to be optional
    }
    func updateElapsedTime(with secs: String) {
        // this is a empty implementation to allow this method to be optional
    }
    func initEmptySession() -> TherapySession? {
        // this is a empty implementation to allow this method to be optional
        return nil
    }
    func vibrateStone(isFirst: Bool) {
        // this is a empty implementation to allow this method to be optional
    }
    func observeBatteryLevel() {
        // this is a empty implementation to allow this method to be optional
    }
    func notifyUpdatesAcceptance() {
        // this is a empty implementation to allow this method to be optional
    }
    func connectionInterrupted(with text: String) {
        // this is a empty implementation to allow this method to be optional
    }
}
