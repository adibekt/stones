//
//  NetworkAccessibilityService.swift
//  Sero App
//
//  Created by tair on 6.02.22.
//

import Foundation
import Network

class NetworkAccessibilityService {
    private var currentStatus = NWPath.Status.satisfied
    private let monitor = NWPathMonitor()
    var onValueChanged: NWStatusClosure?
    static private var share = NetworkAccessibilityService()
    static func shared() -> NetworkAccessibilityService {
        return share
    }

    func startObserving() {
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("We're connected!")
            } else {
                print("No connection.")
            }
            self.onValueChanged?(path.status)
            self.currentStatus = path.status
        }
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }

    func getCurrentStatus() -> NWPath.Status {
        return currentStatus
    }
}
