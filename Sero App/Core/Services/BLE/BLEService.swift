//
//  BLEService.swift
//  Sero App
//
//  Created by Таир Адибек on 13.11.2021.
//

import Foundation
import CoreBluetooth
import RxRelay
import RxSwift

typealias StonesPair = (first: CBPeripheral, second: CBPeripheral)

final class BLEService: NSObject, ObservableObject {

    var peripherals = BehaviorRelay<[CBPeripheral]>(value: [])
    var onError: StringClosure?
    var currentBatteryLevel = BehaviorRelay<String>(value: "")
    private var firstStoneBatteryLevel: Int?
    private var secondStoneBatteryLevel: Int?
    private var periodOfUpdateingBattery = 60.0
    private var centralManager = CBCentralManager()
    private var selectedStones: StonesPair?
    private var firstTapService: CBService?
    private var firtsBatteryService: CBService?
    private var secondTapService: CBService?
    private var secondBatteryService: CBService?
    private var firstStonesCharacteristics: [CBCharacteristic] = []
    private var secondStonesCharacteristics: [CBCharacteristic] = []
    private var isFirstConnected = false
    private var isSecondConnected = false
    private var isLeftStoneBatteryUpdated = false
    private var isRightStoneBatteryUpdated = false
    static let deviceGeneralName = "SeroStone"
    @Singleton static var shared = BLEService()

    override init() {
        super.init()
    }

    func initializeDelegates() {
        centralManager.delegate = self
    }

    func startScanning(completion: @escaping ((String?) -> Void)) {
        guard centralManager.state == .poweredOn else {
            completion(L10n.Welcome.turnBluetoothOn)
            return
        }
        completion(nil)
        // Delay is very important, because we need some time, while central manager become in poweredOn state
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.centralManager.scanForPeripherals(withServices: nil, options: nil)
        }
    }

    func setUpPair(with stones: StonesPair) {
        self.selectedStones = stones
    }

    func connectToSelectedStones() {
        guard let stones = selectedStones else {
            return
        }
        centralManager.stopScan()
        stones.first.delegate = self
        stones.second.delegate = self
        centralManager.connect(stones.first, options: nil)
        centralManager.connect(stones.second, options: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            if !self.isSecondConnected || !self.isFirstConnected {
                self.onError?("The selected device is no longer available")
            }
        }
    }

    private func discover() {
        selectedStones?.first.discoverServices([CBUUID(string: UUIDs.tapService.rawValue),
                                            CBUUID(string: UUIDs.batteryService.rawValue)])
        selectedStones?.second.discoverServices([CBUUID(string: UUIDs.tapService.rawValue),
                                            CBUUID(string: UUIDs.batteryService.rawValue)])
    }

    private func discoverChars() {
        if let tapService = firstTapService {
            selectedStones?.first.discoverCharacteristics(
                [CBUUID(string: UUIDs.tapStrenght.rawValue),
                 CBUUID(string: UUIDs.tapDuration.rawValue),
                 CBUUID(string: UUIDs.tap.rawValue)],
                for: tapService)
        }
        if let service = firtsBatteryService {
            selectedStones?.first.discoverCharacteristics(
                [CBUUID(string: UUIDs.batteryLevel.rawValue)],
                for: service)
        }

        if let secondTapService = secondTapService {
            selectedStones?.second.discoverCharacteristics(
                [CBUUID(string: UUIDs.tapStrenght.rawValue),
                 CBUUID(string: UUIDs.tapDuration.rawValue),
                 CBUUID(string: UUIDs.tap.rawValue)],
                for: secondTapService)
        }

        if let batteryService = secondBatteryService {
            selectedStones?.second.discoverCharacteristics(
                [CBUUID(string: UUIDs.batteryLevel.rawValue)],
                for: batteryService)
        }
    }

    func rescan() {
        clear()
        peripherals.accept([])
        selectedStones = nil
        startScanning { _ in }
    }

    func clear() {
        selectedStones = nil
        firstStonesCharacteristics = []
        secondStonesCharacteristics = []
        firtsBatteryService = nil
        firstTapService = nil
        secondBatteryService = nil
        secondTapService = nil
        isSecondConnected = false
        isFirstConnected = false
        peripherals.accept([])
    }

    func setData(strength: UInt8, duration: UInt16, firstStone: Bool) {
        var strengthCharacteristic, durationCharacteristic, tapCharacteristic: CBCharacteristic?
        if firstStone {
            strengthCharacteristic =
                    self.firstStonesCharacteristics.first(where: {
                        $0.uuid.uuidString.uppercased() == UUIDs.tapStrenght.rawValue.uppercased() })
            durationCharacteristic =
                    self.firstStonesCharacteristics.first(where: { $0.uuid.uuidString.uppercased() ==
                UUIDs.tapDuration.rawValue.uppercased() })
            tapCharacteristic =
                    self.firstStonesCharacteristics.first(where: { $0.uuid.uuidString.uppercased() ==
                UUIDs.tap.rawValue.uppercased() })
        } else {
            strengthCharacteristic =
                    self.secondStonesCharacteristics.first(where: {
                        $0.uuid.uuidString.uppercased() == UUIDs.tapStrenght.rawValue.uppercased() })
            durationCharacteristic =
                    self.secondStonesCharacteristics.first(where: { $0.uuid.uuidString.uppercased() ==
                UUIDs.tapDuration.rawValue.uppercased() })
            tapCharacteristic =
                    self.secondStonesCharacteristics.first(where: { $0.uuid.uuidString.uppercased() ==
                UUIDs.tap.rawValue.uppercased() })
        }
        guard strengthCharacteristic != nil, durationCharacteristic != nil, tapCharacteristic != nil else {
            return
        }
        do {
            let boolData = try JSONEncoder().encode(true)
            var strengthValue = strength
            var durationValue = duration
            let strengthData = Data(bytes: &strengthValue, count: MemoryLayout.size(ofValue: strengthValue))
            let durationData = Data(bytes: &durationValue, count: MemoryLayout.size(ofValue: durationValue))
            if firstStone {
                selectedStones?.first.writeValue(strengthData, for: strengthCharacteristic!, type: .withResponse)
                selectedStones?.first.writeValue(durationData, for: durationCharacteristic!, type: .withResponse)
                selectedStones?.first.writeValue(boolData, for: tapCharacteristic!, type: .withResponse)
            } else {
                selectedStones?.second.writeValue(strengthData, for: strengthCharacteristic!, type: .withResponse)
                selectedStones?.second.writeValue(durationData, for: durationCharacteristic!, type: .withResponse)
                selectedStones?.second.writeValue(boolData, for: tapCharacteristic!, type: .withResponse)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

    func getStoneId() -> String {
        return (selectedStones?.first.identifier.uuidString ?? "") +
        " " + (selectedStones?.second.identifier.uuidString ?? "")
    }

    func getBatteryLevel() {
        guard let batteryLevel = firstStonesCharacteristics.first(where: {
            $0.uuid.uuidString.uppercased() ==
            UUIDs.batteryLevel.rawValue.uppercased() }),
        let secondBatteryLevel = secondStonesCharacteristics.first(where: {
            $0.uuid.uuidString.uppercased() ==
            UUIDs.batteryLevel.rawValue.uppercased() })
        else {
                return
            }
        selectedStones?.first.readValue(for: batteryLevel)
        selectedStones?.second.readValue(for: secondBatteryLevel)
    }

    func reset() {
        peripherals.accept([])
        isSecondConnected = false
        isFirstConnected = false
        firstStoneBatteryLevel = nil
        secondStoneBatteryLevel = nil

        if let firstStone = selectedStones?.first {
            centralManager.cancelPeripheralConnection(firstStone)
        }
        if let secondStone = selectedStones?.second {
            centralManager.cancelPeripheralConnection(secondStone)
        }
        selectedStones = nil
        clear()
    }

    private func observeBattery() {
        self.getBatteryLevel()
        DispatchQueue.main.asyncAfter(deadline: .now() + periodOfUpdateingBattery) {
            self.observeBattery()
        }
    }
}

extension BLEService: CBCentralManagerDelegate, CBPeripheralManagerDelegate, CBPeripheralDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print(peripheral.state)
    }

    func peripheral(_ peripheral: CBPeripheral,
                    didUpdateValueFor characteristic: CBCharacteristic,
                    error: Error?) {
        if peripheral.identifier == selectedStones?.first.identifier {
            firstStoneBatteryLevel = Int(characteristic.value?.first ?? 100)
            isLeftStoneBatteryUpdated = true
            if !isRightStoneBatteryUpdated {
                return
            }
        } else if peripheral.identifier == selectedStones?.second.identifier {
            secondStoneBatteryLevel = Int(characteristic.value?.first ?? 100)
            isRightStoneBatteryUpdated = true
            if !isLeftStoneBatteryUpdated {
                return
            }
        }

        let chargesDifference = (firstStoneBatteryLevel ?? 100) - (secondStoneBatteryLevel ?? 100)
        let batteryLevel: String
        if chargesDifference >= 5 || chargesDifference <= -5 {
            batteryLevel = "L \(firstStoneBatteryLevel ?? 0)% • R \(secondStoneBatteryLevel ?? 0)%"
        } else {
            batteryLevel = L10n.Main.batteryLR + " \(characteristic.value?.first ?? 100)%"
        }
        currentBatteryLevel.accept(batteryLevel)
        isLeftStoneBatteryUpdated = false
        isRightStoneBatteryUpdated = false
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let discoveredServices = peripheral.services
        else {
            return
        }
        if peripheral.identifier == selectedStones?.first.identifier {
            for dService in discoveredServices {
                if dService.uuid.uuidString == UUIDs.tapService.rawValue.uppercased() {
                    firstTapService = dService
                } else if dService.uuid.uuidString == UUIDs.batteryService.rawValue.uppercased() {
                    firtsBatteryService = dService
                }
            }
        } else if peripheral.identifier == selectedStones?.second.identifier {
            for dService in discoveredServices {
                if dService.uuid.uuidString == UUIDs.tapService.rawValue.uppercased() {
                    secondTapService = dService
                } else if dService.uuid.uuidString == UUIDs.batteryService.rawValue.uppercased() {
                    secondBatteryService = dService
                }
            }
        }

        discoverChars()
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if peripheral.identifier == selectedStones?.first.identifier {
            firstStonesCharacteristics += service.characteristics ?? []
        } else if peripheral.identifier == selectedStones?.second.identifier {
            secondStonesCharacteristics += service.characteristics ?? []
        }

        if firstStonesCharacteristics.contains(where: { $0.uuid.uuidString.uppercased() ==
            UUIDs.batteryLevel.rawValue.uppercased() }) &&
            secondStonesCharacteristics.contains(where: { $0.uuid.uuidString.uppercased() ==
                UUIDs.batteryLevel.rawValue.uppercased() }) {
            getBatteryLevel()
            observeBattery()
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to", peripheral.name as Any)
        if peripheral.identifier == selectedStones?.first.identifier {
            selectedStones?.first.discoverServices([CBUUID(string: UUIDs.tapService.rawValue),
                                                CBUUID(string: UUIDs.batteryService.rawValue)])
            isFirstConnected = true

        } else if peripheral.identifier == selectedStones?.second.identifier {
            selectedStones?.second.discoverServices([CBUUID(string: UUIDs.tapService.rawValue),
                                                CBUUID(string: UUIDs.batteryService.rawValue)])
            isSecondConnected = true
        }
    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print(error?.localizedDescription as Any)
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOff {
            onError?(L10n.Common.Errors.connectionInterrupted)
        }
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        onError?(L10n.Common.Errors.connectionInterrupted)
    }

    func centralManager(_ central: CBCentralManager,
                        didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any], rssi RSSI: NSNumber) {
        if peripheral.name == BLEService.deviceGeneralName {
            var currentArray = peripherals.value
            if !currentArray.contains(where: { $0.identifier ==
                peripheral.identifier }) {
                currentArray.append(peripheral)
                peripherals.accept(currentArray)
            }
        }
    }
}
