// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Authorization {
    /// Continue with Apple
    internal static let appleSignUp = L10n.tr("Localizable", "Authorization.AppleSignUp")
    /// Sign up with Email
    internal static let emailSignUp = L10n.tr("Localizable", "Authorization.EmailSignUp")
    /// Continue with Google
    internal static let googleSignUp = L10n.tr("Localizable", "Authorization.GoogleSignUp")
    /// Have an account?
    internal static let haveAccount = L10n.tr("Localizable", "Authorization.HaveAccount")
    /// Log in
    internal static let logIn = L10n.tr("Localizable", "Authorization.LogIn")
    /// Terms
    internal static let terms = L10n.tr("Localizable", "Authorization.Terms")
    /// By using Sero you agree to our 
    internal static let usage = L10n.tr("Localizable", "Authorization.Usage")
    /// Welcome to Sero
    internal static let welcome = L10n.tr("Localizable", "Authorization.Welcome")
  }

  internal enum ClientDashboard {
    /// Stop session
    internal static let exitSession = L10n.tr("Localizable", "ClientDashboard.ExitSession")
    /// Enjoy your session with %s
    internal static func title(_ p1: UnsafePointer<CChar>) -> String {
      return L10n.tr("Localizable", "ClientDashboard.Title", p1)
    }
    internal enum AlertText {
      /// Therapist finished your session.
      internal static let finished = L10n.tr("Localizable", "ClientDashboard.AlertText.finished")
    }
    internal enum Alert {
      /// Your internet is down, please, turn it back on
      internal static let clientsInternetOff = L10n.tr("Localizable", "ClientDashboard.alert.ClientsInternetOff")
      /// Therapist's internet is down, the session is still on, but you can't get any updates
      internal static let therapistsInternetOff = L10n.tr("Localizable", "ClientDashboard.alert.TherapistsInternetOff")
    }
  }

  internal enum Common {
    internal enum Actions {
      /// OK
      internal static let ok = L10n.tr("Localizable", "Common.Actions.OK")
      /// Sign out
      internal static let signout = L10n.tr("Localizable", "Common.Actions.Signout")
    }
    internal enum Errors {
      /// Sero account with provided email not found.
      internal static let accountNotFound = L10n.tr("Localizable", "Common.Errors.AccountNotFound")
      /// Some error occured during creating a new command
      internal static let addError = L10n.tr("Localizable", "Common.Errors.AddError")
      /// Unable to connect to the client.
      internal static let clientConnectionError = L10n.tr("Localizable", "Common.Errors.ClientConnectionError")
      /// Conenction with your Sero Stones interrupted, reconnect them
      internal static let connectionInterrupted = L10n.tr("Localizable", "Common.Errors.ConnectionInterrupted")
      /// Some error occured while dismissing the request.
      internal static let dismissError = L10n.tr("Localizable", "Common.Errors.DismissError")
      /// No active rooms from clients for this code.
      internal static let noActiveRooms = L10n.tr("Localizable", "Common.Errors.NoActiveRooms")
      /// Signout failed. Please, check your connection or try again later.
      internal static let signout = L10n.tr("Localizable", "Common.Errors.Signout")
      /// Error
      internal static let title = L10n.tr("Localizable", "Common.Errors.Title")
      /// Unable to create room for therapists to join.
      internal static let unableToCreateRoom = L10n.tr("Localizable", "Common.Errors.UnableToCreateRoom")
    }
  }

  internal enum ConnectToClient {
    /// Enter client's 4-digit code
    internal static let enterCode = L10n.tr("Localizable", "ConnectToClient.EnterCode")
    /// Request connection
    internal static let request = L10n.tr("Localizable", "ConnectToClient.Request")
    /// Connect with a client
    internal static let title = L10n.tr("Localizable", "ConnectToClient.Title")
    /// Waiting for client...
    internal static let waiting = L10n.tr("Localizable", "ConnectToClient.Waiting")
  }

  internal enum ConnectToTherapistView {
    /// Connect
    internal static let connect = L10n.tr("Localizable", "ConnectToTherapistView.Connect")
    /// Dismiss
    internal static let dismiss = L10n.tr("Localizable", "ConnectToTherapistView.Dismiss")
    /// %s would like to connect
    internal static func request(_ p1: UnsafePointer<CChar>) -> String {
      return L10n.tr("Localizable", "ConnectToTherapistView.Request", p1)
    }
    /// Share this code to connect
    internal static let subtitle = L10n.tr("Localizable", "ConnectToTherapistView.Subtitle")
    /// Connect to your therapist
    internal static let title = L10n.tr("Localizable", "ConnectToTherapistView.Title")
  }

  internal enum ForgotPassword {
    /// Back to login
    internal static let backToLogin = L10n.tr("Localizable", "ForgotPassword.BackToLogin")
    /// Check your email for sign in instructions
    internal static let checkEmail = L10n.tr("Localizable", "ForgotPassword.CheckEmail")
    /// Email
    internal static let email = L10n.tr("Localizable", "ForgotPassword.Email")
    /// Unable to send reset password request for the selected email. Ensure that user with such email is registered and check you internet connection.
    internal static let error = L10n.tr("Localizable", "ForgotPassword.Error")
    /// Reset failed
    internal static let resetFailed = L10n.tr("Localizable", "ForgotPassword.ResetFailed")
    /// Sent!
    internal static let sent = L10n.tr("Localizable", "ForgotPassword.Sent")
    /// Submit
    internal static let submit = L10n.tr("Localizable", "ForgotPassword.Submit")
    /// Forgot password
    internal static let title = L10n.tr("Localizable", "ForgotPassword.Title")
    /// Your email
    internal static let yourEmail = L10n.tr("Localizable", "ForgotPassword.YourEmail")
  }

  internal enum General {
    /// Done
    internal static let done = L10n.tr("Localizable", "General.done")
  }

  internal enum Introduction {
    /// Exhale the past
    internal static let exhale = L10n.tr("Localizable", "Introduction.Exhale")
    /// Inhale the present
    internal static let inhale = L10n.tr("Localizable", "Introduction.Inhale")
    /// Sign out
    internal static let signOut = L10n.tr("Localizable", "Introduction.SignOut")
  }

  internal enum LetsPair {
    /// Connect
    internal static let connect = L10n.tr("Localizable", "LetsPair.connect")
    /// Let's pair your
    /// Sero Stones
    internal static let letsPair = L10n.tr("Localizable", "LetsPair.letsPair")
    /// Make Sure Bluetooth is on
    internal static let makeSureBluetoothIsOn = L10n.tr("Localizable", "LetsPair.makeSureBluetoothIsOn")
    /// Need help?
    internal static let needHelp = L10n.tr("Localizable", "LetsPair.needHelp")
    /// Rescan
    internal static let rescan = L10n.tr("Localizable", "LetsPair.rescan")
    /// Scanning...
    internal static let scanning = L10n.tr("Localizable", "LetsPair.scanning")
    /// Tap each stone to pair
    internal static let tapEachStone = L10n.tr("Localizable", "LetsPair.tapEachStone")
  }

  internal enum Login {
    /// Or login with
    internal static let alternativeLogin = L10n.tr("Localizable", "Login.AlternativeLogin")
    /// Email and password are required.
    internal static let credentialsRequired = L10n.tr("Localizable", "Login.CredentialsRequired")
    /// Email
    internal static let email = L10n.tr("Localizable", "Login.Email")
    /// Forgot password?
    internal static let forgotPassword = L10n.tr("Localizable", "Login.ForgotPassword")
    /// Invalid credentials
    internal static let invalidCredentials = L10n.tr("Localizable", "Login.InvalidCredentials")
    /// Dont have an account? 
    internal static let noAccount = L10n.tr("Localizable", "Login.NoAccount")
    /// Password
    internal static let password = L10n.tr("Localizable", "Login.Password")
    /// Sign in
    internal static let signIn = L10n.tr("Localizable", "Login.SignIn")
    /// Sign-up
    internal static let signUp = L10n.tr("Localizable", "Login.SignUp")
    /// Log-In
    internal static let title = L10n.tr("Localizable", "Login.Title")
    /// Try again
    internal static let tryAgain = L10n.tr("Localizable", "Login.TryAgain")
    /// Unable to sign in with provided credentials.
    internal static let unableToSignin = L10n.tr("Localizable", "Login.UnableToSignin")
    /// Your email
    internal static let yourEmail = L10n.tr("Localizable", "Login.YourEmail")
  }

  internal enum Main {
    /// L • R
    internal static let batteryLR = L10n.tr("Localizable", "Main.batteryLR")
    /// Complete Set
    internal static let complete = L10n.tr("Localizable", "Main.complete")
    /// Conenction with your Sero Stones interrupted, reconnect them
    internal static let connectionInterrupted = L10n.tr("Localizable", "Main.connectionInterrupted")
    /// Done
    internal static let done = L10n.tr("Localizable", "Main.done")
    /// Fast
    internal static let fast = L10n.tr("Localizable", "Main.fast")
    /// High
    internal static let high = L10n.tr("Localizable", "Main.high")
    /// Intensity
    internal static let intensity = L10n.tr("Localizable", "Main.intensity")
    /// Internet connection lost. Session data will not be synchronized with server.
    internal static let localInternetLost = L10n.tr("Localizable", "Main.localInternetLost")
    /// Low
    internal static let low = L10n.tr("Localizable", "Main.low")
    /// Pairs
    internal static let pairs = L10n.tr("Localizable", "Main.pairs")
    /// Placeholder
    internal static let placeholder = L10n.tr("Localizable", "Main.placeholder")
    /// Sero
    internal static let sero = L10n.tr("Localizable", "Main.sero")
    /// Your session has finished
    internal static let sessionDone = L10n.tr("Localizable", "Main.sessionDone")
    /// Sets
    internal static let sets = L10n.tr("Localizable", "Main.sets")
    /// Slow
    internal static let slow = L10n.tr("Localizable", "Main.slow")
    /// Speed
    internal static let speed = L10n.tr("Localizable", "Main.speed")
    /// Start Set
    internal static let start = L10n.tr("Localizable", "Main.start")
    /// Success
    internal static let success = L10n.tr("Localizable", "Main.success")
    /// Your session successfully finished, but its data will not be saved. Check your internet connection.
    internal static let successWithoutConnection = L10n.tr("Localizable", "Main.successWithoutConnection")
    /// Time elapsed
    internal static let time = L10n.tr("Localizable", "Main.time")
    /// Attention
    internal static let warning = L10n.tr("Localizable", "Main.warning")
  }

  internal enum MainView {
    internal enum Alert {
      /// Client's internet is down, please, wait for it to turn back on
      internal static let clientsInternetOff = L10n.tr("Localizable", "MainView.alert.ClientsInternetOff")
      /// Client's stones are disconnected, please, start a new session
      internal static let stonesDisabled = L10n.tr("Localizable", "MainView.alert.StonesDisabled")
      /// Your internet is down, please, turn it back on
      internal static let therapistsInternetOff = L10n.tr("Localizable", "MainView.alert.TherapistsInternetOff")
    }
  }

  internal enum Role {
    /// Client
    internal static let client = L10n.tr("Localizable", "Role.Client")
    /// Therapist
    internal static let therapist = L10n.tr("Localizable", "Role.Therapist")
    /// I am a...
    internal static let title = L10n.tr("Localizable", "Role.Title")
    internal enum Error {
      /// Role selection failed. Please, check your intenet connection.
      internal static let text = L10n.tr("Localizable", "Role.Error.Text")
      /// Something went wrong.
      internal static let title = L10n.tr("Localizable", "Role.Error.Title")
    }
  }

  internal enum Signup {
    /// Camera
    internal static let camera = L10n.tr("Localizable", "Signup.Camera")
    /// Please choose your preferred mode to set your profile image
    internal static let chooseMode = L10n.tr("Localizable", "Signup.ChooseMode")
    /// Let clients easily find you
    internal static let clientsFindYou = L10n.tr("Localizable", "Signup.ClientsFindYou")
    /// Confirm password
    internal static let confirmPassword = L10n.tr("Localizable", "Signup.ConfirmPassword")
    /// Email
    internal static let email = L10n.tr("Localizable", "Signup.Email")
    /// Your name
    internal static let fullFirstName = L10n.tr("Localizable", "Signup.FullFirstName")
    /// Your surname
    internal static let fullLastName = L10n.tr("Localizable", "Signup.FullLastName")
    /// Have an account?
    internal static let haveAccount = L10n.tr("Localizable", "Signup.HaveAccount")
    /// Log in
    internal static let login = L10n.tr("Localizable", "Signup.Login")
    /// Choose mode
    internal static let mode = L10n.tr("Localizable", "Signup.Mode")
    /// Password
    internal static let password = L10n.tr("Localizable", "Signup.Password")
    /// Phone number
    internal static let phoneNumber = L10n.tr("Localizable", "Signup.PhoneNumber")
    /// Photo Library
    internal static let photoLibrary = L10n.tr("Localizable", "Signup.PhotoLibrary")
    /// (for support only)
    internal static let supportOnly = L10n.tr("Localizable", "Signup.SupportOnly")
    /// Sign-up
    internal static let title = L10n.tr("Localizable", "Signup.Title")
    /// Sign up
    internal static let titleSeparated = L10n.tr("Localizable", "Signup.TitleSeparated")
    /// Upload profile picture
    internal static let uploadPicture = L10n.tr("Localizable", "Signup.UploadPicture")
    /// Your email
    internal static let yourEmail = L10n.tr("Localizable", "Signup.YourEmail")
    /// First name
    internal static let yourFirstName = L10n.tr("Localizable", "Signup.YourFirstName")
    /// Last name
    internal static let yourLastName = L10n.tr("Localizable", "Signup.YourLastName")
    /// Your phone number
    internal static let yourPhoneNumber = L10n.tr("Localizable", "Signup.YourPhoneNumber")
    internal enum Error {
      /// Please, provide an access to your camera through the settings.
      internal static let cameraAccess = L10n.tr("Localizable", "Signup.Error.CameraAccess")
      /// Invalid email format
      internal static let emailFormat = L10n.tr("Localizable", "Signup.Error.EmailFormat")
      /// Email is already in use
      internal static let emailInUse = L10n.tr("Localizable", "Signup.Error.EmailInUse")
      /// Field must be not empty
      internal static let notEmpty = L10n.tr("Localizable", "Signup.Error.NotEmpty")
      /// Password length should be more than 6 symbols
      internal static let passwordLength = L10n.tr("Localizable", "Signup.Error.PasswordLength")
      /// Passwords are not equal
      internal static let passwordNotEqual = L10n.tr("Localizable", "Signup.Error.PasswordNotEqual")
      /// Permissions error
      internal static let permissions = L10n.tr("Localizable", "Signup.Error.Permissions")
      /// Invalid phone format
      internal static let phoneFormat = L10n.tr("Localizable", "Signup.Error.PhoneFormat")
      /// Phone number length must be equal to 10
      internal static let phoneLength = L10n.tr("Localizable", "Signup.Error.PhoneLength")
      /// Registration failed.
      internal static let registrationFailed = L10n.tr("Localizable", "Signup.Error.RegistrationFailed")
      /// Unable to signup. Please, check your connection or provided data.
      internal static let unableToSignup = L10n.tr("Localizable", "Signup.Error.UnableToSignup")
    }
  }

  internal enum SuccessfulPair {
    /// Your Sero Stones are now paired and ready!
    internal static let title = L10n.tr("Localizable", "SuccessfulPair.Title")
  }

  internal enum TherapistOption {
    /// Connect to my clients Sero stones
    internal static let connectToClientsStones = L10n.tr("Localizable", "TherapistOption.ConnectToClientsStones")
    /// Connect to my stones
    internal static let connectToStones = L10n.tr("Localizable", "TherapistOption.ConnectToStones")
    /// Hello, %s
    internal static func greeting(_ p1: UnsafePointer<CChar>) -> String {
      return L10n.tr("Localizable", "TherapistOption.Greeting", p1)
    }
    /// Next
    internal static let next = L10n.tr("Localizable", "TherapistOption.Next")
    /// What would you like to do today?
    internal static let question = L10n.tr("Localizable", "TherapistOption.Question")
    /// Therapist
    internal static let therapist = L10n.tr("Localizable", "TherapistOption.Therapist")
  }

  internal enum Welcome {
    /// Accept Bluetooth permissions
    internal static let acceptPermissions = L10n.tr("Localizable", "Welcome.AcceptPermissions")
    /// Please, ensure that:
    /// 1. Bluetooth is turned on
    /// 2. New connections are allowed
    /// 3. Sero has premissions to use Bluetooth
    internal static let alertMessege = L10n.tr("Localizable", "Welcome.alertMessege")
    /// "Sero" has troubles with Bluetooth connection
    internal static let alertTitle = L10n.tr("Localizable", "Welcome.alertTitle")
    /// Before we get started
    internal static let beforeWeStart = L10n.tr("Localizable", "Welcome.beforeWeStart")
    /// Charge Stones
    internal static let chargeStones = L10n.tr("Localizable", "Welcome.chargeStones")
    /// Go to settings
    internal static let goToSettings = L10n.tr("Localizable", "Welcome.goToSettings")
    /// Try again
    internal static let tryAgain = L10n.tr("Localizable", "Welcome.tryAgain")
    /// Turn Bluetooth on
    internal static let turnBluetoothOn = L10n.tr("Localizable", "Welcome.turnBluetoothOn")
    /// Welcome to Sero
    internal static let welcomeToSero = L10n.tr("Localizable", "Welcome.welcomeToSero")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
