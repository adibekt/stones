//
//  SFSymbols.swift
//  Sero App
//
//  Created by Roman Nekliukov on 14.11.21.
//

import Foundation

enum SFSymbols: String {
    case eyeFill = "eye.fill"
    case eyeSlashFill = "eye.slash.fill"
    case cameraFill = "camera.fill"
}
