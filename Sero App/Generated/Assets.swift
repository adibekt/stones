// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal enum Colors {
    internal static let appleBlack = ColorAsset(name: "Colors/AppleBlack")
    internal static let backgroundGray = ColorAsset(name: "Colors/BackgroundGray")
    internal static let batteryGray = ColorAsset(name: "Colors/BatteryGray")
    internal static let buttonDarkGray = ColorAsset(name: "Colors/ButtonDarkGray")
    internal static let buttonGray = ColorAsset(name: "Colors/ButtonGray")
    internal static let customBlack = ColorAsset(name: "Colors/CustomBlack")
    internal static let darkGreen = ColorAsset(name: "Colors/DarkGreen")
    internal static let dashboardBlockGray = ColorAsset(name: "Colors/DashboardBlockGray")
    internal static let dashboardTextBlack = ColorAsset(name: "Colors/DashboardTextBlack")
    internal static let lightGray = ColorAsset(name: "Colors/LightGray")
    internal static let lightGreen = ColorAsset(name: "Colors/LightGreen")
    internal static let logoBottomGray = ColorAsset(name: "Colors/LogoBottomGray")
    internal static let logoTopGray = ColorAsset(name: "Colors/LogoTopGray")
    internal static let modalBackgroundLightGray = ColorAsset(name: "Colors/ModalBackgroundLightGray")
    internal static let newBackground = ColorAsset(name: "Colors/NewBackground")
    internal static let newLightGray = ColorAsset(name: "Colors/NewLightGray")
    internal static let oldWhite = ColorAsset(name: "Colors/OldWhite")
    internal static let seroBlack = ColorAsset(name: "Colors/SeroBlack")
    internal static let seroBlue = ColorAsset(name: "Colors/SeroBlue")
    internal static let textGray = ColorAsset(name: "Colors/TextGray")
    internal static let textUnderlineGray = ColorAsset(name: "Colors/TextUnderlineGray")
  }
  internal enum Images {
    internal static let stoneImage = ImageAsset(name: "Images/StoneImage")
    internal static let appIconGray = ImageAsset(name: "Images/appIconGray")
    internal static let appleLogo = ImageAsset(name: "Images/appleLogo")
    internal static let authBackground = ImageAsset(name: "Images/authBackground")
    internal static let awesomeBluetooth = ImageAsset(name: "Images/awesome-bluetooth")
    internal static let batteryIcon = ImageAsset(name: "Images/battery-icon")
    internal static let checkIcon = ImageAsset(name: "Images/check-icon")
    internal static let clientRole = ImageAsset(name: "Images/clientRole")
    internal static let cloudsBackground = ImageAsset(name: "Images/cloudsBackground")
    internal static let defaultUserImage = ImageAsset(name: "Images/defaultUserImage")
    internal static let emptyStone = ImageAsset(name: "Images/emptyStone")
    internal static let envelope = ImageAsset(name: "Images/envelope")
    internal static let googleLogo = ImageAsset(name: "Images/googleLogo")
    internal static let ionicMdBatteryCharging = ImageAsset(name: "Images/ionic-md-battery-charging")
    internal static let lock = ImageAsset(name: "Images/lock")
    internal static let logo = ImageAsset(name: "Images/logo")
    internal static let ls = ImageAsset(name: "Images/ls")
    internal static let pauseIcon = ImageAsset(name: "Images/pause-icon")
    internal static let placeholder = ImageAsset(name: "Images/placeholder")
    internal static let playIcon = ImageAsset(name: "Images/play-icon")
    internal static let signinBackground = ImageAsset(name: "Images/signinBackground")
    internal static let signinBackgroundShort = ImageAsset(name: "Images/signinBackgroundShort")
    internal static let stone = ImageAsset(name: "Images/stone")
    internal static let stones = ImageAsset(name: "Images/stones")
    internal static let therapistRole = ImageAsset(name: "Images/therapistRole")
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
