//
//  MainFactory.swift
//  Sero App
//
//  Created by Таир Адибек on 12.11.2021.
//

import SwiftUI
import Firebase

class MainFactory {

    private let usersRepository: UsersRepository
    private let imagesRepository: ImagesRepository

    private let firebaseAuthenticationService: FirebaseAuthenticationService
    private let appleAuthManager: AppleAuthorizationManager
    private let googleAuthManager: GoogleAuthorizationManager
    private let therapistRequestRepository: TherapistRequestRepository
    private let remoteSessionRepository: IRemoteSessionRepository

    init(isMock: Bool = false) {
        if !isMock {
            FirebaseConfiguration.shared.setLoggerLevel(FirebaseLoggerLevel.min)
            FirebaseApp.configure()
        }
        usersRepository = UsersRepository()
        imagesRepository = ImagesRepository()
        therapistRequestRepository = TherapistRequestRepository()

        firebaseAuthenticationService = FirebaseAuthenticationService(usersRepository: usersRepository,
                                                                      imagesRepository: imagesRepository)
        appleAuthManager = AppleAuthorizationManager(firebaseAuthenticationService: firebaseAuthenticationService)
        googleAuthManager = GoogleAuthorizationManager(firebaseAuthenticationService: firebaseAuthenticationService)
        remoteSessionRepository = RemoteSessionRepository()
    }

    func getAuthView(router: MainRouter) -> AuthorizationView {
        let viewModel = AuthorizationViewModel(router: router,
                                               appleAuthManager: appleAuthManager,
                                               googleAuthManager: googleAuthManager)
        return AuthorizationView(authViewModel: viewModel)
    }

    func getRequirementsView(router: MainRouter) -> RequirementsView {
        let viewModel = RequirementsViewModel(router: router)
        let view = RequirementsView(viewModel: viewModel)
        return view
    }

    func getRoleView(router: MainRouter) -> RoleView {
        return RoleView(viewModel: RoleViewModel(
            router: router,
            usersRepository: self.usersRepository))
    }

    func getTherapistOptionView(router: MainRouter) -> TherapistOptionView {
        return TherapistOptionView(viewModel: TherapistOptionViewModel(
            router: router, firebaseAuthenticationService: self.firebaseAuthenticationService))
    }

    func getAuthTabView(router: MainRouter, isLogin: Bool) -> AuthTabView {
        return AuthTabView(router: router, isLogin: isLogin)
    }

    func getLoginView(router: MainRouter) -> LoginView {
        let viewModel = LoginViewModel(router: router,
                                       appleAuthManager: appleAuthManager,
                                       googleAuthManager: googleAuthManager,
                                       firebaseAuthenticationService: firebaseAuthenticationService)
        return LoginView(viewModel: viewModel)
    }

    func getSignupView(router: MainRouter) -> SignupView {
        let viewModel = SignupViewModel(router: router,
                                        firebaseAuthenticationService: firebaseAuthenticationService)
        return SignupView(viewModel: viewModel)
    }

    func getForgotPasswordView(router: MainRouter) -> ForgotPasswordView {
        let viewModel = ForgotPasswordViewModel(firebaseAuthenticationService: firebaseAuthenticationService)
        return ForgotPasswordView(viewModel: viewModel)
    }

    func getLetsPairView(router: MainRouter) -> LetsPairView {
        let viewModel = LetsPairViewModel(router: router, firebaseAuthenticationService: firebaseAuthenticationService)
        let view = LetsPairView(viewModel: viewModel)
        return view
    }

    func getSuccessfulPairView(router: MainRouter) -> SuccessfulPairView {
        return SuccessfulPairView(router: router)
    }

    func getRoleMainView(router: MainRouter, role: UserRole,
                         with connectionResult: ConnectionResult? = nil) -> AnyView {
        if role == .client {
            let clientViewModel = ConnectToTherapistViewModel(
                router: router, therapistRequestRepository: therapistRequestRepository)
            let clientView = ConnectToTherapistView(viewModel: clientViewModel)
            return AnyView(clientView)
        } else {
            let therapistViewModel = MainViewModel(
                router: router,
                with: connectionResult)
            let therapistView = MainView(viewModel: therapistViewModel)
            return AnyView(therapistView)
        }
    }

    func getConnectToTherapistView(router: MainRouter) -> ConnectToTherapistView {
        let connectToTherapistViewModel = ConnectToTherapistViewModel(
            router: router, therapistRequestRepository: therapistRequestRepository)
        let connectToTherapistView = ConnectToTherapistView(viewModel: connectToTherapistViewModel)
        return connectToTherapistView
    }

    func getClientDashboardView(router: MainRouter, therapistName: String) -> ClientDashboardView {
        let viewModel = ClientDashboardViewModel(router: router,
                                                 therapistName: therapistName,
                                                 therapistRequestRepository: therapistRequestRepository,
                                                 repository: remoteSessionRepository)
        return ClientDashboardView(viewModel: viewModel)
    }

    func getConnectToClientView(router: MainRouter) -> ConnectToClientView {
        let viewModel = ConnectToClientViewModel(
            router: router,
            therapistRequestRepository: therapistRequestRepository)
        return ConnectToClientView(viewModel: viewModel)
    }

    public func isUserAuthorized(completion: @escaping BoolClosure) {
        firebaseAuthenticationService.isAuthorized { result in
            completion(result)
        }
    }
}
