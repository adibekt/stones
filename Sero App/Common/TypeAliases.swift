//
//  TypeAliases.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.11.21.
//

import Foundation
import Network

typealias StringClosure = (String) -> Void
typealias IntClosure = (Int) -> Void
typealias BoolClosure = (Bool) -> Void
typealias UserClosure = (User?) -> Void
typealias OptionalStringClosure = (String?) -> Void
typealias VoidClosure = () -> Void
typealias ResultClosure = (Result<Void, AppError>) -> Void
typealias ConnectionResult = (therapyID: String, patientID: String)
typealias ConnectionResultClosure = (ConnectionResult) -> Void
typealias FirebaseFilter = (key: String, value: String)
typealias NWStatusClosure = (NWPath.Status) -> Void
