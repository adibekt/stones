//
//  AudioPlayer.swift
//  Sero App
//
//  Created by Roman Nekliukov on 16.02.22.
//

import Foundation
import AVFoundation

class AudioPlayer {

    private var queuePlayer = AVQueuePlayer()
    private var playerLooper: AVPlayerLooper?

    @Singleton static var shared = AudioPlayer()

    private init() { }

    func playBackgroundMusic(volume: Float = 1.0) {
        guard let url = Bundle.main.url(forResource: "relaxationMelody", withExtension: "mp3") else {
            return
        }

        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print("Unable to use setActive for AVAudioSession.")
        }

        let playerItem = AVPlayerItem(asset: AVAsset(url: url))
        playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
        queuePlayer.volume = volume
        queuePlayer.play()
    }

    func pauseBackgroundMusic() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch {
            print("Unable to use setActive for AVAudioSession.")
        }

        queuePlayer.pause()
    }
}
