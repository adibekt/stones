//
//  StickyImageHeader.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct StickyImageHeader: View {
    var body: some View {
        GeometryReader { (reader: GeometryProxy) in
            Image(Asset.Images.authBackground.name)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .offset(y: reader.frame(in: .global).minY <= 0
                            ? 0: -reader.frame(in: .global).minY)
                .frame(width: reader.size.width,
                       height: reader.frame(in: .global).minY <= 0
                        ? reader.size.height
                        : reader.size.height + reader.frame(in: .global).minY)
        }.frame(width: UIScreen.main.bounds.width,
                height: UIScreen.main.bounds.height * 0.30)
    }
}
