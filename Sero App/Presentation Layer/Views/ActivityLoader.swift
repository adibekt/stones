//
//  ActivityLoader.swift
//  Sero App
//
//  Created by Roman Nekliukov on 13.11.21.
//

import SwiftUI

struct ActivityLoader: View {
    var body: some View {
        ZStack {
            Color(.white)
                .opacity(0.6)
                .ignoresSafeArea()
            ProgressView()
                .scaleEffect(1.5, anchor: .center)
                .progressViewStyle(CircularProgressViewStyle(tint: Color(Asset.Colors.darkGreen.name)))
        }
    }
}

struct ActivityLoader_Previews: PreviewProvider {
    static var previews: some View {
        ActivityLoader()
    }
}
