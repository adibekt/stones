//
//  BackgroundGradient.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.01.22.
//

import SwiftUI

struct MainBackgroundGradient: View {
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [Color(Asset.Colors.oldWhite.name), .white]),
                       startPoint: .topLeading,
                       endPoint: .bottomTrailing)
            .ignoresSafeArea()
    }
}

struct BackgroundGradient_Previews: PreviewProvider {
    static var previews: some View {
        MainBackgroundGradient()
    }
}
