//
//  SecuredInputRowView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct SecuredInputRowView: View {
    let title: String
    let placeholder: String
    var subtitle: String = ""

    @Binding var errorText: String
    @Binding var value: String
    @Binding var hideValue: Bool

    var body: some View {
        VStack(spacing: inputsSpacing) {
            HStack {
                Text(title)
                    .font(.custom(FontFamily.SFProDisplay.bold.name, size: 24))
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                Spacer()
            }

            VStack(spacing: 4) {
                HStack {
                    if hideValue {
                        SecureField(placeholder, text: $value)
                            .onChange(of: value) { _ in
                                errorText = ""
                            }
                            .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                            .foregroundColor(Color(Asset.Colors.darkGreen.name))
                            .frame(height: 20)
                    } else {
                        TextField(placeholder, text: $value)
                            .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                            .foregroundColor(Color(Asset.Colors.darkGreen.name))
                            .frame(height: 20)
                    }

                    Spacer()
                    Button(action: {
                        hideValue.toggle()
                    }, label: {
                        Image(systemName: hideValue
                              ? SFSymbols.eyeSlashFill.rawValue : SFSymbols.eyeFill.rawValue)
                            .foregroundColor(Color(Asset.Colors.textGray.name))
                    })
                }
                Divider().background(Color(Asset.Colors.darkGreen.name))
                if let errorText = errorText {
                    HStack {
                        Text(errorText)
                            .font(.custom(FontFamily.SFProDisplay.regular.name, size: 12))
                            .foregroundColor(.red)
                        Spacer()
                    }
                }
            }
        }
    }

    var inputsSpacing: CGFloat {
        return ScreenHelper().getScreenType() == .small ? 5 : 15
    }
}
