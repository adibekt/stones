//
//  BackButton.swift
//  Sero App
//
//  Created by Roman Nekliukov on 16.11.21.
//

import SwiftUI

struct BackButton: View {
    var color: Color = Color(Asset.Colors.darkGreen.name)
    let action: VoidClosure

    var body: some View {
        HStack {
            Button(action: { action() },
                   label: {
                       Image(systemName: "chevron.backward")
                           .font(.system(size: 20, weight: .bold))
                           .foregroundColor(color)
            })
            .frame(width: 10, height: 20, alignment: .center)
            .position(x: 40, y: 20)

            Spacer()
        }
    }
}
