//
//  InputRowView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI
import Combine

struct InputRowView: View {
    let title: String
    let placeholder: String
    var maxLength: Int = 100
    var subtitle: String = ""
    var keyboardType: UIKeyboardType = .default

    @Binding var errorText: String
    @Binding var value: String

    var body: some View {
        VStack(spacing: inputsSpacing) {
            HStack {
                Text(title)
                    .font(.custom(FontFamily.SFProDisplay.bold.name, size: 24))
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                Text(subtitle)
                    .font(.custom(FontFamily.SFProDisplay.bold.name, size: 16))
                    .foregroundColor(Color(Asset.Colors.textGray.name))
                Spacer()
            }

            VStack(spacing: 4) {
                TextField(placeholder, text: $value)
                    .onReceive(Just(self.value)) { inputValue in
                        if inputValue.count > maxLength {
                            self.value.removeLast()
                        }
                    }
                    .onChange(of: value) { _ in
                        errorText = ""
                    }
                    .keyboardType(keyboardType)
                    .autocapitalization(keyboardType == .emailAddress ? .none : .words)
                    .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                    .frame(height: 20)

                Divider().background(Color(Asset.Colors.darkGreen.name))
                HStack {
                    Text(errorText)
                        .font(.custom(FontFamily.SFProDisplay.regular.name, size: 12))
                        .foregroundColor(.red)
                    Spacer()
                }
            }
        }
    }

    var inputsSpacing: CGFloat {
        return ScreenHelper().getScreenType() == .small ? 5 : 15
    }
}
