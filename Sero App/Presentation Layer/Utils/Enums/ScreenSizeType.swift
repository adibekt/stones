//
//  ScreenSizeType.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

enum ScreenSizeType {
    case small
    case medium
    case large
}
