//
//  TherapistOption.swift
//  Sero App
//
//  Created by Roman Nekliukov on 20.01.22.
//

import Foundation

enum TherapistOption {
    case requirements
    case connectToClient
    case none
}
