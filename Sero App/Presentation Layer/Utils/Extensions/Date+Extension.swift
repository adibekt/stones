//
//  DateExtension.swift
//  Sero App
//
//  Created by Таир Адибек on 22.11.2021.
//

import Foundation

extension TimeInterval {

    func string() -> String {
        let time = NSInteger(self)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        var formatString = ""
        formatString = "%0.2d:%0.2d"
        return String(format: formatString, minutes, seconds)
    }
}
extension Date {
    func seconds(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.second], from: sinceDate, to: self).second
    }
}
