//
//  AppSettings+Extenion.swift
//  Sero App
//
//  Created by Roman Nekliukov on 14.02.22.
//

import Foundation

extension AppSettings {
	static func getRole() -> UserRole {
		guard let user = currentUser else {
			return .none
		}

		return user.role
	}
}
