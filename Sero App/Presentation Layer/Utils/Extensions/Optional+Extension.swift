//
//  Optional+Extension.swift
//  Sero App
//
//  Created by tair on 29.01.22.
//

import Foundation

extension Optional where Wrapped == String {
    var orEmpty: String {
        return self ?? ""
    }
}
