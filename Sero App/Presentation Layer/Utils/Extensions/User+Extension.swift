//
//  User+Extension.swift
//  Sero App
//
//  Created by Roman Nekliukov on 3.01.22.
//

import Foundation

extension User {
    func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
}
