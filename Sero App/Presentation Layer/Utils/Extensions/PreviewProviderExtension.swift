//
//  PreviewProviderExtension.swift
//  Sero App
//
//  Created by Таир Адибек on 18.11.2021.
//

import Foundation
import SwiftUI

extension PreviewProvider {
    static func getRouter() -> MainRouter {
        return MainRouter(factory: MainFactory(isMock: true))
    }
}
