//
//  View+Extension.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

import SwiftUI

extension View {
    /// Applies the given transform if the given condition evaluates to `true`.
    /// - Parameters:
    ///   - condition: The condition to evaluate.
    ///   - transform: The transform to apply to the source `View`.
    /// - Returns: Either the original `View` or the modified `View` if the condition is `true`.
    @ViewBuilder func `if`<Content: View>(_ condition: Bool, transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }

    var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }

    var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }

    var screenType: ScreenSizeType {
        return ScreenHelper().getScreenType()
    }

    var isSmallScreen: Bool {
        return ScreenHelper().getScreenType() == .small
    }

    var isMediumScreen: Bool {
        return ScreenHelper().getScreenType() == .medium
    }

    var isLargeScreen: Bool {
        return ScreenHelper().getScreenType() == .large
    }

    func haptic(with style: UIImpactFeedbackGenerator.FeedbackStyle) {
        let generator = UIImpactFeedbackGenerator(style: style)
        generator.impactOccurred()
    }

    func hideKeyboard() {
        let resign = #selector(UIResponder.resignFirstResponder)
        UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
    }
}
struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCorner(radius: radius, corners: corners) )
    }
}

extension UIView {
    func find<T: UIView>(for type: T.Type, maxLevel: Int = 3) -> T? {
        guard maxLevel >= 0 else {
            return nil
        }

        if let view = self as? T {
            return view
        } else {
            for view in subviews {
                if let found = view.find(for: type, maxLevel: maxLevel - 1) {
                    return found
                }
            }
        }

        return nil
    }
}
