//
//  RoundedBorderButton.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

import SwiftUI

struct RoundedBorderButtonModifier: ViewModifier {
    let borderColor: Color
    func body(content: Content) -> some View {
        content
            .overlay(
                RoundedRectangle(cornerRadius: 33)
                    .stroke(borderColor, lineWidth: 0.5)
            )
    }
}
