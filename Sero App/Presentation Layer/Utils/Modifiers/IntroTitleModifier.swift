//
//  IntroTitleModifier.swift
//  Sero App
//
//  Created by Roman Nekliukov on 9.11.21.
//

import SwiftUI

struct IntroTitleModifier: ViewModifier {
    @Binding var titleVisible: Bool

    func body(content: Content) -> some View {
        content
            .font(.custom(FontFamily.OrpheusPro.regular.name, size: 38))
            .foregroundColor(.white)
            .opacity(titleVisible ? 1 : 0)
    }
}
