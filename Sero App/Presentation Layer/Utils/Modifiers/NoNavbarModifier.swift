//
//  NoNavbarModifier.swift
//  Sero App
//
//  Created by Roman Nekliukov on 9.11.21.
//

import Foundation

import SwiftUI

struct NoNavbarModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarTitle("")
            .navigationBarHidden(true)
    }
}
