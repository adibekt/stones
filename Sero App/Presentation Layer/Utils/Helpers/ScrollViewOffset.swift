//
//  ScrollViewOffset.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

class ScrollViewOffset: ObservableObject {
    @Published var offset: CGFloat = 0
}
