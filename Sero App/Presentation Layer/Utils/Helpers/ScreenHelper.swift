//
//  ScreenHelper.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

import SwiftUI

final class ScreenHelper {
    func getScreenType() -> ScreenSizeType {
        let height = UIScreen.main.bounds.height
        if height < 700 {
            return .small
        } else if height < 900 {
            return .medium
        }

        return .large
    }
}
