//
//  Stone.swift
//  Sero App
//
//  Created by Таир Адибек on 20.11.2021.
//

import Foundation
import CoreBluetooth

struct Stone {
    var name: String
    var peripheral: CBPeripheral
}
