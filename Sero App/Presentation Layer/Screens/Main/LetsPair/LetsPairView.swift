//
//  LetsPairView.swift
//  Sero App
//
//  Created by Таир Адибек on 16.11.2021.
//

import SwiftUI
import CoreBluetooth
import Introspect

struct LetsPairView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var appState: AppState
    @EnvironmentObject var clientState: ClientState
    @StateObject var viewModel: LetsPairViewModel
    let popUpHeight: CGFloat = {
        let screenSize = ScreenHelper().getScreenType()
        switch screenSize {
        case .small: return 340.0
        case .medium: return 390.0
        case .large: return 410.0
        }
    }()

    var body: some View {
        ZStack {
            // This empty navigation link is required, because in case of
            // multiple navigation links on the screen, swiftui without any reason
            // automatically pops recently added view.
            // https://developer.apple.com/forums/thread/670267?commentId=4300025&page=1#684115022
            NavigationLink(destination: EmptyView()) {
                EmptyView()
            }

            NavigationLink(destination: viewModel.router.toConnectToTherapistView(),
                           isActive: $clientState.enterConnectionMode) {
            }

			NavigationLink(destination: viewModel.router.toRoleMainView(role: AppSettings.getRole()),
                           isActive: $viewModel.navigateToRoleMainScreen) {
            }

            Color(Asset.Colors.darkGreen.name)
                .edgesIgnoringSafeArea(.top)

            VStack {
                HStack {
                    Button(action: { presentationMode.wrappedValue.dismiss() },
                           label: {
                               Image(systemName: "chevron.backward")
                                   .font(.system(size: 20, weight: .bold))
                                   .foregroundColor(.white)
                    })
                    .frame(width: 10, height: 20, alignment: .center)
                    Spacer()
                    if AppSettings.currentUser?.role == .client {
                        Text(L10n.Common.Actions.signout)
                            .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                            .foregroundColor(.white)
                            .onTapGesture {
                                viewModel.signOut {
                                    self.appState.isAuthorized = false
                                }
                            }
                    }
                }
                Spacer()
            }
            .padding(EdgeInsets(top: 10, leading: 30, bottom: 0, trailing: 30))

            VStack {
                Spacer()
                LetsPairBodyView()
                StonesPopupView(viewModel: viewModel, setNavigationStateClosure: {
                    clientState.enterConnectionMode = true
                })
            }

            if viewModel.showSuccessModal {
                viewModel.router.toSuccessfullPairScreen()
                    .transition(.move(edge: .trailing))
                    .animation(.easeInOut(duration: 0.30))
            }
        }
        .modifier(NoNavbarModifier())
        .onAppear {
            BLEService.shared.reset()
            viewModel.showSuccessModal = false
            viewModel.clear()
            viewModel.retrieveFoundperipherals()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.viewModel.rescan()
            }
        }
        .onReceive(clientState.$enterConnectionMode, perform: { value in
            print("client state changed = \(value)")
        })
    }
}

struct LetsPairBodyView: View {
    var body: some View {
        VStack(alignment: .center, spacing: 13) {
            HStack {
                Image(Asset.Images.appIconGray.name)
                    .resizable()
                    .frame(width: 50, height: 50, alignment: .center)
                    .padding(.leading, 30)
                    .foregroundColor(Color(Asset.Colors.buttonDarkGray.name))
                    .opacity(0.1)
                Spacer()
            }

            HStack {
                Text(L10n.LetsPair.letsPair)
                    .foregroundColor(.white)
                    .padding(.leading, 30)
                    .font(.custom(FontFamily.OrpheusPro.regular.name, size: 36))
                    .lineSpacing(10)
                Spacer()
            }.padding(.vertical, 15)

            HStack {
                Image(Asset.Images.awesomeBluetooth.name)
                    .resizable()
                    .frame(width: 16, height: 20, alignment: .center)
                    .padding(.leading, 30)
                    .foregroundColor(Color(Asset.Colors.buttonDarkGray.name))

                Text(L10n.LetsPair.makeSureBluetoothIsOn)
                    .foregroundColor(Color(Asset.Colors.buttonDarkGray.name))
                    .font(.custom(FontFamily.SFProDisplay.regular.name, size: 16))
                Spacer()
            }
        }.padding(.bottom, 30)
    }
}

struct StonesView: View {
    var stones: StonePage
    var onStoneTap: ((Stone) -> Void)
    @ObservedObject var viewModel: LetsPairViewModel
    @State private var didTapFirstStone: Bool = false
    @State private var didTapSecondStone: Bool = false

    init(stones: StonePage, viewModel: LetsPairViewModel, onStoneTap: @escaping ((Stone) -> Void)) {
        self.stones = stones
        self.onStoneTap = onStoneTap
        self.viewModel = viewModel
    }

    var body: some View {
        HStack {
            EachStoneView(stone: stones.objects.0, viewModel: viewModel) { stone in
                onStoneTap(stone)
            }

            EachStoneView(stone: stones.objects.1, viewModel: viewModel) { stone in
                onStoneTap(stone)
            }
        }.frame(width: UIScreen.main.bounds.width, height: 203, alignment: .center)
    }
}

struct EachStoneView: View {
    var stone: Stone?
    var onStoneTap: ((Stone) -> Void)
    @ObservedObject var viewModel: LetsPairViewModel
    @State private var didTap = false

    init(stone: Stone?, viewModel: LetsPairViewModel, onStoneTap: @escaping ((Stone) -> Void)) {
        self.stone = stone
        self.viewModel = viewModel
        self.onStoneTap = onStoneTap
    }

    var body: some View {
        ZStack {
            if stone?.peripheral == nil {
                Image(Asset.Images.emptyStone.name)
                    .resizable()
                    .frame(width: 107, height: 153, alignment: .center)
                    .padding(5)
            } else {
                Image(Asset.Images.stone.name)
                    .resizable()
                    .frame(width: 107, height: 153, alignment: .center)
                    .padding(5)
                    .opacity(didTap ? 1 : 0.4)
                    .onTapGesture {
                        guard let stone = stone else {
                            return
                        }
                        self.didTap.toggle()
                        self.onStoneTap(stone)
                    }
                    .onAppear {
                        didTap = false
                    }
            }

            Image(Asset.Images.appIconGray.name)
                .resizable()
                .frame(width: 28, height: 28, alignment: .center)
                .foregroundColor(Color(Asset.Colors.buttonDarkGray.name))
                .opacity(stone?.peripheral == nil ? 0.8 : 0.0)

            if let peripheral = stone?.peripheral {
                Text("#" + peripheral.identifier.uuidString)
                    .foregroundColor(.white)
                    .font(.system(size: 6))
                    .frame(width: 80, height: 40, alignment: .bottom)
                    .padding(.top, 40)
            }
        }
        .onAppear {
            let selectedStone = viewModel.selectedStones.first(where: { $0.identifier == stone?.peripheral.identifier })
            didTap = selectedStone != nil
        }
    }
}

struct StonesPopupView: View {
    @ObservedObject var viewModel: LetsPairViewModel
    @State private var actionTitle: String = L10n.LetsPair.rescan
    let setNavigationStateClosure: VoidClosure

    var body: some View {
        VStack {
            // Header text
            VStack(spacing: 8) {
                Text(L10n.LetsPair.tapEachStone)
                    .foregroundColor(Color(Asset.Colors.customBlack.name))
                    .font(.custom(FontFamily.SFProDisplay.regular.name, size: 20))

                Button {
                    viewModel.canGoToNextScreen = false
                    viewModel.rescan()
                    let rescanAproxDurationSeconds = 5.0
                    actionTitle = L10n.LetsPair.scanning
                    DispatchQueue.main.asyncAfter(deadline: .now() + rescanAproxDurationSeconds) {
                        actionTitle = L10n.LetsPair.rescan
                    }

                } label: {
                    Text(actionTitle)
                        .foregroundColor(Color(Asset.Colors.customBlack.name))
                        .font(.custom(FontFamily.SFProDisplay.regular.name, size: 16))
                }
            }

            // Stones
            TabView {
                EnumeratedForEach(viewModel.objects) { (_, element) in
                    StonesView(stones: element, viewModel: viewModel,
                               onStoneTap: { stone in
                         haptic(with: .medium)
                         viewModel.select(stone: stone.peripheral)
                    }).padding(.bottom, 30)
                }
            }
            .introspectViewController { viewController in
                if let collectionView = viewController.view.find(for: UICollectionView.self) {
                    collectionView.alwaysBounceVertical = false
                }

                if let pagedControl = viewController.view.find(for: UIPageControl.self) {
                    pagedControl.isUserInteractionEnabled = false
                }
            }
            .tabViewStyle(.page)
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            .id(viewModel.objects.count)
            .frame(height: 240)
            .onAppear {
                setupAppearance()
            }

            // Connect button
            Button(action: {
                self.viewModel.connect {
					if AppSettings.getRole() == .client {
                        setNavigationStateClosure()
                    } else {
                        viewModel.navigateToRoleMainScreen = true
                    }
                }
                haptic(with: .medium)
            }, label: {
                Text(L10n.LetsPair.connect)
                    .frame(width: 254, height: 54, alignment: .center)
                    .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                    .foregroundColor(viewModel.canGoToNextScreen
                                     ? Color.white
                                     : Color(Asset.Colors.darkGreen.name))
                    .background(viewModel.canGoToNextScreen
                                ? Color(Asset.Colors.darkGreen.name)
                                : Color.white)
                    .cornerRadius(30)
                    .overlay(
                        RoundedRectangle(cornerRadius: 30)
                            .stroke(Color(Asset.Colors.darkGreen.name), lineWidth: 2)
                    )
            }).allowsHitTesting(viewModel.canGoToNextScreen)
        }
        .padding(EdgeInsets(top: 20, leading: 0, bottom: isSmallScreen ? 18 : 0, trailing: 0))
        .background(
            Color(.white)
                .cornerRadius(30, corners: [.topLeft, .topRight])
        )
    }

    func setupAppearance() {
        UIPageControl.appearance().currentPageIndicatorTintColor = .black
    }
}

struct LetsPairView_Previews: PreviewProvider {
    static var previews: some View {
        LetsPairView(viewModel: LetsPairViewModel(
            router: getRouter(),
            firebaseAuthenticationService: FirebaseAuthenticationService(
                usersRepository: UsersRepository(),
                imagesRepository: ImagesRepository()))).previewDevice("iPhone 8")

        LetsPairView(viewModel: LetsPairViewModel(
            router: getRouter(),
            firebaseAuthenticationService: FirebaseAuthenticationService(
                usersRepository: UsersRepository(),
                imagesRepository: ImagesRepository()))).previewDevice("iPhone 13")
    }
}

struct EnumeratedForEach<ItemType, ContentView: View>: View {
    let data: [ItemType]
    let content: (Int, ItemType) -> ContentView

    init(_ data: [ItemType], @ViewBuilder content: @escaping (Int, ItemType) -> ContentView) {
        self.data = data
        self.content = content
    }

    var body: some View {
        ForEach(Array(self.data.enumerated()), id: \.offset) { idx, item in
            self.content(idx, item)
        }
    }
}
