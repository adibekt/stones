//
//  StonePage.swift
//  Sero App
//
//  Created by Таир Адибек on 20.11.2021.
//

import Foundation

struct StonePage: Identifiable {
    var id: String {
        (objects.0?.name ?? "") + (objects.1?.name ?? "")
    }
    var objects: (Stone?, Stone?)
}
