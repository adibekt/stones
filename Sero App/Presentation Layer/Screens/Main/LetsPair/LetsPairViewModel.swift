//
//  LetsPairViewModel.swift
//  Sero App
//
//  Created by Таир Адибек on 16.11.2021.
//

import Foundation
import SwiftUI
import CoreBluetooth
import RxSwift

class LetsPairViewModel: ObservableObject {
    var router: MainRouter
    let bag = DisposeBag()
    var selectedStones: [CBPeripheral] = []
    @Published var objects: [StonePage] = [StonePage(objects: (nil, nil)), StonePage(objects: (nil, nil))]
    @Published var navigateToRoleMainScreen: Bool = false
    @Published var showSuccessModal: Bool = false
    @Published var canGoToNextScreen: Bool = false

    let firebaseAuthenticationService: FirebaseAuthenticationService

    init(router: MainRouter, firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.router = router
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func retrieveFoundperipherals() {
        DispatchQueue.main.async {
            BLEService.shared.peripherals
                .subscribe(onNext: { peripherals in
                    if peripherals.count > 0 {
                        self.objects = []
                    }
                    peripherals.forEach { peripheral in
                        let stone = Stone(name: peripheral.identifier.uuidString,
                                          peripheral: peripheral)
                        if var semiEmptyObject = self.objects.enumerated()
                            .first(where: { $0.element.objects.0 != nil
                                && $0.element.objects.1 == nil }) {
                            semiEmptyObject.element.objects.1 = stone
                            self.objects.removeAll(where: {
                                $0.objects.0?.peripheral.identifier ==
                                semiEmptyObject.element.objects.0?.peripheral.identifier
                            })
                            self.objects.append(semiEmptyObject.element)
                        } else {
                            let stonePage = StonePage(objects: (stone, nil))
                            self.objects.append(stonePage)
                        }
                    }
                }).disposed(by: self.bag)
        }
    }

    func rescan() {
        canGoToNextScreen = false
        selectedStones.removeAll()
        DispatchQueue.main.async {
            self.objects = [StonePage(objects: (nil, nil))]
        }
        BLEService.shared.rescan()
    }

    func select(stone: CBPeripheral) {
        if selectedStones.contains(stone) {
            selectedStones.removeAll(where: { $0.identifier.uuidString == stone.identifier.uuidString })
            canGoToNextScreen = false
        } else {
            if selectedStones.count > 1 {
                selectedStones.remove(at: 0)
            }
            selectedStones.append(stone)
            if selectedStones.count == 2 {
                canGoToNextScreen = true
            }
        }
    }

    func clear() {
        objects = [StonePage(objects: (nil, nil))]
        selectedStones = []
        BLEService.shared.clear()
    }

    func connect(navigationOptionClosure: @escaping VoidClosure) {
        if canGoToNextScreen {
            BLEService.shared.setUpPair(with: (first: selectedStones[0], second: selectedStones[1]))
            BLEService.shared.connectToSelectedStones()

            self.showSuccessModal = true

            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                navigationOptionClosure()
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                self.showSuccessModal = false
            }
        }
    }

    func signOut(navigateToAuth: @escaping VoidClosure) {
        firebaseAuthenticationService.signOut { result in
            switch result {
            case .success:
                AppSettings.authorizationType = nil
                AppSettings.currentUser = nil
                navigateToAuth()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
