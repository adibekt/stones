//
//  RemoteSessionData.swift
//  Sero App
//
//  Created by Roman Nekliukov on 30.12.21.
//

import Foundation

struct RemoteSessionData {
    var therapist: User
}
