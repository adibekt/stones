//
//  ConnectToTherapistView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.12.21.
//

import SwiftUI

struct ConnectToTherapistView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var viewModel: ConnectToTherapistViewModel
    @State private var shareCodeContentVisible = false

    var body: some View {
        ZStack {
            NavigationLink(destination: viewModel.router.toClientDashboardScreen(
                therapistName: viewModel.getFullTherapistName()),
                           isActive: $viewModel.navigateToClientSession) { }

            LinearGradient(gradient: Gradient(colors: [Color(Asset.Colors.oldWhite.name), .white]),
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing)
                .ignoresSafeArea()

            BackButton(action: {
                viewModel.stopListenting()
                presentationMode.wrappedValue.dismiss()
            })

            VStack(spacing: 25) {
                if viewModel.showTherapistRequestSheet && viewModel.therapistData != nil {
                    ConnectedTherapistView(
                        therapist: viewModel.therapistData!,
                        connectClosure: {
                            haptic(with: .medium)
                            viewModel.connect()
                        },
                        dismissClosure: {
                            haptic(with: .medium)
                            viewModel.dismiss()
                        }
                    )
                    .transition(.opacity)
                } else {
                    Text(L10n.ConnectToTherapistView.title)
                        .multilineTextAlignment(.center)
                        .font(.custom(FontFamily.OrpheusPro.regular.name, size: 30))
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))

                    VStack {
                        Text(L10n.ConnectToTherapistView.subtitle)
                            .multilineTextAlignment(.center)
                            .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                            .foregroundColor(Color(Asset.Colors.seroBlack.name))

                        HStack(spacing: 25) {
                            Image(Asset.Images.lock.name)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 20, height: 16)

                            if viewModel.code.isBlank {
                                ProgressView().frame(width: 140, height: 35)
                            } else {
                                Text(viewModel.code)
                                    .kerning(12)
                                    .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 32))
                                    .foregroundColor(Color(Asset.Colors.seroBlack.name))
                                    .frame(width: 140, height: 35)
                            }
                        }
                        .frame(maxWidth: 230)
                        .padding(10)
                        .background(Color(Asset.Colors.buttonGray.name))
                        .cornerRadius(33)
                        .opacity(shareCodeContentVisible ? 1 : 0)
                        .transition(.opacity)
                        .animation(.easeIn)
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .center)
            .padding(.horizontal, 26)
            .offset(x: 0, y: -30)
        }
        .alert(isPresented: $viewModel.hasError) {
            Alert(
                title: Text(L10n.Common.Errors.title),
                message: Text(viewModel.errorText),
                dismissButton: .default(Text(L10n.Login.tryAgain))
            )
        }
        .onDisappear {
            viewModel.code = ""
        }
        .onAppear {
            viewModel.initConnectionRequest()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                shareCodeContentVisible = true
            }
        }
        .modifier(NoNavbarModifier())
    }
}

struct ConnectToTherapistView_Previews: PreviewProvider {
    static var previews: some View {
        ConnectToTherapistView(viewModel: ConnectToTherapistViewModel(
            router: getRouter(),
            therapistRequestRepository: TherapistRequestRepository()))
    }
}
