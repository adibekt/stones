//
//  ConnectedTherapistView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 22.12.21.
//

import SwiftUI
import URLImage

struct ConnectedTherapistView: View {
    @State private var imageSaved = false
    @State private var imageScale: CGFloat = 1
    let therapist: User
    let connectClosure: VoidClosure
    let dismissClosure: VoidClosure

    var body: some View {
        VStack(spacing: 20) {
            if therapist.imageUrl.isEmpty {
                ProfileDefaultImage()
            } else {
                URLImage(URL(string: therapist.imageUrl)!,
                         empty: { ProfileDefaultImage() },
                         inProgress: { _ in
                            ActivityIndicator()
                                .frame(width: 100, height: 100, alignment: .center) },
                         failure: { _, _ in ProfileDefaultImage() },
                         content: { image in
                            image
                                .resizable()
                                .scaledToFill()
                                .frame(width: 100, height: 100, alignment: .center)
                                .cornerRadius(20)
                })
            }

            Text(L10n.ConnectToTherapistView.request(getTherapistName()))
                .multilineTextAlignment(.center)
                .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                .foregroundColor(Color(Asset.Colors.seroBlack.name))

            Button(action: {
                connectClosure()
            }, label: {
                Text(L10n.ConnectToTherapistView.connect)
                    .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                    .foregroundColor(.white)
                    .frame(maxWidth: 230)
                    .padding(20)
                    .background(
                        Color(Asset.Colors.darkGreen.name)
                            .cornerRadius(33)
                            .scaleEffect(imageScale)
                            .onAppear {
                                let baseAnimation = Animation.easeInOut(duration: 1)
                                let repeated = baseAnimation.repeatForever(autoreverses: true)
                                withAnimation(repeated) {
                                    imageScale = 0.95
                                }
                            }
                    )
            })

            Button(action: {
                dismissClosure()
            }, label: {
                Text(L10n.ConnectToTherapistView.dismiss)
                    .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                    .frame(maxWidth: 230)
                    .padding(5)
                    .background(Color(.clear))
                    .buttonStyle(PlainButtonStyle())
            })
        }
    }

    private func getTherapistName() -> String {
        if therapist.firstName.isBlank && therapist.lastName.isBlank {
            return L10n.TherapistOption.therapist
        }

        return "\(therapist.firstName) \(therapist.lastName)"
    }
}

struct ProfileDefaultImage: View {
    var size: CGFloat = 100
    var cornerRadius: CGFloat = 20

    var body: some View {
        Image(Asset.Images.defaultUserImage.name)
            .resizable()
            .scaledToFill()
            .frame(width: size, height: size, alignment: .center)
            .background(Color(Asset.Colors.buttonGray.name))
            .cornerRadius(cornerRadius)
    }
}

struct ConnectedTherapistView_Previews: PreviewProvider {
    static var previews: some View {
        ConnectedTherapistView(therapist: User(id: "", email: "",
                                               firstName: "", lastName: "",
                                               phoneNumber: "", provider: ""),
                               connectClosure: { print("connect") },
                               dismissClosure: { print("dismiss") }
        )
    }
}
