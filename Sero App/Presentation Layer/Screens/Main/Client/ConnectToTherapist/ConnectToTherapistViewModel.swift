//
//  ConnectToTherapistViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.12.21.
//

import Foundation
import SwiftUI

final class ConnectToTherapistViewModel: ObservableObject {
    @Published var therapistData: User?
    @Published var showTherapistRequestSheet = false
    @Published var navigateToClientSession = false
    @Published var code = ""
    @Published var errorText = ""
    @Published var hasError = false

    let router: MainRouter
    private let therapistRequestRepository: ITherapistRequestRepository
    private let localSessionService: ITherapistSessionService = TherapistLocalSessionService.shared()

    init(router: MainRouter, therapistRequestRepository: ITherapistRequestRepository) {
        self.router = router
        self.therapistRequestRepository = therapistRequestRepository
    }

    func showConnectedTherapist() {
        showTherapistRequestSheet = true
    }

    func connect() {
        guard let patientId = AppSettings.currentUser?.id else {
            print("Current user Id not found")
            return
        }

        let successClosure = { [weak self] in
            self?.navigateToClientSession = true
            self?.therapistRequestRepository.stopListening()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                withAnimation {
                    self?.showTherapistRequestSheet = false
                }
            }
        }

        let failedClosure: StringClosure = { [weak self] errorMessage in
            print(errorMessage)
            self?.hasError = true
            self?.errorText = L10n.Common.Errors.clientConnectionError
        }
        if let therapyID =  localSessionService.initEmptySession()?.documentID {
            therapistRequestRepository.confirmFromPatient(patientId: patientId,
                                                          code: code,
                                                          therapyID: therapyID,
                                                          completed: successClosure,
                                                          failed: failedClosure)
        }
    }

    func stopListenting() {
        therapistRequestRepository.stopListening()
    }

    func dismiss() {
        let failedClosure: StringClosure = { [weak self] errorMessage in
            print(errorMessage)
            self?.hasError = true
            self?.errorText = L10n.Common.Errors.dismissError
        }

        let successClosure: VoidClosure = { [weak self] in
            withAnimation {
                self?.showTherapistRequestSheet = false
            }
        }

        therapistRequestRepository.dismissTherapist(code: code,
                                                    completed: successClosure,
                                                    failed: failedClosure)
    }

    func initConnectionRequest() {
        let expirationSeconds = 60.0
        let date = Date().addingTimeInterval(expirationSeconds)

        guard let patientId = AppSettings.currentUser?.id else {
            return
        }

        let connectionRequest = TherapistConnectionRequest(patientId: patientId, expiresAt: date)
        let failedClosure: StringClosure = { [weak self] errorMessage in
            print(errorMessage)
            self?.hasError = true
            self?.errorText = L10n.Common.Errors.unableToCreateRoom
        }

        let createSuccessClosure: StringClosure = { [weak self] accessCode in
            let listenSuccessClosure: UserClosure = { [weak self] user in
                guard let user = user else {
                    return
                }

                self?.showTherapistRequestSheet = true
                self?.therapistData = user
            }

            self?.code = accessCode
            self?.therapistRequestRepository.listenForConnection(accessCode: accessCode,
                                                                 completed: listenSuccessClosure,
                                                                 failed: failedClosure)
        }

        therapistRequestRepository.createRequest(connectionRequest: connectionRequest,
                                                 completed: createSuccessClosure,
                                                 failed: failedClosure)
    }

    func getFullTherapistName() -> String {
        guard let therapist = therapistData else {
            return ""
        }

        return therapist.getFullName()
    }
}
