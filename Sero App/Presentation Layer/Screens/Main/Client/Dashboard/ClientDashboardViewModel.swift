//
//  ClientDashboardViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 22.12.21.
//

import Foundation
import Combine
import RxRelay
import RxSwift

final class ClientDashboardViewModel: ObservableObject {
    var viewDismissalModePublisher = PassthroughSubject<Bool, Never>()

    @Published var alertText: String = L10n.ClientDashboard.AlertText.finished
    @Published var alertTitle: String = L10n.Main.success
    @Published var showAlert = false
    @Published var connectionRequestData: TherapistConnectionRequest?
    let router: MainRouter
    let therapistName: String
    var shouldGoBack = false
    private let therapistRequestRepository: ITherapistRequestRepository
    private var shouldCheckTherapistsOnline = true
    private var isBusy = false
    private let repo: IRemoteSessionRepository
    private var isSessionStarted = false
    private var isOnPause = true
    private var shouldCheckErrors = true
    private let bag = DisposeBag()
    private let sessionService: ITherapistSessionService = TherapistLocalSessionService.shared()
    private var lastUpdatedTime: Date?
    private var sessionStopped = false {
        didSet {
            shouldCheckErrors = false
            viewDismissalModePublisher.send(sessionStopped)
        }
    }
    private var lastIntensity: UInt8?
    private var lastDuration: UInt16?

    init(router: MainRouter, therapistName: String,
         therapistRequestRepository: ITherapistRequestRepository,
         repository: IRemoteSessionRepository) {
        self.router = router
        self.therapistName = therapistName.isBlank
            ? L10n.TherapistOption.therapist
            : therapistName
        self.therapistRequestRepository = therapistRequestRepository
        self.repo = repository
    }

    func stopSession() {
        sessionService.finishSession()
        sessionStopped = true
        shouldGoBack = true
        self.alertTitle = L10n.Main.success
        self.alertText = L10n.Main.sessionDone
        self.showAlert = true
    }

    func reset() {
        repo.reset()
        BLEService.shared.reset()
    }

    func startObserving() {
        shouldCheckErrors = true
        NetworkAccessibilityService.shared().startObserving()
        BLEService.shared.onError = { _ in
            self.showConnectionInterruptedError()
        }
        guard let sessionID = sessionService.getCurrentSession()?.documentID else {
            return
        }
        let relay: BehaviorRelay<CommandsQueue?> = repo.observe(path: RemoteCommandsService.path,
                                                                filter: (key: "therapySessionID", value: sessionID))
        relay.subscribe { [weak self] commandsQueue in
            guard let command = commandsQueue else {
                return
            }
            self?.handleUpdates(with: command)
            self?.observeQueue(with: command)
            if let date = command.lastUpdated {
                self?.handleLastUpdatedTime(date: date)
            }
            if command.isInBackground ?? false {
                self?.shouldCheckTherapistsOnline = false
            } else {
                self?.shouldCheckTherapistsOnline = true
            }
        } onError: { error in
            print(error.localizedDescription)
        }.disposed(by: bag)
    }

    func cleanupCreatedRoom(completion: @escaping VoidClosure) {
        guard let patientId = AppSettings.currentUser?.id else {
            print("Unable to get current user Id. Cleanup failed.")
            return
        }

        therapistRequestRepository.clearAccessCodes(patientId: patientId) {
            completion()
        }
    }

    private func observeQueue(with command: CommandsQueue) {
        repo.observeQueue(in: command).subscribe { [weak self] queue in
            queue.forEach { command in
                self?.handleQueue(with: command)
            }
        } onError: { error in
            print(error.localizedDescription)
        }.disposed(by: bag)
    }

    private func showConnectionInterruptedError() {
        if !self.shouldCheckErrors {
            return
        }
        self.sessionService.connectionInterrupted(with: L10n.Main.connectionInterrupted)
        self.alertText = L10n.Main.connectionInterrupted
        self.alertTitle = L10n.Main.warning
        self.shouldGoBack = true
        self.showAlert = true
    }

    private func handleUpdates(with command: CommandsQueue) {
        if isSessionStarted {
            guard let intensity = command.currentIntensity,
                  let duration = command.currentDuration,
                  (lastDuration != duration || lastIntensity != intensity) else {
                        return
                    }
            lastIntensity = intensity
            lastDuration = duration
            sessionService.addInterval(intensity: intensity, duration: duration)
        } else {
            guard let intensity = command.currentIntensity,
                  let duration = command.currentDuration
                    else {
                        return
                    }
            lastIntensity = intensity
            lastDuration = duration
            _ = sessionService.startSession(intensity: intensity, duration: duration, connection: nil)
            isSessionStarted = true
            isOnPause = false
        }
    }

    private func handleQueue(with command: RemoteSessionCommand) {
        if !isBusy {
            isBusy = true
            switch command.action {
            case .startSet:
                sessionService.addSet(intensity: command.newIntensity!, duration: command.newDuration!)
                isOnPause = false
            case .stopSet:
                sessionService.stopSet()
                isOnPause = true
            case .finishSession:
                sessionService.finishSession()
                self.alertTitle = L10n.Main.success
                self.alertText = L10n.ClientDashboard.AlertText.finished
                shouldGoBack = true
                showAlert = true
                isOnPause = true
                sessionStopped = true
            default:
                print(command.action.rawValue)
            }
            repo.removeCommand(theraySessionID: (sessionService.getCurrentSession()?.documentID ?? ""),
                               commandID: command.id ?? "", deleted: {
                self.isBusy = false
            })
        }
    }

    private func handleLastUpdatedTime(date: Date) {
        lastUpdatedTime = date
        DispatchQueue.main.asyncAfter(deadline: .now()
                                      + Double(RemoteCommandsService.waitingAwakenessTime)) { [weak self] in
            guard let `self` = self else {
                return
            }

            if abs((Date().seconds(sinceDate: self.lastUpdatedTime!) ?? 0))
                >= RemoteCommandsService.waitingAwakenessTime && self.isOnPause == false
                && self.shouldCheckTherapistsOnline {
                if NetworkAccessibilityService.shared().getCurrentStatus() == .satisfied {
                    self.alertTitle = L10n.Main.warning
                    self.alertText = L10n.ClientDashboard.Alert.therapistsInternetOff
                } else {
                    self.sessionService.stopSet()
                    self.isOnPause = true
                    self.alertTitle = L10n.Main.warning
                    self.alertText = L10n.ClientDashboard.Alert.clientsInternetOff
                }
                self.showAlert = true
            }
        }
    }
}
