//
//  ClientDashboardView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 22.12.21.
//

import SwiftUI

struct ClientDashboardView: View {
    @EnvironmentObject var clientState: ClientState
    @StateObject var viewModel: ClientDashboardViewModel

    var body: some View {
        ZStack {
            Image(Asset.Images.cloudsBackground.name)
                .resizable()
                .scaledToFill()
                .frame(width: screenWidth, height: screenHeight, alignment: .center)
                .ignoresSafeArea()

            VStack(spacing: 35) {
                Image(Asset.Images.logo.name)
                    .resizable()
                    .frame(width: logoSize, height: logoSize, alignment: .center)
                    .foregroundColor(Color(Asset.Colors.seroBlue.name))

                Text(L10n.ClientDashboard.title(viewModel.therapistName))
                    .multilineTextAlignment(.center)
                    .lineSpacing(5)
                    .font(.custom(FontFamily.OrpheusPro.regular.name, size: titleFontSize))
                    .foregroundColor(Color(Asset.Colors.seroBlack.name))
            }
            .frame(maxWidth: .infinity, alignment: .center)
            .padding(.horizontal, 16)
            .offset(x: 0, y: centerContentOffset)

            VStack {
                Spacer()
                Button(
                    action: { viewModel.stopSession() },
                    label: {
                        Text(L10n.ClientDashboard.exitSession)
                            .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                            .foregroundColor(.white)
                            .padding(20)
                            .frame(maxWidth: .infinity)
                            .background(Color(Asset.Colors.darkGreen.name))
                            .cornerRadius(18)
                    }
                )
            }.padding(EdgeInsets(top: 0, leading: 16, bottom: isSmallScreen ? 25 : 32, trailing: 16))
        }
        .modifier(NoNavbarModifier())
        .alert(isPresented: $viewModel.showAlert) {
            Alert(
                title: Text(viewModel.alertTitle),
                message: Text(viewModel.alertText),
                dismissButton: .default(Text("Ok"), action: {
                    if viewModel.shouldGoBack {
                        clientState.enterConnectionMode = false
                    }
                })
            )
        }
        .onAppear {
            AudioPlayer.shared.playBackgroundMusic()
            viewModel.startObserving()
        }
        .onDisappear {
            AudioPlayer.shared.pauseBackgroundMusic()
            viewModel.reset()
        }
    }
}

private extension ClientDashboardView {
    var centerContentOffset: CGFloat {
        return isLargeScreen ? -140 : -90
    }

    var titleFontSize: CGFloat {
        return isLargeScreen ? 38 : 32
    }

    var logoSize: CGFloat {
        return isLargeScreen ? 55 : 45
    }
}

struct ClientDashboardView_Previews: PreviewProvider {
    static var previews: some View {
        ClientDashboardView(viewModel: ClientDashboardViewModel(
            router: getRouter(),
            therapistName: "Ivan Stalovskiy",
            therapistRequestRepository: TherapistRequestRepository(), repository: RemoteSessionRepository()))
    }
}
