//
//  RoleView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 11.12.21.
//

import SwiftUI

struct RoleView: View {
    private let horizontalSpacing = 20.0

    @EnvironmentObject var appState: AppState
    @StateObject var viewModel: RoleViewModel

    var body: some View {
        NavigationView {
            ZStack {
                LinearGradient(gradient: Gradient(colors: [Color(Asset.Colors.oldWhite.name), .white]),
                               startPoint: .topLeading,
                               endPoint: .bottomTrailing)
                    .ignoresSafeArea()

                VStack(alignment: .leading, spacing: 45) {
                    Image(Asset.Images.logo.name)
                        .resizable()
                        .frame(width: 80, height: 80)
                        .opacity(0.05)
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))

                    Text(L10n.Role.title)
                        .font(.custom(FontFamily.OrpheusPro.regular.name, size: 60))
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))

                    VStack(spacing: 20) {
                        RoleButton(action: {
                            viewModel.selectRole(userRole: .client) {
                                appState.roleSelected = true
                            }
                        },
                        role: .client,
                        width: screenWidth - horizontalSpacing * 2,
                        height: buttonHeight)
                    RoleButton(
                        action: {
                            viewModel.selectRole(userRole: .therapist) {
                                appState.roleSelected = true
                            }
                        },
                        role: .therapist,
                        width: screenWidth - horizontalSpacing * 2,
                        height: buttonHeight)
                    }
                }
                .padding(.horizontal, horizontalSpacing)

                ActivityLoader().opacity(viewModel.isBusy ? 1 : 0)
            }
            .alert(isPresented: $viewModel.hasError) {
                Alert(
                    title: Text(L10n.Role.Error.title),
                    message: Text(viewModel.errorText),
                    dismissButton: .default(Text(L10n.Login.tryAgain))
                )
            }
        }
        .navigationViewStyle(.stack)
        .modifier(NoNavbarModifier())
    }
}

private extension RoleView {
    var buttonHeight: CGFloat {
        return screenType == .small ? 90 : 120
    }
}

struct RoleButton: View {
    let action: VoidClosure
    let role: UserRole
    let width: CGFloat
    let height: CGFloat

    var body: some View {
        Button(action: {
                action()
            }, label: {
                HStack(spacing: 40) {
                    Image(role.imageName)
                        .resizable()
                        .frame(width: height, height: height, alignment: .center)
                        .cornerRadius(18)
                    Text(role.title)
                        .font(.custom(FontFamily.OrpheusPro.regular.name, size: 36))
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))
                }
                .padding(20)
                .frame(width: width, alignment: .leading)
                .overlay(
                    RoundedRectangle(cornerRadius: 18)
                        .stroke(.black, lineWidth: 1))
            }
        )
    }
}

struct RoleView_Previews: PreviewProvider {
    static var previews: some View {
        RoleView(viewModel: RoleViewModel(router: getRouter(),
                                          usersRepository: UsersRepository()))
            .previewDevice("iPhone 8")

        RoleView(viewModel: RoleViewModel(router: getRouter(),
                                          usersRepository: UsersRepository()))
            .previewDevice("iPhone 12")
    }
}
