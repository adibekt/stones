//
//  RoleViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 12.12.21.
//

import Foundation

final class RoleViewModel: ObservableObject {
    let router: MainRouter
    let usersRepository: UsersRepository

    @Published var isBusy: Bool = false
    @Published var selectedRole: UserRole = .none
    @Published var errorText: String = ""
    @Published var hasError: Bool = false

    init(router: MainRouter, usersRepository: UsersRepository) {
        self.router = router
        self.usersRepository = usersRepository
    }

    func selectRole(userRole: UserRole, completion: @escaping VoidClosure) {
        isBusy = true
        guard let userId = AppSettings.currentUser?.id else {
            isBusy = false
            hasError = true
            return
        }

        let userUpdateModel = UserUpdate(id: userId, role: userRole)
        let successClosure: VoidClosure = { [weak self] in
            self?.isBusy = false
            self?.selectedRole = userRole
			guard let currentUser = AppSettings.currentUser else {
				return
			}

			currentUser.role = userRole
			AppSettings.currentUser = currentUser
            completion()
        }

        let failClosure: StringClosure = { [weak self] errorText in
            self?.errorText = errorText.isBlank ? L10n.Role.Error.text : errorText
            self?.isBusy = false
            self?.hasError = true
        }

        usersRepository.updateUser(user: userUpdateModel,
                                   completed: successClosure,
                                   failed: failClosure)
    }
}
