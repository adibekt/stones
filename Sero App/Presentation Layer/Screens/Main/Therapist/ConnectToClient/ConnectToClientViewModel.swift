//
//  ConnectToClientViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 24.12.21.
//

import Foundation

final class ConnectToClientViewModel: ObservableObject {
    @Published var code = ""
    @Published var errorText = ""
    @Published var requestPressed = false
    @Published var hasError = false
    @Published var navigateToDashboard = false

    let router: MainRouter
    var connectionResult: ConnectionResult?
    private let therapistRequestRepository: ITherapistRequestRepository

    init(router: MainRouter, therapistRequestRepository: ITherapistRequestRepository) {
        self.router = router
        self.therapistRequestRepository = therapistRequestRepository
    }

    func requestConnection(code: String) {
        guard let therapistId = AppSettings.currentUser?.id else {
            print("Current user Id not found")
            return
        }

        let successClosure = { [weak self] in
            let acceptedClosure: ConnectionResultClosure = { (result: ConnectionResult) in
                self?.connectionResult = result
                self?.therapistRequestRepository.stopListening()
                self?.navigateToDashboard = true
            }

            let dismissClosure: VoidClosure = {
                self?.requestPressed = false
            }

            let listenFailed: StringClosure = { [weak self] errorMessage in
                print(errorMessage)
                self?.hasError = true
                self?.errorText = L10n.Common.Errors.clientConnectionError
            }

            self?.therapistRequestRepository.listenForAcceptance(
                code: code,
                accepted: acceptedClosure,
                dismissed: dismissClosure,
                failed: listenFailed)
        }

        let failedClosure: StringClosure = { [weak self] errorMessage in
            print(errorMessage)
            self?.hasError = true
            self?.errorText = L10n.Common.Errors.noActiveRooms
        }

        therapistRequestRepository.confirmFromTherapist(
            therapistId: therapistId,
            code: code,
            completed: successClosure,
            failed: failedClosure)
    }

    func cleanupData() {
        navigateToDashboard = false
        hasError = false
        code = ""
    }
}
