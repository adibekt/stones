//
//  ConnectToClientView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 22.12.21.
//

import SwiftUI
import Introspect
import Combine

struct ConnectToClientView: View {
    private let codeLength = 4
    @State private var keyboardShowDelaySeconds = 0.6
    @State private var showKeyboard = true
    @State private var isRequestAllowed = false

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var viewModel: ConnectToClientViewModel

    var body: some View {
        ZStack {
            NavigationLink(
                destination: viewModel.router.toRoleMainView(role: AppSettings.getRole(),
                                                             with: viewModel.connectionResult),
                isActive: $viewModel.navigateToDashboard) { }

            TextField("", text: $viewModel.code)
                .introspectTextField { textField in
                    if !viewModel.requestPressed {
                        DispatchQueue.main.asyncAfter(deadline: .now() + keyboardShowDelaySeconds) {
                            if self.showKeyboard {
                                textField.becomeFirstResponder()
                                self.keyboardShowDelaySeconds = 0.0
                            }
                        }
                    } else {
                        textField.resignFirstResponder()
                    }
                }
                .onReceive(Just(viewModel.code)) { inputValue in
                    isRequestAllowed = viewModel.code.count == codeLength
                    if inputValue.count > codeLength {
                        viewModel.code.removeLast()
                    }
                }
                .keyboardType(.numberPad)
                .opacity(0)

            LinearGradient(gradient: Gradient(colors: [Color(Asset.Colors.oldWhite.name), .white]),
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing)
                .ignoresSafeArea()

            BackButton(action: {
                presentationMode.wrappedValue.dismiss()
                showKeyboard = false
            })

            VStack(spacing: 30) {
                Text(L10n.ConnectToClient.title)
                    .multilineTextAlignment(.center)
                    .font(.custom(FontFamily.OrpheusPro.regular.name, size: 36))
                    .foregroundColor(Color(Asset.Colors.seroBlack.name))

                VStack(spacing: 30) {
                    Text(L10n.ConnectToClient.enterCode)
                        .multilineTextAlignment(.center)
                        .font(.custom(FontFamily.SFProDisplay.regular.name, size: isSmallScreen ? 22 : 24))
                        .foregroundColor(Color(Asset.Colors.seroBlack.name))

                    HStack(spacing: 10) {
                        ForEach(0..<codeLength, id: \.self) { index in
                            CodeDigitView(digitText: getCurrentDigit(code: viewModel.code, index: index))
                        }
                    }.padding(.horizontal, 15)

                    Button(action: {
                        if isRequestAllowed {
                            viewModel.requestPressed = true
                            viewModel.requestConnection(code: viewModel.code)
                        }
                    }, label: {
                        Text(viewModel.requestPressed
                                ? L10n.ConnectToClient.waiting
                                : L10n.ConnectToClient.request
                        )
                            .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity)
                            .padding(.vertical, 15)
                            .background(Color(getRequestButtonBgColorName())
                                            .cornerRadius(35))
                    }).allowsHitTesting(isRequestAllowed && !viewModel.requestPressed)
                }
                .padding(EdgeInsets(top: 30, leading: 25, bottom: 30, trailing: 25))
                .frame(maxWidth: .infinity, maxHeight: 240, alignment: .center)
                .background(Color(Asset.Colors.buttonGray.name).cornerRadius(15))

                Spacer()
            }
            .padding(EdgeInsets(top: 60, leading: 30, bottom: 0, trailing: 30))
        }
        .ignoresSafeArea(.keyboard)
        .alert(isPresented: $viewModel.hasError) {
            Alert(
                title: Text(L10n.Common.Errors.title),
                message: Text(viewModel.errorText),
                dismissButton: .default(Text(L10n.Login.tryAgain),
                action: {
                    isRequestAllowed = false
                    viewModel.requestPressed = false
                })
            )
        }
        .onAppear {
            viewModel.requestPressed = false
            isRequestAllowed = false
            showKeyboard = true
            keyboardShowDelaySeconds = 0.6
            viewModel.cleanupData()
        }
        .modifier(NoNavbarModifier())
    }

    func getRequestButtonBgColorName() -> String {
        if isRequestAllowed {
            return viewModel.requestPressed
                ? Asset.Colors.buttonDarkGray.name
                : Asset.Colors.darkGreen.name
        } else {
            return Asset.Colors.lightGray.name
        }
    }

    func getCurrentDigit(code: String, index: Int) -> String {
        if code.isBlank || index > code.count - 1 {
            return "-"
        }

        let start = code.startIndex
        let current = code.index(start, offsetBy: index)

        return String(code[current])
    }
}

struct CodeDigitView: View {
    let emptyDigitText = "-"
    let digitText: String

    var body: some View {
        VStack {
            Text(digitText)
                .font(.custom(FontFamily.SFProDisplay.bold.name, size: 36))
                .foregroundColor(Color(Asset.Colors.darkGreen.name))
                .opacity(digitText == emptyDigitText ? 0.1 : 1)

            Capsule()
                .fill(Color(Asset.Colors.textUnderlineGray.name))
                .frame(height: 2)
                .offset(x: 0, y: -15)
        }
    }
}
