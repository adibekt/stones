//
//  TherapistOptionView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.12.21.
//

import SwiftUI

struct TherapistOptionView: View {
    @EnvironmentObject var appState: AppState
    @EnvironmentObject var therapistState: TherapistOptionState
    @StateObject var viewModel: TherapistOptionViewModel

    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.white, Color(Asset.Colors.oldWhite.name)]),
                           startPoint: .bottom,
                           endPoint: .top)
                .ignoresSafeArea()

            NavigationLink(
                destination: viewModel.router.toRequirementsView(),
                tag: TherapistOption.requirements,
                selection: $therapistState.option) { }

            NavigationLink(
                destination: viewModel.router.toConnectToClientView(),
                tag: TherapistOption.connectToClient,
                selection: $therapistState.option) { }

            VStack {
                HStack {
                    Spacer()
                    Text(L10n.Common.Actions.signout)
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))
                        .onTapGesture {
                            viewModel.signOut {
                                self.appState.isAuthorized = false
                            }
                        }
                }
                Spacer()
            }
            .padding(.horizontal, 16)

            VStack {
                VStack(spacing: 24) {
                    VStack(spacing: 15) {
                        Text(L10n.TherapistOption.greeting(viewModel.userName))
                            .font(.custom(FontFamily.OrpheusPro.regular.name, size: 36))
                            .foregroundColor(Color(Asset.Colors.darkGreen.name))
                        Text(L10n.TherapistOption.question)
                            .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 20))
                            .foregroundColor(Color(Asset.Colors.darkGreen.name))
                    }

                    VStack(spacing: 16) {
                        TransparentButton(isActive: $viewModel.connectToMyStones,
                                          isEnabled: .constant(true),
                                          text: L10n.TherapistOption.connectToStones,
                                          action: { viewModel.selectMyStones() })
                        TransparentButton(isActive: $viewModel.connectToClientsStones,
                                          isEnabled: .constant(true),
                                          text: L10n.TherapistOption.connectToClientsStones,
                                          action: { viewModel.selectClientsStones() })
                    }
                }

                Spacer()
                TransparentButton(isActive: $viewModel.nextButtonEnabled,
                                  isEnabled: $viewModel.nextButtonEnabled,
                                  text: L10n.TherapistOption.next,
                                  action: {
                    therapistState.option = viewModel.connectToMyStones
                        ? TherapistOption.requirements
                        : TherapistOption.connectToClient
                })
            }
            .padding(EdgeInsets(top: 40, leading: 16, bottom: 10, trailing: 16))
        }
        .onAppear {
            viewModel.getUsername()
            therapistState.option = TherapistOption.none
        }
        .modifier(NoNavbarModifier())
    }
}

struct TransparentButton: View {
    @Binding var isActive: Bool
    @Binding var isEnabled: Bool
    let text: String
    let action: VoidClosure

    var body: some View {
        Button(
            action: { action() },
            label: {
                if isActive {
                    Text(text)
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                        .padding(20)
                        .background(Color(Asset.Colors.darkGreen.name))
                        .cornerRadius(13)
                } else {
                    Text(text)
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                        .foregroundColor(Color(Asset.Colors.seroBlack.name))
                        .frame(maxWidth: .infinity)
                        .padding(20)
                        .background(Color(.clear))
                        .buttonStyle(PlainButtonStyle())
                        .overlay(RoundedRectangle(cornerRadius: 13)
                                    .stroke(.black, lineWidth: 1)
                                    .allowsHitTesting(false)
                        )
                }
            }
        ).allowsHitTesting(isEnabled)
    }
}

struct TherapistOptionView_Previews: PreviewProvider {
    static var previews: some View {
        TherapistOptionView(viewModel: TherapistOptionViewModel(
            router: getRouter(),
            firebaseAuthenticationService: FirebaseAuthenticationService(
                usersRepository: UsersRepository(),
                imagesRepository: ImagesRepository()))).previewDevice("iPhone 13 mini")

        TherapistOptionView(viewModel: TherapistOptionViewModel(
            router: getRouter(),
            firebaseAuthenticationService: FirebaseAuthenticationService(
                usersRepository: UsersRepository(),
                imagesRepository: ImagesRepository()))).previewDevice("iPhone 13 Pro Max")

        TherapistOptionView(viewModel: TherapistOptionViewModel(
            router: getRouter(),
            firebaseAuthenticationService: FirebaseAuthenticationService(
                usersRepository: UsersRepository(),
                imagesRepository: ImagesRepository()))).previewDevice("iPhone 8")

    }
}
