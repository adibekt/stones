//
//  TherapistOptionViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.12.21.
//

import Foundation

final class TherapistOptionViewModel: ObservableObject {
    private let firebaseAuthenticationService: FirebaseAuthenticationService
    let router: MainRouter

    @Published var connectToMyStones = false
    @Published var connectToClientsStones = false
    @Published var nextButtonEnabled = false
    @Published var userName = ""

    init(router: MainRouter, firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.router = router
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func selectMyStones() {
        if connectToMyStones {
            connectToMyStones = false
            nextButtonEnabled = false
            return
        }

        connectToMyStones = true
        connectToClientsStones = false
        nextButtonEnabled = true
    }

    func selectClientsStones() {
        if connectToClientsStones {
            connectToClientsStones = false
            nextButtonEnabled = false
            return
        }

        connectToMyStones = false
        connectToClientsStones = true
        nextButtonEnabled = true
    }

    func signOut(navigateToAuth: @escaping VoidClosure) {
        firebaseAuthenticationService.signOut { result in
            switch result {
            case .success:
                AppSettings.authorizationType = nil
                AppSettings.currentUser = nil
                navigateToAuth()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func getUsername() {
        guard let firstName = AppSettings.currentUser?.firstName else {
            userName = L10n.TherapistOption.therapist
            return
        }

        if firstName.isEmpty {
            userName = L10n.TherapistOption.therapist
            return
        }

        userName = firstName
    }
}
