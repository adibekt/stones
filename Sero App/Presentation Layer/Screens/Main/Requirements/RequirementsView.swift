//
//  WelcomeView.swift
//  Sero App
//
//  Created by Таир Адибек on 12.11.2021.
//

import SwiftUI

struct RequirementsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var viewModel: RequirementsViewModel
    let imageSize: CGFloat = {
        let screenSize = ScreenHelper().getScreenType()
        return screenSize == .large ? 67.0 : 58.0
    }()

    let topConstraint: CGFloat = {
        let screenSize = ScreenHelper().getScreenType()
        switch screenSize {
        case .small: return 60.0
        case .medium: return 145.0
        case .large: return 185.0
        }
    }()

    var body: some View {
        ZStack(alignment: .top) {
            Color(Asset.Colors.darkGreen.name)
                .ignoresSafeArea()

            NavigationLink(destination: viewModel.router.toLetsPairView()
                            .modifier(NoNavbarModifier()),
                           isActive: $viewModel.navigateToLetsPair) {
            }

            if AppSettings.getRole() == .therapist {
                BackButton(color: .white, action: {
                    presentationMode.wrappedValue.dismiss()
                })
            }

            VStack(alignment: .center, spacing: 20) {
                Image(Asset.Images.logo.name)
                    .resizable()
                    .frame(width: imageSize, height: imageSize, alignment: .center)
                    .foregroundColor(Color.white)
                    .padding(.top, topConstraint)

                Text(L10n.Welcome.welcomeToSero)
                    .font(.custom(FontFamily.OrpheusPro.regular.name, size: 38))
                    .foregroundColor(.white)

                Spacer()
                RequirementsVStackView(onButtonTap: viewModel.checkIfBluetoothIsOn)
            }.alert(isPresented: $viewModel.hasError) {
                Alert(
                    title: Text(L10n.Welcome.alertTitle),
                    message: Text(L10n.Welcome.alertMessege),
                    dismissButton: .default(Text(L10n.Common.Actions.ok))
                )
            }
            .onAppear {
                BLEService.shared.initializeDelegates()
            }
            .modifier(NoNavbarModifier())
        }
    }
}

struct RequirementsVStackView: View {
    var onButtonTap: VoidClosure

    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Text(L10n.Welcome.beforeWeStart)
                .font(.custom(FontFamily.SFProDisplay.regular.name, size: 22))
                .foregroundColor(.white)

            ListOfRequirementsView()
                .padding(.vertical, 15)
                .background(Color.white.opacity(0.05))
                .cornerRadius(14)

            Button(action: {
                onButtonTap()
                haptic(with: .medium)
            }, label: {
                Text(L10n.General.done)
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                    .frame(width: 254, height: 54, alignment: .center)
                    .background(Color.white)
                    .cornerRadius(33)
                    .padding(EdgeInsets(top: 24, leading: 0, bottom: 18, trailing: 0))
            })
        }
    }

    init(onButtonTap: @escaping (() -> Void)) {
        self.onButtonTap = onButtonTap
    }
}

struct ListOfRequirementsView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            RequirementsRowView(text: L10n.Welcome.chargeStones,
                                icon: Asset.Images.ionicMdBatteryCharging.name)
                .padding(.top, 6)

            RequirementsRowView(text: L10n.Welcome.turnBluetoothOn,
                                icon: Asset.Images.awesomeBluetooth.name)

            RequirementsRowView(text: L10n.Welcome.acceptPermissions,
                                icon: Asset.Images.checkIcon.name)
                .padding(.bottom, 6)
        }.padding(.horizontal, 34)
    }
}

struct RequirementsRowView: View {
    var text, icon: String
    var body: some View {
        HStack(alignment: .center, spacing: 15) {
            Image(icon)
                .frame(width: 15, height: 29, alignment: .center)
                .foregroundColor(.white)

            Text(text)
                .font(.custom(FontFamily.SFProDisplay.regular.name, size: 18))
                .foregroundColor(.white)
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        RequirementsView(viewModel: RequirementsViewModel(router: MainRouter(factory: MainFactory())))
    }
}
