//
//  WelcomeViewModel.swift
//  Sero App
//
//  Created by Таир Адибек on 12.11.2021.
//

import SwiftUI

final class RequirementsViewModel: ObservableObject {
    var router: MainRouter
    @Published var navigateToLetsPair: Bool = false
    @Published var hasError = false

    func checkIfBluetoothIsOn() {
        BLEService.shared.startScanning { error in
            guard error != nil else {
                self.navigateToLetsPair = true
                return
            }
            self.hasError = true
            self.navigateToLetsPair = false
        }
    }

    init(router: MainRouter) {
        self.router = router
    }
}
