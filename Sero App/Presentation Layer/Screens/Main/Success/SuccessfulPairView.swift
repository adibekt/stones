//
//  SuccessfulPairView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.12.21.
//

import SwiftUI

struct SuccessfulPairView: View {
    let router: MainRouter

    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [Color(Asset.Colors.oldWhite.name), .white]),
                           startPoint: .topLeading,
                           endPoint: .bottomTrailing)
                .ignoresSafeArea()

            Text(L10n.SuccessfulPair.title)
                .multilineTextAlignment(.center)
                .font(.custom(FontFamily.OrpheusPro.regular.name, size: 30))
                .foregroundColor(Color(Asset.Colors.seroBlack.name))
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity, alignment: .center)
                .offset(x: 0, y: -50)

        }
        .modifier(NoNavbarModifier())
    }
}

struct SuccessfulPairView_Previews: PreviewProvider {
    static var previews: some View {
        SuccessfulPairView(router: getRouter()).previewDevice("iPhone 8")
    }
}
