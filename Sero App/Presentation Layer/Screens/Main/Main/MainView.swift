import Foundation
import SwiftUI
import RxSwift
import RxRelay
import URLImage

struct MainView: View {
    let horizontalPadding = 20.0
    @StateObject var viewModel: MainViewModel
    @EnvironmentObject var therapistState: TherapistOptionState
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            MainBackgroundGradient()

            VStack {
                VStack(spacing: 20) {
                    MainNavigationView(viewModel: viewModel)
                    TherapySessionInfoView(viewModel: viewModel)
                }

                Spacer()

                VStack(spacing: 60) {
                    VStack(spacing: 20) {
                        SliderView(title: L10n.Main.intensity,
                                   width: screenWidth - horizontalPadding * 2.0,
                                   handleValueUpdate: { value in
                            viewModel.strengthValue = UInt8(value)
                            viewModel.updateValues(strength: viewModel.strengthValue,
                                                   duration: viewModel.durationValue)
                        })
                        .allowsHitTesting(viewModel.isUserInteractionEnabled())
                        .opacity(viewModel.isUserInteractionEnabled() ? 1 : 0.5)

                        SliderView(title: L10n.Main.speed,
                                   width: screenWidth - horizontalPadding * 2.0,
                                   handleValueUpdate: { value in
                            viewModel.durationValue = UInt16(value)
                            viewModel.updateValues(strength: viewModel.strengthValue,
                                                   duration: viewModel.durationValue)
                        })
                        .allowsHitTesting(viewModel.isUserInteractionEnabled())
                        .opacity(viewModel.isUserInteractionEnabled() ? 1 : 0.5)
                    }

                    StartSetButton(
                        setStarted: $viewModel.setStarted,
                        action: {
                            viewModel.onSetPressed(
                                strength: viewModel.strengthValue,
                                duration: viewModel.durationValue)
                    })
                }
            }
            .padding(.horizontal, horizontalPadding)
        }
        .onAppear {
            viewModel.setObservers()
            AudioPlayer.shared.playBackgroundMusic(volume: 0.0)
        }
        .alert(isPresented: $viewModel.showAlert) {
            Alert(
                title: Text(viewModel.alertTitle.orEmpty),
                message: Text(viewModel.alertText.orEmpty),
                dismissButton: .default(Text("Ok"), action: {
                    viewModel.errorShown {
                        if viewModel.shouldGoBack {
                            therapistState.option = TherapistOption.none
                        }
                    }
                })
            )
        }
        .onDisappear {
            viewModel.reset()
            AudioPlayer.shared.pauseBackgroundMusic()
        }
        .modifier(NoNavbarModifier())
    }
}

struct MainNavigationView: View {
    @StateObject var viewModel: MainViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            Image(Asset.Images.logo.name).resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .foregroundColor(Color(Asset.Colors.darkGreen.name))

            HStack {
                Spacer()
                Button {
                    haptic(with: .light)
                    viewModel.finishSession()
                } label: {
                    Text("Finish")
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 17))
                        .foregroundColor(.black)
                }
            }
        }.frame(maxWidth: .infinity, maxHeight: 30)
    }
}

struct TherapySessionInfoView: View {
    @StateObject var viewModel: MainViewModel

    var body: some View {
        VStack(spacing: 26) {
            HStack {
                EndUserInfoView(name: viewModel.fullname, imageUrl: viewModel.clientImageUrl)
                Spacer()
                BatteryStateView(viewModel: viewModel)
            }

            HStack(spacing: 16) {
                SetsAndPairsView(viewModel: viewModel)
                ElapsedTimeView(viewModel: viewModel)
            }

            Spacer()
        }
    }
}

struct BatteryStateView: View {
    @StateObject var viewModel: MainViewModel
    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: 15) {
                StoneView(isVibrating: $viewModel.isFirstStoneVibrating)
                StoneView(isVibrating: $viewModel.isSecondStoneVibrating)
            }

            HStack {
                Image(Asset.Images.batteryIcon.name)
                    .resizable()
                    .frame(width: 27, height: 21, alignment: .center)
                Text(viewModel.batteryLevel ?? "-")
                    .font(.custom(FontFamily.ProximaNova.regular.name, size: 14))
                    .foregroundColor(.black)
            }
        }
        .frame(maxWidth: .infinity, alignment: .center)
    }
}

struct EndUserInfoView: View {
    let name: String
    let imageUrl: String

    var body: some View {
        VStack(spacing: 7) {
            if imageUrl.isEmpty {
                ProfileDefaultImage(size: 60, cornerRadius: 18)
            } else {
                URLImage(URL(string: imageUrl)!,
                         empty: { ProfileDefaultImage(size: 60, cornerRadius: 18) },
                         inProgress: { _ in
                            ActivityIndicator()
                        .frame(width: 60, height: 60, alignment: .center) },
                         failure: { _, _ in ProfileDefaultImage(size: 60, cornerRadius: 18) },
                         content: { image in
                            image
                                .resizable()
                                .scaledToFill()
                                .frame(width: 60, height: 60, alignment: .center)
                                .cornerRadius(18)
                })
            }

            Text(name)
                .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                .foregroundColor(Color(Asset.Colors.dashboardTextBlack.name))
        }
        .frame(maxWidth: .infinity, alignment: .center)
    }
}

struct SetsAndPairsView: View {
    @StateObject var viewModel: MainViewModel
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 18)
                .foregroundColor(Color(Asset.Colors.dashboardBlockGray.name))
                .frame(maxWidth: .infinity, maxHeight: 80)

            HStack(spacing: 25) {
                VStack(spacing: 3) {
                    Text("\($viewModel.numberOfSets.wrappedValue)")
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 24))
                        .foregroundColor(Color(Asset.Colors.dashboardTextBlack.name))
                    Text(L10n.Main.sets)
                        .font(.custom(FontFamily.ProximaNova.regular.name, size: 15))
                        .foregroundColor(Color(Asset.Colors.textGray.name))
                }

                Rectangle()
                    .frame(width: 1.5, height: 36, alignment: .center)
                    .foregroundColor(Color(Asset.Colors.textGray.name))
                    .opacity(0.3)

                VStack(spacing: 3) {
                    Text("\($viewModel.numberOfPairs.wrappedValue)")
                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 24))
                        .foregroundColor(Color(Asset.Colors.dashboardTextBlack.name))

                    Text(L10n.Main.pairs)
                        .font(.custom(FontFamily.ProximaNova.regular.name, size: 15))
                        .foregroundColor(Color(Asset.Colors.textGray.name))
                }
            }
        }
    }
}

struct ElapsedTimeView: View {
    @StateObject var viewModel: MainViewModel
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 18)
                .foregroundColor(Color(Asset.Colors.dashboardBlockGray.name))
                .frame(maxWidth: .infinity, maxHeight: 80)

            VStack(spacing: 3) {
                Text(viewModel.sessionDuration)
                    .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 24))
                    .foregroundColor(Color(Asset.Colors.dashboardTextBlack.name))

                Text(L10n.Main.time)
                    .font(.custom(FontFamily.ProximaNova.regular.name, size: 15))
                    .foregroundColor(Color(Asset.Colors.textGray.name))
            }
        }
    }
}

struct StartSetButton: View {
    @Binding var setStarted: Bool
    let action: VoidClosure

    var body: some View {
        Button {
            haptic(with: .soft)
            action()
        } label: {
            HStack {
                Image(setStarted
                        ? Asset.Images.pauseIcon.name
                        : Asset.Images.playIcon.name)
                    .resizable()
                    .frame(width: 20, height: 23, alignment: .center)
                    .foregroundColor(.white)
                Text(setStarted ? L10n.Main.complete : L10n.Main.start)
                    .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 18))
                    .foregroundColor(.white)
            }.frame(maxWidth: .infinity, maxHeight: 60)
                .background(Color(Asset.Colors.darkGreen.name))
                .cornerRadius(18)
                .if(screenType == .small) { content in
                    content.padding(.bottom, 15)
                }
        }
    }
}

struct SliderView: View {
    private let maxValue = 10.0

    @State private var sliderWidth: CGFloat = 0.0
    @State private var lastDragValue: CGFloat = 0.0
    @State private var previousValue: Int = 0
    @State private var value: Int = 0
    @State private var titleTextColor = Color(Asset.Colors.textGray.name)
    @State private var valueTextColor = Color(Asset.Colors.textGray.name)

    let title: String
    let width: CGFloat
    let initialValue: Int = 3
    let handleValueUpdate: IntClosure

    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: 18)
                .fill(Color(Asset.Colors.dashboardBlockGray.name))
            RoundedRectangle(cornerRadius: 18)
                .fill(Color(Asset.Colors.darkGreen.name))
                .opacity(0.82)
                .frame(width: sliderWidth)
            Text(title)
                .font(.custom(FontFamily.SFProDisplay.regular.name, size: 16))
                .foregroundColor(.white)
                .colorMultiply(titleTextColor)
                .frame(maxWidth: .infinity)
            Text("\(Int(value))")
                .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 20))
                .foregroundColor(.white)
                .colorMultiply(valueTextColor)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding(.trailing, 15)
        }

        .frame(width: width, height: 60)
        .gesture(DragGesture(minimumDistance: 0).onChanged({ (slideValue) in
            let translation = slideValue.translation
            var calculatedWidth = translation.width + lastDragValue
            calculatedWidth = calculatedWidth > width ? width : calculatedWidth
            calculatedWidth = calculatedWidth >= 0 ? calculatedWidth : 0
            let percentage = calculatedWidth * 100 / width
            if percentage <= maxValue {
                return
            }

            sliderWidth = calculatedWidth
            value = Int(percentage / maxValue)
            if value != previousValue {
                previousValue = value
                // Uncomment the line below to make slider change by steps
                // sliderWidth = maxWidth * CGFloat(value) / 10
                haptic(with: .medium)
                withAnimation {
                    titleTextColor = value > 5 ? .white : Color(Asset.Colors.textGray.name)
                    valueTextColor = value == 10 ? .white : Color(Asset.Colors.textGray.name)
                }
            }
        }).onEnded({ _ in
            sliderWidth = sliderWidth > width ? width : sliderWidth
            sliderWidth = sliderWidth >= 0 ? sliderWidth : 0
            lastDragValue = sliderWidth
            handleValueUpdate(value)
        }))
        .onAppear {
            previousValue = initialValue
            value = initialValue
            sliderWidth = (CGFloat(initialValue) * width) / maxValue
            lastDragValue = sliderWidth
            handleValueUpdate(value)
        }
    }
}

struct StoneView: View {
    @Binding var isVibrating: Bool?
    var body: some View {
        ZStack {
            Image(Asset.Images.stone.name)
                .resizable()
                .frame(width: 40, height: 60, alignment: .center)
                .shadow(color: ($isVibrating.wrappedValue ?? false)
                                    ? Color(Asset.Colors.lightGreen.name)
                                    : .clear,
                        radius: 8)

            LinearGradient(gradient: Gradient(colors: [
                Color(Asset.Colors.logoTopGray.name),
                Color(Asset.Colors.logoBottomGray.name)
            ]), startPoint: .top, endPoint: .bottom)
                .frame(width: 12, height: 12, alignment: .center)
                .mask(Image(Asset.Images.logo.name)
                        .resizable()
                        .frame(width: 12, height: 12, alignment: .center)
                )
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(viewModel: MainViewModel(
            router: getRouter())).previewDevice("iPhone 13")
        MainView(viewModel: MainViewModel(
            router: getRouter())).previewDevice("iPhone 8")
    }
}
