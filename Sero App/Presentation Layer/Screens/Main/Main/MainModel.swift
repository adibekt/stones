//
//  MainModel.swift
//  Sero App
//
//  Created by Таир Адибек on 22.11.2021.
//

import Foundation
import FirebaseFirestore
import Firebase

class MainModel {

    static private var share = MainModel()
    static func shared() -> MainModel {
        return share
    }
    var currentSession: TherapySession?
    let firebaseRepo = SessionsRepository.shared()
    let infoFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss"
        return dateFormatter
    }()
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()

    func setStarted(strength: UInt8, duration: UInt16) {
        guard currentSession != nil else {
            self.initNewSession(strength: strength, duration: duration)
            return
        }
        addNewSet(strength: strength, duration: duration)
    }

    func addNewInterval(strength: UInt8, duration: UInt16) {
        guard let set = currentSession?.sets.last else {
            return
        }
        completeLatestInterval()
        let newInterval = Interval(documentID: getNewID(), intensity: "\(strength)",
                                   speed: "\(duration)", startTime: Date(), timeElapsed: "0")
        set.intervals.append(newInterval)
        currentSession?.currentIntensity = "\(strength)"
        currentSession?.currentSpeed = "\(duration)"
        updateCurrentSessionInDB()
    }

    func increasePairsCount() {
        guard let set = currentSession?.sets.last else {
            return
        }
        set.pairsCount += 1
        let difference = set.startTime.distance(to: Date()).string()
        set.timeElapsed = difference
        updateCurrentSessionInDB()
    }

    func completeLatestInterval() {
        guard let currentInterval = currentSession?.sets.last?.intervals.last else {
            return
        }
        let difference = currentInterval.startTime.distance(to: Date()).string()
        currentInterval.timeElapsed = difference
        updateCurrentSessionInDB()
    }

    func completeLatestSet() {
        guard let set = currentSession?.sets.last else {
            return
        }
        let difference = set.startTime.distance(to: Date()).string()
        set.timeElapsed = difference
        updateCurrentSessionInDB()
    }

    func reset() {
        currentSession = nil
    }

    func getLastTherapy() -> TherapySession? {
        guard let data = AppSettings.lastTherapy,
              let loadedSession = try? decoder.decode(TherapySession.self, from: data) else {
            return nil
        }
        return loadedSession
    }

    private func addNewSet(strength: UInt8, duration: UInt16) {
        guard let session = currentSession else {
            return
        }
        let interval = Interval(documentID: getNewID(), intensity: "\(strength)",
                                speed: "\(duration)",
                                startTime: Date(),
                                timeElapsed: "0")
        let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                      startTime: Date(),
                      timeElapsed: "0",
                      intervals: [interval], pairsCount: 0)
        session.sets.append(set)
        updateCurrentSessionInDB()
    }

    private func initNewSession(strength: UInt8, duration: UInt16) {
        let interval = Interval(documentID: getNewID(), intensity: "\(strength)",
                                speed: "\(duration)",
                                startTime: Date(),
                                timeElapsed: "0")
        let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                      startTime: Date(),
                      timeElapsed: "0", intervals: [interval],
                      pairsCount: 0)
        currentSession = TherapySession(documentID: getNewID(), currentIntensity: "\(strength)",
                                        currentSpeed: "\(duration)",
                                        stoneId: BLEService.shared.getStoneId(),
                                        sets: [set], userID: AppSettings.currentUser?.id ?? "")
        updateCurrentSessionInDB()
    }

    private func updateCurrentSessionInDB() {
        guard let currentSession = currentSession else {
            return
        }
        if let encoded = try? encoder.encode(currentSession) {
            AppSettings.lastTherapy = encoded
        }
    }

    private func getNewID() -> String {
        return UUID().uuidString
    }
}
