//
//  MainViewModel.swift
//  Sero App
//
//  Created by Таир Адибек on 19.11.2021.
//

import Foundation
import SwiftUI
import RxRelay
import RxSwift
import Network

final class MainViewModel: ObservableObject {
    private let bag = DisposeBag()
    private let sessionService: ITherapistSessionService

    private var ignoreInternetConnectionCheck = false
    private var sessionStarted = false
    private var model = MainModel.shared()
    private var currentSession = BehaviorRelay<TherapySession?>(value: nil)
    private var connectionResult: ConnectionResult?
    private var isRemoteSession: Bool
    private var lastUpdatedTime: Date?

    @Published var isClientsInternetDied = false
    @Published var isTherapistsInternetDied = false
    @Published var showAlert = false
    @Published var alertTitle: String?
    @Published var alertText: String?
    @Published var numberOfPairs = 0
    @Published var numberOfSets = 0
    @Published var setStarted = false
    @Published var sessionDuration = "00:00"
    @Published var batteryLevel: String?
    @Published var isFirstStoneVibrating: Bool?
    @Published var isSecondStoneVibrating: Bool?
    @Published var fullname: String = ""
    @Published var clientImageUrl: String = ""

    var router: MainRouter
    var strengthValue: UInt8 = 5
    var durationValue: UInt16 = 5
    var errorString: String?
    var shouldGoBack = false

    init(router: MainRouter, with connectionResult: ConnectionResult? = nil) {
        self.router = router
        strengthValue = UInt8(model.getLastTherapy()?.currentIntensity ?? "5") ?? 5
        durationValue = UInt16(model.getLastTherapy()?.currentSpeed ?? "5000") ?? 5000
        isRemoteSession = connectionResult != nil

        if isRemoteSession {
            sessionService = TherapistRemoteSessionService.shared()
            currentSession = sessionService.observeCurrentSession(connection: connectionResult!)
            self.connectionResult = connectionResult
            UsersRepository().getUser(id: connectionResult?.patientID ?? "") { [weak self] user in
                if let user = user {
                    self?.fullname = user.getFullName()
                    self?.clientImageUrl = user.imageUrl
                }
            }
        } else {
            sessionService = TherapistLocalSessionService.shared()
            fullname = (AppSettings.currentUser?.getFullName() ?? "")
            clientImageUrl = (AppSettings.currentUser?.imageUrl ?? "")
        }

        NetworkAccessibilityService.shared().startObserving()
        observeCurrentSession()
    }

    /// Handling on `complete` or `start` set button pressed
    func onSetPressed(strength: UInt8, duration: UInt16) {
        let networkConnectionSucceeded = validateNetworkConnection()
        if !networkConnectionSucceeded {
            return
        }

        if setStarted {
            sessionService.stopSet()
            setStarted = false
        } else {
            setStarted = true
            if sessionStarted {
                sessionService.addSet(intensity: strength, duration: getDurationInMs(duration))
            } else {
                currentSession = sessionService.startSession(
                    intensity: strength,
                    duration: getDurationInMs(duration),
                    connection: connectionResult)

                if !isRemoteSession {
                    observeCurrentSession()
                }
            }

            sessionStarted = true
        }

        resetUI()
    }

    /// Handle changing values on `Speed` or `Intensity` sliders
    func updateValues(strength: UInt8, duration: UInt16) {
        let networkConnectionSucceeded = validateNetworkConnection()
        if !networkConnectionSucceeded {
            return
        }

        if setStarted {
            (strengthValue, durationValue) = (strength, duration)
            sessionService.addInterval(intensity: strength, duration: getDurationInMs(duration))
        }
    }

    /// Handle Finish button pressed
    func finishSession() {
        sessionService.finishSession()
        alertTitle = L10n.Main.success
        if !isRemoteSession {
            alertText = isTherapistsInternetDied
                ? L10n.Main.successWithoutConnection
                : L10n.Main.sessionDone
        } else {
            alertText = L10n.Main.sessionDone
        }

        shouldGoBack = true
        showAlert = true
        sessionStarted = false
        numberOfSets = 0
        numberOfPairs = 0
        sessionDuration = "00:00"
    }

    func errorShown(completion: @escaping VoidClosure) {
        showAlert = false
        alertText = nil
        alertTitle = nil
        completion()
    }

    func reset() {
        resetUI()
        sessionService.reset()
        model.reset()
        if !isRemoteSession {
            BLEService.shared.reset()
        }
    }

    func isUserInteractionEnabled() -> Bool {
        if isRemoteSession {
            let internetConnectionLost = NetworkAccessibilityService.shared().getCurrentStatus() == .unsatisfied
            return !internetConnectionLost && !isClientsInternetDied
        } else {
            return true
        }
    }

    private func validateNetworkConnection() -> Bool {
        // We do not validate internet connection for therapist local sesstion
        if isRemoteSession {
            let internetConnectionLost = NetworkAccessibilityService.shared().getCurrentStatus() == .unsatisfied
            if  internetConnectionLost || isClientsInternetDied {
                showInternetError(isClientSide: isClientsInternetDied)
                return false
            }
        }

        return true
    }

    private func setSignOutFlags() {
        sessionService.finishSession()
        resetUI()
        AppSettings.authorizationType = nil
    }

    private func resetUI() {
        sessionDuration = "00:00"
        numberOfPairs = 0
    }

    private func showInternetError(isClientSide: Bool) {
        self.alertTitle = L10n.Main.warning
        if isClientSide {
            self.alertText = L10n.MainView.Alert.clientsInternetOff
        } else {
            self.alertText = L10n.MainView.Alert.therapistsInternetOff
        }

        self.showAlert = true
    }

    private func showAlert(title: String, text: String, shouldGoback: Bool) {
        alertTitle = title
        alertText = text
        self.shouldGoBack = shouldGoback
        showAlert = true
    }

    private func getDurationInMs(_ value: UInt16) -> UInt16 {
        return value * 1000
    }

    private func getDurationInMs(_ value: Int) -> UInt16 {
        return UInt16(value * 1000)
    }

    private func getDurationInSeconds(_ value: UInt16) -> Double {
        return Double(value / 1000)
    }
}

// Observers
extension MainViewModel {
    func setObservers() {
        if !isRemoteSession {
            BLEService.shared.onError = { [weak self] text in
                self?.showAlert(title: L10n.Main.warning, text: L10n.Main.connectionInterrupted, shouldGoback: true)
                self?.errorString = text
                self?.finishSession()
            }
        }

        NetworkAccessibilityService.shared().onValueChanged = { [weak self] status in
            guard let self = self else {
                return
            }

            DispatchQueue.main.async {
                self.isTherapistsInternetDied = status == .unsatisfied
                if !self.isRemoteSession && self.isTherapistsInternetDied {
                    self.showAlert(title: L10n.Main.warning,
                                   text: L10n.Main.localInternetLost,
                                   shouldGoback: false)
                }
            }
        }
    }

    private func observeCurrentSession() {
        currentSession.subscribe { [weak self] therapySession in
            guard let session = therapySession,
                  let self = self else {
                return
            }

            if self.isRemoteSession {
                self.sessionService.notifyUpdatesAcceptance()
                self.isClientsInternetDied = false
            }

            if self.setStarted {
                self.numberOfSets = session.sets.count
                self.numberOfPairs = session.sets.last?.pairsCount ?? 0
                self.sessionDuration = session.sets.last?.timeElapsed ?? "00:00"
                self.isFirstStoneVibrating = session.isFirstStoneVibrating
                self.isSecondStoneVibrating = session.isSecondStoneVibrating
                if self.isRemoteSession {
                    if let date = session.lastUpdated {
                        self.handleLastUpdatedTime(date: date)
                    }
                }
            } else {
                self.resetUI()
            }

            if session.isFinished ?? false {
                if session.errorText != nil {
                    self.showAlert(title: L10n.Main.warning,
                                    text: L10n.MainView.Alert.stonesDisabled,
                                    shouldGoback: true)
                    self.ignoreInternetConnectionCheck = true
                    return
                }

                self.showAlert(title: L10n.Main.success, text: L10n.Main.sessionDone, shouldGoback: true)
            }
            self.batteryLevel = session.batteryLevel
        } onError: { error in
            print(error.localizedDescription)
        }.disposed(by: bag)
    }

    private func handleLastUpdatedTime(date: Date) {
        if ignoreInternetConnectionCheck {
            return
        }

        lastUpdatedTime = date
        let awakenessTime = Double(RemoteCommandsService.waitingAwakenessTime)
        DispatchQueue.main.asyncAfter(deadline: .now() + awakenessTime) { [weak self] in
            guard let `self` = self else {
                return
            }

            if abs((Date().seconds(sinceDate: self.lastUpdatedTime!) ?? 0))
                >= RemoteCommandsService.waitingAwakenessTime && self.setStarted {
                if NetworkAccessibilityService.shared().getCurrentStatus() == .satisfied {
                    self.setStarted = false
                    self.resetUI()
                    self.isClientsInternetDied = true
                    self.sessionService.stopSet()
                    self.showAlert(
                        title: L10n.Main.warning,
                        text: L10n.MainView.Alert.clientsInternetOff,
                        shouldGoback: false)
                } else {
                    self.showAlert(
                        title: L10n.Main.warning,
                        text: L10n.MainView.Alert.therapistsInternetOff,
                        shouldGoback: false)
                }
            }
        }
    }
}
