//
//  CustomSliderView.swift
//  Sero App
//
//  Created by Таир Адибек on 26.12.2021.
//

import Foundation
import SwiftUI

struct CustomSlider: View {
    @Binding var percentage: Double

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle()
                    .foregroundColor(Color(Asset.Colors.newLightGray.name))
                Rectangle()
                    .foregroundColor(.accentColor)
                    .frame(width: geometry.size.width * Double(self.percentage / 10))
            }
            .cornerRadius(12)
            .gesture(DragGesture(minimumDistance: 0)
                        .onChanged({ value in
                self.percentage = min(max(0, Double(value.location.x / geometry.size.width * 10)), 10)
                print(self.percentage)
            }))
        }
    }
}
