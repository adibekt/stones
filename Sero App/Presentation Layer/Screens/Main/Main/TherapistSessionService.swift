//
//  TherapistSessionService.swift
//  Sero App
//
//  Created by Таир Адибек on 15.12.2021.
//

import Foundation
import RxRelay
import RxSwift

 protocol TherapistSessionServiceProtocol {
    func startSession(intensity: UInt8, duration: UInt16) -> BehaviorRelay<TherapySession?>
    func addSet(intensity: UInt8, duration: UInt16)
    func addInterval(intensity: UInt8, duration: UInt16)
    func stopSet()
    func finishSession()
    func addNewInterval(intensity: UInt8, duration: UInt16)
    func increaseNumberOfPairs()
    func updateElapsedTime(with secs: String)
}

class TherapistLocalSessionService: TherapistSessionServiceProtocol {

    let currentSession = BehaviorRelay<TherapySession?>(value: nil)
    var stoneService = StoneBluetoothService()
    private let bag = DisposeBag()
    private let firebaseRepo = FirebaseRepository.shared()
    static private var share = TherapistLocalSessionService()
    static func shared() -> TherapistSessionServiceProtocol {
        return share
    }

    func startSession(intensity: UInt8, duration: UInt16) -> BehaviorRelay<TherapySession?> {
        let interval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                speed: "\(duration)",
                                startTime: Date(),
                                timeElapsed: "0")
        let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                      startTime: Date(),
                      timeElapsed: "0", intervals: [interval],
                      pairsCount: 0)
        let session = TherapySession(documentID: getNewID(), currentIntensity: "\(intensity)",
                                     currentSpeed: "\(duration)",
                                     stoneId: BLEService.shared.getStoneId(),
                                     sets: [set], userID: AppSettings.userID ?? "")
        currentSession.accept(session)
        stoneService.startInterval(intensity: intensity, duration: duration)
        listenToUpdates()
        return currentSession
    }

    func addSet(intensity: UInt8, duration: UInt16) {
        guard let session = currentSession.value else {
            return
        }
        let interval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                speed: "\(duration)",
                                startTime: Date(),
                                timeElapsed: "0")
        let set = Set(documentID: getNewID(), stoneId: BLEService.shared.getStoneId(),
                      startTime: Date(),
                      timeElapsed: "0",
                      intervals: [interval], pairsCount: 0)
        session.sets.append(set)
        stoneService.startInterval(intensity: intensity, duration: duration)
        currentSession.accept(session)
    }

    /// Call this method from VM, it calls stone service's vibration
    func addInterval(intensity: UInt8, duration: UInt16) {
        guard currentSession.value != nil else {
            return
        }
        stoneService.startInterval(intensity: intensity, duration: duration)
    }

    /// Call this method from Stone service to write data to Firebase
    func addNewInterval(intensity: UInt8, duration: UInt16) {
        guard let session = currentSession.value else {
            return
        }
        completeLatestInterval()
        let newInterval = Interval(documentID: getNewID(), intensity: "\(intensity)",
                                   speed: "\(duration)", startTime: Date(), timeElapsed: "0")
        session.currentIntensity = "\(intensity)"
        session.currentSpeed = "\(duration)"
        session.sets.last?.intervals.append(newInterval)
        currentSession.accept(session)
    }

    func stopSet() {
        guard let session = currentSession.value,
              let set = session.sets.last else {
                  return
              }
        let difference = set.startTime.distance(to: Date()).string()
        set.timeElapsed = difference
        completeLatestInterval()
        currentSession.accept(session)
        stoneService.pause()
    }

    func finishSession() {
        stopSet()
        currentSession.accept(nil)
        stoneService.pause()
    }

    func increaseNumberOfPairs() {
        guard let session = currentSession.value else {
            return
        }
        session.sets.last?.pairsCount += 1
        currentSession.accept(session)
    }

    func updateElapsedTime(with secs: String) {
        guard let session = currentSession.value else {
            return
        }
        session.sets.last?.timeElapsed = secs
        currentSession.accept(session)
    }

    private func completeLatestInterval() {
        guard let session = currentSession.value,
              let currentInterval = session.sets.last?.intervals.last else {
                  return
              }
        let difference = currentInterval.startTime.distance(to: Date()).string()
        currentInterval.timeElapsed = difference
        currentSession.accept(session)
    }

    private func getNewID() -> String {
        return UUID().uuidString
    }

    private func listenToUpdates() {
        currentSession.subscribe { session in
            guard let session = session else {
                return
            }
            self.firebaseRepo.setUpSession(session)
        } onError: { error in
            print(error.localizedDescription)
        }.disposed(by: bag)

    }
}
