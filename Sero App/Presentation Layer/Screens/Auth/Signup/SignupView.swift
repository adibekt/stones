//
//  SignupView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 15.11.21.
//

import SwiftUI
import SwiftUITrackableScrollView
import RxSwift
import AVFoundation

struct SignupView: View {
    private let backButtonVisibilityLimit = 50.0

    @State var isViewAppeared: Bool = false
    @State var useCameraAsPicker: Bool = false
    @StateObject var viewModel: SignupViewModel
    @ObservedObject var scrollViewOffset = ScrollViewOffset()
    @EnvironmentObject var authState: AuthState
    @EnvironmentObject var appState: AppState
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ZStack {
            BackButton(action: { presentationMode.wrappedValue.dismiss() })
                .opacity(scrollViewOffset.offset > backButtonVisibilityLimit ? 0 : 1)
                .if(isViewAppeared, transform: { content in
                    content.animation(.easeIn(duration: 0.15))
                })
                .transition(.opacity)
                .zIndex(2)

            TrackableScrollView(.vertical, showIndicators: false,
                                contentOffset: $scrollViewOffset.offset) {
                VStack(spacing: -30) {
                    StickyImageHeader()
                        .zIndex(0)

                    VStack(spacing: 25) {
                        HeaderTitle(text: L10n.Signup.title)

                        UploadPictureView(image: $viewModel.signupModel.image,
                                          showPicker: $viewModel.showImagePickerAlert)

                        InputRowView(title: L10n.Signup.yourFirstName,
                                     placeholder: L10n.Signup.fullFirstName,
                                     maxLength: 15,
                                     errorText: $viewModel.validationModel.firstNameError,
                                     value: $viewModel.signupModel.firstName)

                        InputRowView(title: L10n.Signup.yourLastName,
                                     placeholder: L10n.Signup.fullLastName,
                                     maxLength: 15,
                                     errorText: $viewModel.validationModel.lastNameError,
                                     value: $viewModel.signupModel.lastName)

                        InputRowView(title: L10n.Signup.email,
                                     placeholder: L10n.Signup.yourEmail,
                                     keyboardType: .emailAddress,
                                     errorText: $viewModel.validationModel.emailError,
                                     value: $viewModel.signupModel.email)

                        InputRowView(title: L10n.Signup.phoneNumber,
                                     placeholder: L10n.Signup.yourPhoneNumber,
                                     subtitle: L10n.Signup.supportOnly,
                                     keyboardType: .numberPad,
                                     errorText: $viewModel.validationModel.phoneNumberError,
                                     value: $viewModel.signupModel.phoneNumber)

                        SecuredInputRowView(title: L10n.Signup.password,
                                            placeholder: L10n.Signup.password,
                                            errorText: $viewModel.validationModel.passwordError,
                                            value: $viewModel.signupModel.password,
                                            hideValue: $viewModel.hidePassword)

                        SecuredInputRowView(title: L10n.Signup.confirmPassword,
                                            placeholder: L10n.Signup.password,
                                            errorText: $viewModel.validationModel.confirmedPasswordError,
                                            value: $viewModel.signupModel.confirmedPassword,
                                            hideValue: $viewModel.hideRepeatedPassword)

                        SignupButton(action: {
                            haptic(with: .medium)
                            viewModel.signup {
                                navigateToMainFlow()
                            }
                        })

                        SignupDescriptions(viewModel: viewModel)
                            .environmentObject(authState)
                    }
                    .padding(EdgeInsets(top: 30, leading: 30, bottom: 15, trailing: 30))
                    .frame(width: screenWidth, alignment: .bottom)
                    .background(Color(.white))
                    .cornerRadius(40)
                    .zIndex(2)
                }
                .onTapGesture {
                    hideKeyboard()
                }
                .edgesIgnoringSafeArea(.top)
            }
            .alert(isPresented: $viewModel.hasError) {
                Alert(
                    title: Text(viewModel.errorTitle),
                    message: Text(viewModel.errorText),
                    dismissButton: .default(Text(L10n.Login.tryAgain))
                )
            }
            .actionSheet(isPresented: $viewModel.showImagePickerAlert) { () -> ActionSheet in
                ActionSheet(title: Text(L10n.Signup.mode),
                            message: Text(L10n.Signup.chooseMode),
                            buttons: [
                                ActionSheet.Button.default(Text(L10n.Signup.camera), action: {
                        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                            showCamera()
                        } else {
                            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                                if granted {
                                    showCamera()
                                } else {
                                    viewModel.setCameraError()
                                }
                            })
                        }

                    }),
                    ActionSheet.Button.default(Text(L10n.Signup.photoLibrary), action: {
                        self.viewModel.showImagePickerSheet = true
                        self.useCameraAsPicker = false
                    }),
                    ActionSheet.Button.cancel()])
            }
            .sheet(isPresented: $viewModel.showImagePickerSheet) {
                SUImagePickerView(sourceType: .photoLibrary,
                                  image: $viewModel.signupModel.image,
                                  isPresented: $viewModel.showImagePickerSheet)
            }
            .fullScreenCover(isPresented: $useCameraAsPicker) {
                SUImagePickerView(sourceType: .camera,
                                  image: $viewModel.signupModel.image,
                                  isPresented: $useCameraAsPicker)
                    .ignoresSafeArea()
            }
            .navigationBarHidden(true)
            .navigationTitle("")
            .onAppear(perform: {
                viewModel.clearInputs()
                // Hack to prevent animations for views that appears faster than current screen
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.isViewAppeared = true
                }
            })

            ActivityLoader().opacity(viewModel.isBusy ? 1 : 0)
                .zIndex(3)
        }.modifier(NoNavbarModifier())
    }

    private func navigateToMainFlow() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder),
                                        to: nil, from: nil, for: nil)
        appState.isAuthorized = true
        // Move to initial screen after app state switching
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            presentationMode.wrappedValue.dismiss()
        }
    }

    private func showCamera() {
        #if targetEnvironment(simulator)
            return
        #else
        viewModel.showImagePickerSheet = false
        useCameraAsPicker = true
        #endif
    }
}

private extension SignupView {
    var mainRowsSpacing: CGFloat {
        switch screenType {
        case .small:
            return 16
        case .medium:
            return 30
        case .large:
            return 35
        }
    }
}
