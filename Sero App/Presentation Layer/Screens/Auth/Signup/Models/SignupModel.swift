//
//  SignupModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 15.11.21.
//

import UIKit

struct SignupModel {
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var phoneNumber: String  = ""
    var password: String  = ""
    var confirmedPassword: String  = ""
    var image: UIImage?
}
