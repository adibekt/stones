//
//  SignupValidationModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

struct SignupValidationModel {
    var firstNameError: String = ""
    var lastNameError: String = ""
    var emailError: String = ""
    var phoneNumberError: String  = ""
    var passwordError: String  = ""
    var confirmedPasswordError: String  = ""
}
