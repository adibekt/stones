//
//  LinkedAccountModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 18.11.21.
//

import UIKit

struct LinkedAccountModel {
    var id: String
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var imageUrl: URL?
    var provider: AuthorizationType
}
