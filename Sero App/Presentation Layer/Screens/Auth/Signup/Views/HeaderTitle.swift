//
//  HeaderTitle.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct HeaderTitle: View {
    let text: String
    var body: some View {
        HStack {
            Text(text)
                .font(.custom(FontFamily.OrpheusPro.regular.name, size: 36))
                .foregroundColor(Color(Asset.Colors.darkGreen.name))
            Spacer()
        }
    }
}
