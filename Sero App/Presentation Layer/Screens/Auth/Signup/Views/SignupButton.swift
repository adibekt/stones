//
//  SignupButton.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct SignupButton: View {
    let action: VoidClosure

    var body: some View {
        Button(action: {
            action()
        }, label: {
            Text(L10n.Signup.titleSeparated)
                .font(.custom(FontFamily.ProximaNova.regular.name, size: 18))
                .foregroundColor(.white)
                .frame(width: UIScreen.main.bounds.width * 0.7,
                       height: 55,
                       alignment: .center)
                .background(Color(Asset.Colors.darkGreen.name))
                .cornerRadius(33)
        })
    }
}
