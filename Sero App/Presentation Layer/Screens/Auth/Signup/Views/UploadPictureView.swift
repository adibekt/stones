//
//  UploadPictureView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct UploadPictureView: View {
    @Binding var image: UIImage?
    @Binding var showPicker: Bool

    var pickerSize: CGFloat {
        return isSmallScreen ? 70.0 : 100.0
    }

    var body: some View {
        HStack(spacing: 10) {
            if let image = image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: pickerSize, height: pickerSize, alignment: .center)
                    .clipShape(Circle())
                    .onTapGesture {
                        showPicker = true
                        haptic(with: .medium)
                    }
            } else {
                Circle()
                    .foregroundColor(Color(Asset.Colors.backgroundGray.name))
                    .frame(width: pickerSize, height: pickerSize, alignment: .center)
                    .overlay(Image(systemName: SFSymbols.cameraFill.rawValue)
                                .resizable()
                                .foregroundColor(Color(Asset.Colors.darkGreen.name))
                                .frame(width: 30, height: 24, alignment: .center))
                    .onTapGesture {
                        showPicker = true
                        haptic(with: .medium)
                    }
            }

            VStack(alignment: .leading, spacing: 5) {
                Text(L10n.Signup.uploadPicture)
                    .font(.custom(FontFamily.SFProDisplay.bold.name, size: 18))
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                Text(L10n.Signup.clientsFindYou)
                    .font(.custom(FontFamily.SFProDisplay.bold.name, size: 18))
                    .foregroundColor(Color(Asset.Colors.textGray.name))
            }

            Spacer()
        }
    }
}
