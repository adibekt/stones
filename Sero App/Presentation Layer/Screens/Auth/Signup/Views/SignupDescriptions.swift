//
//  SignupDescriptions.swift
//  Sero App
//
//  Created by Roman Nekliukov on 17.11.21.
//

import SwiftUI

struct SignupDescriptions: View {
    @StateObject var viewModel: SignupViewModel
    @EnvironmentObject var authState: AuthState

    var body: some View {
        HStack(spacing: 3) {
            Text(L10n.Signup.haveAccount)
            Text(L10n.Signup.login)
                .underline()
                .onTapGesture {
                    withAnimation(.easeIn(duration: 0.25)) {
                        authState.isLogin.toggle()
                    }
                }
        }
        .foregroundColor(Color(Asset.Colors.darkGreen.name))
        .font(.custom(FontFamily.ProximaNova.regular.name, size: 16))
    }
}
