//
//  SignupViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 15.11.21.
//

import Foundation
import SwiftUI

final class SignupViewModel: ObservableObject {
    let maxPasswordLength = 6
    let phoneNumberLength = 10

    @Published var signupModel = SignupModel()
    @Published var validationModel = SignupValidationModel()
    @Published var hidePassword = true
    @Published var hideRepeatedPassword = true
    @Published var isBusy = false
    @Published var navigateToIntro = false
    @Published var hasError = false
    @Published var errorText = ""
    @Published var errorTitle = ""
    @Published var showImagePickerAlert = false
    @Published var showImagePickerSheet = false

    let firebaseAuthenticationService: FirebaseAuthenticationService

    var router: MainRouter

    init(router: MainRouter,
         firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.router = router
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func signup(completed: @escaping VoidClosure) {
        isBusy = true
        resetValidationValues()

        let validationSucceeded = validateInputs(data: signupModel)
        if !validationSucceeded {
            isBusy = false
            return
        }

        let successClosure = {
            self.setAuthorizationState(authorized: true, type: .email)
            completed()
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.errorText = text.isBlank ? L10n.Signup.Error.unableToSignup : text
            self?.hasError = true
            self?.errorTitle = L10n.Signup.Error.registrationFailed
            self?.isBusy = false
        }

        signupModel.email = signupModel.email.trimmingCharacters(in: .whitespacesAndNewlines)
        signupModel.phoneNumber = signupModel.phoneNumber.trimmingCharacters(in: .whitespacesAndNewlines)

        firebaseAuthenticationService.manualSignup(with: signupModel) { result in
            switch result {
            case .success:
                successClosure()
            case .failure(let error):
                failClosure(error.localizedDescription)
            }
        }
    }

    func clearInputs() {
        signupModel = SignupModel()
        isBusy = false
        hasError = false
        hideRepeatedPassword = true
        hidePassword = true
    }

    func setCameraError() {
        hasError = true
        errorText = L10n.Signup.Error.cameraAccess
        errorTitle = L10n.Signup.Error.permissions
    }

    private func validateInputs(data: SignupModel) -> Bool {
        // Clear previous errors
        validationModel = SignupValidationModel()

        if data.firstName.isBlank {
            validationModel.firstNameError = L10n.Signup.Error.notEmpty
        }

        if data.lastName.isBlank {
            validationModel.lastNameError = L10n.Signup.Error.notEmpty
        }

        if !data.email.isEmail {
            validationModel.emailError = L10n.Signup.Error.emailFormat
        }
        if data.email.isBlank {
            validationModel.emailError = L10n.Signup.Error.notEmpty
        }

        if data.phoneNumber.count != phoneNumberLength {
            validationModel.phoneNumberError = L10n.Signup.Error.phoneLength
        }
        if !data.phoneNumber.allSatisfy({ $0.isNumber }) {
            validationModel.phoneNumberError = L10n.Signup.Error.phoneFormat
        }
        if data.phoneNumber.isBlank {
            validationModel.phoneNumberError = L10n.Signup.Error.notEmpty
        }

        if data.password.count < maxPasswordLength {
            validationModel.passwordError = L10n.Signup.Error.passwordLength
        }
        if data.password.isBlank {
            validationModel.passwordError = L10n.Signup.Error.notEmpty
        }

        if data.password != data.confirmedPassword {
            validationModel.confirmedPasswordError = L10n.Signup.Error.passwordNotEqual
        }

        return validationModel.emailError.isBlank && validationModel.firstNameError.isBlank &&
               validationModel.lastNameError.isBlank && validationModel.emailError.isBlank &&
               validationModel.confirmedPasswordError.isBlank && validationModel.passwordError.isBlank &&
               validationModel.phoneNumberError.isBlank
    }

    private func setAuthorizationState(authorized: Bool, type: AuthorizationType) {
        AppSettings.authorizationType = type
        navigateToIntro = authorized
        hasError = false
        errorText = L10n.Signup.Error.unableToSignup
        isBusy = false
    }

    private func resetValidationValues() {
        errorText = ""
        hasError = false
        validationModel = SignupValidationModel()
    }
}
