//
//  IntroductionView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 9.11.21.
//

import SwiftUI

struct IntroductionView: View {
    @StateObject var viewModel: IntroductionViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        NavigationView {
            ZStack(alignment: .center) {
                Color(Asset.Colors.darkGreen.name)

                NavigationLink(destination: viewModel.router.toRequirementsView()
                                                .modifier(NoNavbarModifier()),
                               isActive: $viewModel.navigateToRequirements) { EmptyView() }
                                .isDetailLink(false)

                VStack {
                    VStack(spacing: 25) {
                        Image(Asset.Images.logo.name)
                            .resizable()
                            .frame(width: 65, height: 65, alignment: .center)
                            .foregroundColor(.white)

                        if viewModel.nextTitleShown {
                            Text(L10n.Introduction.exhale)
                                .modifier(IntroTitleModifier(titleVisible: $viewModel.titleVisible))
                        } else {
                            Text(L10n.Introduction.inhale)
                                .modifier(IntroTitleModifier(titleVisible: $viewModel.titleVisible))
                        }
                    }
                    .offset(x: 0, y: -45)
                    .frame(alignment: .top)
                }
            }.ignoresSafeArea()
        }.modifier(NoNavbarModifier())
    }
}

struct HeaderLogoIntroView: View {
    @State var title: String
    let color: Color

    var body: some View {
        VStack(spacing: 15) {
            Image(Asset.Images.logo.name)
                .frame(width: 70, height: 70, alignment: .center)
                .foregroundColor(color)
            Text(title)
                .font(.custom(FontFamily.OrpheusPro.regular.name, size: 42))
                .foregroundColor(color)
        }
    }
}

struct IntroductionView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = IntroductionViewModel(router: MainRouter(factory: MainFactory()))
        IntroductionView(viewModel: viewModel)
            .previewDevice("iPhone 8")
        IntroductionView(viewModel: viewModel)
            .previewDevice("iPhone 12 Pro Max")
    }
}
