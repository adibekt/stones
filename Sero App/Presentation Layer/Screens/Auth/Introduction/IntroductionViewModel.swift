//
//  IntroductionViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 9.11.21.
//
import SwiftUI

final class IntroductionViewModel: ObservableObject {
    @Published public var titleVisible = true
    @Published public var signOutVisible = false
    @Published public var nextTitleShown = false
    @Published public var navigateToAuth = false
    @Published public var navigateToRequirements = false

    var router: MainRouter

    init(router: MainRouter) {
        self.router = router
        self.animateIntroduction()
    }

    func animateIntroduction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            withAnimation(.linear(duration: 1)) {
                self.titleVisible.toggle()
                self.nextTitleShown.toggle()
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            withAnimation(.linear(duration: 1)) {
                self.titleVisible.toggle()
                self.signOutVisible.toggle()
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            self.navigateToRequirements.toggle()
        }
    }
}
