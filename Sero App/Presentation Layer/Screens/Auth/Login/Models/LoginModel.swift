//
//  LoginModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 15.11.21.
//

import Foundation

final class LoginModel {
    var email: String = ""
    var password: String = ""
}
