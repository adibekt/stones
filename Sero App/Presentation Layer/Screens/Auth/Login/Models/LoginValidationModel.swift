//
//  LoginValidationModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.11.21.
//

import Foundation

struct LoginValidationModel {
    var emailError: String = ""
    var passwordError: String = ""
}
