//
//  LoginViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 13.11.21.
//

import Foundation
import AuthenticationServices
import Firebase
import SwiftUI

final class LoginViewModel: ObservableObject {
    @Published var loginModel = LoginModel()
    @Published var validationModel = LoginValidationModel()
    @Published var hidePassword = true
    @Published var isBusy = false
    @Published var navigateToForgotPassword = false
    @Published var hasError = false
    @Published var errorText = ""

    let appleAuthManager: AppleAuthorizationManager
    let googleAuthManager: GoogleAuthorizationManager
    let firebaseAuthenticationService: FirebaseAuthenticationService

    var router: MainRouter

    init(router: MainRouter,
         appleAuthManager: AppleAuthorizationManager,
         googleAuthManager: GoogleAuthorizationManager,
         firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.router = router
        self.appleAuthManager = appleAuthManager
        self.googleAuthManager = googleAuthManager
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func login(completed: @escaping VoidClosure) {
        isBusy = true
        resetValidationValues()
        print("Email = \(loginModel.email). Password: \(loginModel.password)")

        let email = loginModel.email
        let password = loginModel.password
        if email.isBlank || password.isBlank {
            isBusy = false
            validationModel.emailError = email.isBlank ? L10n.Signup.Error.notEmpty : ""
            validationModel.passwordError = password.isBlank ? L10n.Signup.Error.notEmpty : ""
            return
        }

        let successClosure = { [weak self] in
            self?.setAuthorizationState(authorized: true, type: .email)
            completed()
        }
        let failClosure: StringClosure = { [weak self] text in
            self?.errorText = text.isBlank ? L10n.Login.unableToSignin : text
            self?.hasError = true
            self?.isBusy = false
        }

        loginModel.email = loginModel.email.trimmingCharacters(in: .whitespacesAndNewlines)

        firebaseAuthenticationService.manualSignIn(withCredentials: loginModel) { result in
            switch result {
            case .success:
                successClosure()
            case .failure(let error):
                failClosure(error.localizedDescription)
            }
        }
    }

    func loginWithGoogle(completed: @escaping VoidClosure) {
        isBusy = true
        let successClosure = {
            self.setAuthorizationState(authorized: true, type: .google)
            completed()
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.isBusy = false
            self?.hasError = !text.isBlank
            self?.errorText = text
        }

        googleAuthManager.signIn { result in
            switch result {
            case .success:
                successClosure()
            case .failure(let error):
                failClosure(error.localizedDescription)
            }
        }
    }

    func loginWithApple(completed: @escaping VoidClosure) {
        isBusy = true
        let successClosure = {
            self.setAuthorizationState(authorized: true, type: .apple)
            completed()
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.isBusy = false
            self?.hasError = !text.isBlank
            self?.errorText = text
        }

        appleAuthManager.authFailedClosure = failClosure
        appleAuthManager.authCompletedClosure = successClosure
        appleAuthManager.performAppleSignIn()
    }

    func clearInputs() {
        loginModel = LoginModel()
        errorText = ""
    }

    private func setAuthorizationState(authorized: Bool, type: AuthorizationType) {
        AppSettings.authorizationType = type
        hasError = false
        errorText = ""
        isBusy = false
    }

    private func resetValidationValues() {
        errorText = ""
        hasError = false
        validationModel = LoginValidationModel()
    }
}
