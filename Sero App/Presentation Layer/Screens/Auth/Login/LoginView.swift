//
//  LoginView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 13.11.21.
//

import SwiftUI
import UIKit

struct LoginView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var authState: AuthState
    @EnvironmentObject var appState: AppState
    @StateObject var viewModel: LoginViewModel

    var body: some View {
        GeometryReader { reader in

            NavigationLink(destination: viewModel.router.toForgotPasswordScreen(),
                           isActive: $viewModel.navigateToForgotPassword) { }

            ZStack {
                Color(Asset.Colors.backgroundGray.name)

                VStack {
                    Image(Asset.Images.signinBackground.name)
                        .resizable()
                        .frame(width: reader.size.width,
                               height: reader.size.height * 0.35)
                        .scaledToFill()
                    Spacer()
                }

                VStack {
                    BackButton(action: { presentationMode.wrappedValue.dismiss() })
                        .padding(.top, isSmallScreen ? 25 : 50)
                        .zIndex(2)

                    Spacer()

                    VStack(spacing: mainRowsSpacing) {
                        HeaderTitle(text: L10n.Login.title)

                        VStack(spacing: inputsSpacing) {
                            InputRowView(title: L10n.Login.email,
                                         placeholder: L10n.Login.yourEmail,
                                         errorText: $viewModel.validationModel.emailError,
                                         value: $viewModel.loginModel.email)

                            VStack(spacing: 0) {
                                SecuredInputRowView(title: L10n.Login.password,
                                                    placeholder: L10n.Login.password,
                                                    errorText: $viewModel.validationModel.passwordError,
                                                    value: $viewModel.loginModel.password,
                                                    hideValue: $viewModel.hidePassword)
                                HStack {
                                    Spacer()
                                    Text(L10n.Login.forgotPassword)
                                        .font(.custom(FontFamily.ProximaNova.regular.name, size: 18))
                                        .foregroundColor(Color(Asset.Colors.textGray.name)).onTapGesture(perform: {
                                                haptic(with: .medium)
                                                viewModel.navigateToForgotPassword = true
                                        })
                                }
                            }

                        }

                        VStack(spacing: mainRowsSpacing * 0.66) {
                            SeroButton(text: L10n.Login.signIn, action: {
                                haptic(with: .medium)
                                viewModel.login {
                                    setAuthorizedAppState()
                                }
                            })

                            HStack(spacing: 3) {
                                Text(L10n.Login.noAccount)
                                Text(L10n.Login.signUp)
                                    .underline()
                                    .onTapGesture {
                                        haptic(with: .medium)
                                        withAnimation(.easeIn(duration: 0.25)) {
                                            authState.isLogin.toggle()
                                        }
                                    }
                            }
                            .foregroundColor(Color(Asset.Colors.darkGreen.name))
                            .font(.custom(FontFamily.ProximaNova.regular.name, size: 16))
                        }

                        LabelledDivider(label: L10n.Login.alternativeLogin,
                                        horizontalPadding: 0,
                                        color: Color(Asset.Colors.darkGreen.name))

                        HStack(spacing: loginButtonSize) {
                            LoginRoundedButton(
                                action: {
                                    haptic(with: .medium)
                                    viewModel.loginWithGoogle {
                                        setAuthorizedAppState()
                                    }
                                },
                                type: .google,
                                backgroundColor: .white,
                                buttonSize: loginButtonSize)

                            LoginRoundedButton(
                                action: {
                                    haptic(with: .medium)
                                    viewModel.loginWithApple {
                                        setAuthorizedAppState()
                                    }
                                },
                                type: .apple,
                                backgroundColor: .white,
                                buttonSize: loginButtonSize,
                                imageColor: Color.black)
                        }
                        .frame(width: reader.size.width * 0.5, height: loginButtonSize, alignment: .center)

                        Spacer()
                    }
                    .padding(panelPadding)
                    .frame(width: reader.size.width,
                           height: reader.size.height * 0.7,
                           alignment: .top)
                    .background(Color(.white))
                    .cornerRadius(40)

                }
            }

            ActivityLoader().opacity(viewModel.isBusy ? 1 : 0)
        }
        .alert(isPresented: $viewModel.hasError) {
            Alert(title: Text(L10n.Login.invalidCredentials),
                  message: Text(viewModel.errorText),
                  dismissButton: .default(Text(L10n.Login.tryAgain)))
        }
        .onAppear(perform: {
            viewModel.clearInputs()
        })
        .modifier(NoNavbarModifier())
        .ignoresSafeArea()
    }

    private func setAuthorizedAppState() {
        appState.isAuthorized = true
        // Move to initial screen after app state switching
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            presentationMode.wrappedValue.dismiss()
        }
    }
}

struct LoginRoundedButton: View {
    let action: (() -> Void)
    let type: AuthorizationType
    let backgroundColor: Color
    let buttonSize: CGFloat
    var imageColor: Color = .clear

    var body: some View {
        Button {
            action()
        } label: {
            Image(type.imageName)
                .resizable()
                .scaledToFit()
                .frame(width: 30, height: 30, alignment: .center)
                .if(imageColor != .clear) { content in
                    content.foregroundColor(imageColor)
                }
                .background(
                    Circle()
                        .frame(width: buttonSize, height: buttonSize, alignment: .center)
                        .foregroundColor(backgroundColor)
                        .shadow(color: Color(Asset.Colors.lightGray.name)
                                        .opacity(0.4),
                                radius: 3, x: 0, y: 3)
                )
        }
    }
}

struct LabelledDivider: View {
    let label: String
    let horizontalPadding: CGFloat
    let color: Color

    init(label: String, horizontalPadding: CGFloat = 20, color: Color = .gray) {
        self.label = label
        self.horizontalPadding = horizontalPadding
        self.color = color
    }

    var body: some View {
        HStack {
            line
            Text(label).foregroundColor(color)
                .font(.custom(FontFamily.ProximaNova.regular.name, size: 16))
            line
        }
    }

    var line: some View {
        VStack {
            Divider().background(color)
        }.padding(horizontalPadding)
    }
}

private extension LoginView {
    var mainRowsSpacing: CGFloat {
        switch screenType {
        case .small:
            return 16
        case .medium:
            return screenWidth * 0.06
        case .large:
            return 35
        }
    }

    var panelPadding: EdgeInsets {
        return isSmallScreen
            ? EdgeInsets(top: 15, leading: 30, bottom: 0, trailing: 30 )
            : EdgeInsets(top: 35, leading: 30, bottom: 10, trailing: 30 )
    }

    var inputsSpacing: CGFloat {
        return isSmallScreen ? 3 : 10
    }

    var loginButtonSize: CGFloat {
        switch screenType {
        case .small:
            return 50
        case .medium:
            return 70
        case .large:
            return 85
        }
    }
}
