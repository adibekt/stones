//
//  ForgotPasswordViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.11.21.
//

import Foundation
import SwiftUI

final class ForgotPasswordViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var emailErrorText: String = ""
    @Published var hasError: Bool = false
    @Published var errorText: String = ""
    @Published var isBusy: Bool = false
    @Published var messageSent: Bool = false

    let firebaseAuthenticationService: FirebaseAuthenticationService

    init(firebaseAuthenticationService: FirebaseAuthenticationService) {
        self.firebaseAuthenticationService = firebaseAuthenticationService
    }

    func resetPassword() {
        isBusy = true
        emailErrorText = ""

        if email.isBlank {
            emailErrorText = L10n.Signup.Error.notEmpty
        } else if !email.isEmail {
            emailErrorText = L10n.Signup.Error.emailFormat
        }

        if !emailErrorText.isBlank {
            isBusy = false
            return
        }

        let successClosure = { [weak self] in
            self?.isBusy = false
            withAnimation(.easeOut(duration: 0.3)) {
                self?.messageSent = true
            }
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.isBusy = false
            self?.errorText = text
            self?.hasError = true
        }

        email = email.trimmingCharacters(in: .whitespacesAndNewlines)

        firebaseAuthenticationService.resetPassword(email: email) { result in
            switch result {
            case .success:
                successClosure()
            case .failure(let error):
                failClosure(error.localizedDescription)
            }
        }
    }
}
