//
//  ForgotPasswordView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 21.11.21.
//

import SwiftUI

struct ForgotPasswordView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @StateObject var viewModel: ForgotPasswordViewModel

    @ViewBuilder var body: some View {
        ZStack {
            BackButton(action: { presentationMode.wrappedValue.dismiss() })
                .zIndex(2)

            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: -30) {
                    StickyImageHeader()
                        .zIndex(0)

                    VStack(spacing: 28) {
                        HeaderTitle(text: L10n.ForgotPassword.title)

                        if !viewModel.messageSent {
                                InputRowView(title: L10n.ForgotPassword.email,
                                             placeholder: L10n.ForgotPassword.yourEmail,
                                             errorText: $viewModel.emailErrorText,
                                             value: $viewModel.email)

                                SeroButton(
                                    text: L10n.ForgotPassword.submit,
                                    action: {
                                        viewModel.resetPassword()
                                    }
                                )

                                SeroLink(text: L10n.ForgotPassword.backToLogin,
                                         action: { presentationMode.wrappedValue.dismiss() })
                        } else {
                            VStack(spacing: 28) {
                                VStack(spacing: 6) {
                                    Text(L10n.ForgotPassword.sent)
                                        .font(.custom(FontFamily.SFProDisplay.semibold.name, size: 22))
                                        .foregroundColor(Color(Asset.Colors.darkGreen.name))
                                    Text(L10n.ForgotPassword.checkEmail)
                                        .font(.custom(FontFamily.SFProDisplay.bold.name, size: 16))
                                        .foregroundColor(Color(Asset.Colors.darkGreen.name))
                                }

                                SeroButton(text: L10n.ForgotPassword.backToLogin,
                                           action: { presentationMode.wrappedValue.dismiss() })
                            }
                            .transition(.slide)
                        }
                    }
                    .padding(EdgeInsets(top: 30, leading: 30, bottom: 15, trailing: 30))
                    .frame(width: screenWidth, alignment: .bottom)
                    .background(Color(.white))
                    .cornerRadius(40)
                    .zIndex(2)

                }
                .edgesIgnoringSafeArea(.top)
            }
            .alert(isPresented: $viewModel.hasError) {
                Alert(
                    title: Text(L10n.ForgotPassword.resetFailed),
                    message: Text(viewModel.errorText),
                    dismissButton: .default(Text(L10n.Login.tryAgain))
                )
            }
            .modifier(NoNavbarModifier())

            ActivityLoader().opacity(viewModel.isBusy ? 1 : 0)
                .zIndex(3)
        }
    }
}

struct SeroButton: View {
    let text: String
    let action: VoidClosure

    var body: some View {
        Button(action: { action() }, label: {
            Text(text)
                .font(.custom(FontFamily.ProximaNova.regular.name, size: 18))
                .foregroundColor(.white)
                .frame(width: UIScreen.main.bounds.width * 0.7,
                       height: 55,
                       alignment: .center)
                .background(Color(Asset.Colors.darkGreen.name))
                .cornerRadius(33)
        })
    }
}

struct SeroLink: View {
    let text: String
    let action: VoidClosure

    var body: some View {
        Button(action: { action() }, label: {
            Text(text)
                .font(.custom(FontFamily.ProximaNova.regular.name, size: 18))
                .foregroundColor(Color(Asset.Colors.darkGreen.name))
        })
    }
}
