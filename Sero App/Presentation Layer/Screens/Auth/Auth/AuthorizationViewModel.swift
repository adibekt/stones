//
//  AuthorizationViewModel.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//
import SwiftUI
import Firebase
import AuthenticationServices

final class AuthorizationViewModel: ObservableObject {
    @Published var navigateToLogin = false
    @Published var navigateToSignup = false
    @Published var navigateToAuth = false
    @Published var hasError = false
    @Published var errorText = ""
    @Published var currentNonce: String?
    @Published var isBusy = false

    let appleAuthManager: AppleAuthorizationManager
    let googleAuthManager: GoogleAuthorizationManager

    var router: MainRouter

    init(router: MainRouter,
         appleAuthManager: AppleAuthorizationManager,
         googleAuthManager: GoogleAuthorizationManager) {
        self.router = router
        self.appleAuthManager = appleAuthManager
        self.googleAuthManager = googleAuthManager
    }

    func signUpWithEmail() {
        navigateToSignup = true
    }

    func continueWithGoogle(completed: @escaping VoidClosure) {
        isBusy = true
        let successClosure = {
            self.setAuthorizationState(authorized: true, type: .google)
            completed()
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.isBusy = false
            self?.hasError = !text.isBlank
            self?.errorText = text
        }

        googleAuthManager.signIn { result in
            switch result {
            case .success:
                successClosure()
            case .failure(let error):
                failClosure(error.localizedDescription)
            }
        }
    }

    func continueWithApple(completed: @escaping VoidClosure) {
        isBusy = true
        let successClosure = {
            self.setAuthorizationState(authorized: true, type: .apple)
            completed()
        }

        let failClosure: StringClosure = { [weak self] text in
            self?.isBusy = false
            self?.hasError = !text.isBlank
            self?.errorText = text
        }

        appleAuthManager.authFailedClosure = failClosure
        appleAuthManager.authCompletedClosure = successClosure
        appleAuthManager.performAppleSignIn()
    }

    private func setAuthorizationState(authorized: Bool, type: AuthorizationType) {
        AppSettings.authorizationType = type
        isBusy = false
        hasError = false
    }
}
