//
//  AuthTabView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 20.11.21.
//

import SwiftUI

struct AuthTabView: View {
    @StateObject var authState = AuthState()
    @EnvironmentObject var appState: AppState

    let router: MainRouter
    let isLogin: Bool

    var body: some View {
        VStack {
            if authState.isLogin {
                router.toLoginScreen()
                    .transition(AnyTransition.asymmetric(
                                    insertion: .move(edge: isLogin ? .leading : .trailing),
                                    removal: .move(edge: isLogin ? .trailing : .leading))
                                )
            } else {
                router.toSignupScreen()
                    .transition(AnyTransition.asymmetric(
                                    insertion: .move(edge: isLogin ? .trailing : .leading),
                                    removal: .move(edge: isLogin ? .leading : .trailing))
                                )
            }
        }
        .modifier(NoNavbarModifier())
        .environmentObject(authState)
        .onAppear(perform: {
            authState.isLogin = isLogin
        })
    }
}

class AuthState: ObservableObject {
    @Published var isLogin: Bool = true
}
