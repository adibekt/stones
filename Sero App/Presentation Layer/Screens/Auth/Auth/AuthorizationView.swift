//
//  AuthorizationView.swift
//  Sero App
//
//  Created by Roman Nekliukov on 4.11.21.
//

import SwiftUI

struct AuthorizationView: View {
    @StateObject var authViewModel: AuthorizationViewModel
    @EnvironmentObject var appState: AppState

    var body: some View {
        NavigationView {
            GeometryReader { reader in
                ZStack {
                    MainBackgroundGradient()
                        .ignoresSafeArea()

                    NavigationLink(destination: authViewModel.router.toAuthTabView(isLogin: false),
                                   isActive: $authViewModel.navigateToSignup) {
                    }

                    VStack(alignment: .center, spacing: 40) {
                        HeaderLogoView(logoImageSize: isLargeScreen ? 55 : 45)

                        Image(Asset.Images.stones.name)
                            .resizable()
                            .frame(width: isLargeScreen ? 320 : 270,
                                   height: isLargeScreen ? 200 : 175, alignment: .center)

                        VStack(spacing: 22) {
                            VStack(spacing: isLargeScreen ? 25 : 15) {
                                AuthorizationResourceButton(
                                    action: authViewModel.signUpWithEmail,
                                    type: .email,
                                    backgroundColor: .clear,
                                    textColor: Color(Asset.Colors.darkGreen.name),
                                    fontName: FontFamily.SFProDisplay.semibold.name)

                                AuthorizationResourceButton(
                                    action: {
                                        authViewModel.continueWithGoogle {
                                            self.appState.isAuthorized = true
                                        }
                                    },
                                    type: .google,
                                    backgroundColor: .white,
                                    textColor: Color(Asset.Colors.seroBlack.name))

                                AuthorizationResourceButton(
                                    action: {
                                        authViewModel.continueWithApple {
                                            self.appState.isAuthorized = true
                                        }
                                    },
                                    type: .apple,
                                    backgroundColor: .black,
                                    textColor: .white,
                                    imageColor: .white)
                            }

                            AuthorizationHelpersAreaView(authViewModel: authViewModel)
                        }
                    }
                    .if(screenType != .small) { content in
                        content.padding(.bottom, 40)
                    }
                    .frame(width: reader.size.width,
                           height: reader.size.height,
                           alignment: screenType == .small ? .center : .bottom)
                }

                ActivityLoader().opacity(authViewModel.isBusy ? 1 : 0)
            }
            .alert(isPresented: $authViewModel.hasError) {
                Alert(title: Text(L10n.Login.invalidCredentials),
                      message: Text(authViewModel.errorText),
                      dismissButton: .default(Text(L10n.Login.tryAgain)))
            }
            .navigationViewStyle(.stack)
            .modifier(NoNavbarModifier())
        }
    }
}

struct HeaderLogoView: View {
    let logoImageSize: Double

    var body: some View {
        VStack(spacing: 15) {
            Image(Asset.Images.logo.name)
                .resizable()
                .frame(width: logoImageSize, height: logoImageSize, alignment: .center)
                .foregroundColor(Color(Asset.Colors.darkGreen.name))
            Text(L10n.Authorization.welcome)
                .font(.custom(FontFamily.OrpheusPro.regular.name, size: 42))
                .foregroundColor(Color(Asset.Colors.darkGreen.name))
        }
    }
}

struct AuthorizationHelpersAreaView: View {
    @StateObject var authViewModel: AuthorizationViewModel

    var body: some View {
        VStack(spacing: 22) {
            HStack(spacing: 3) {
                Text(L10n.Authorization.haveAccount)
                    .foregroundColor(Color(Asset.Colors.darkGreen.name))
                    .font(.custom(FontFamily.ProximaNova.regular.name, size: 16))

                NavigationLink(destination: authViewModel.router.toAuthTabView(isLogin: true)) {
                    Text(L10n.Authorization.logIn)
                        .foregroundColor(Color(Asset.Colors.darkGreen.name))
                        .font(.custom(FontFamily.ProximaNova.regular.name, size: 16))
                        .underline()
                }
            }

            (
                Text(L10n.Authorization.usage) +
                Text(L10n.Authorization.terms)
                    .underline()
            )
                .foregroundColor(Color(Asset.Colors.lightGray.name))
                .font(.custom(FontFamily.ProximaNova.regular.name, size: 12))
        }
    }
}

struct AuthorizationResourceButton: View {
    let action: (() -> Void)
    let type: AuthorizationType
    let backgroundColor: Color
    let textColor: Color
    var imageColor: Color = .clear
    var fontName: String?

    var body: some View {
        Button {
            haptic(with: .medium)
            action()
        } label: {
            HStack(spacing: 20) {
                Image(type.imageName)
                    .frame(width: 25, height: 25, alignment: .center)
                    .if(imageColor != .clear) { content in
                    content.foregroundColor(imageColor)
                }
                Text(type.title)
                    .font(.custom(fontName ??
                                  FontFamily.ProximaNova.regular.name, size: 18))
                    .foregroundColor(textColor)
            }
            .frame(width: 250, height: 55, alignment: .leading)
            .padding(.leading, 30)
            .background(backgroundColor)
            .cornerRadius(33)
            .if(type.isCustomType) { content in
                content.modifier(RoundedBorderButtonModifier(
                    borderColor: Color(Asset.Colors.darkGreen.name)))
            }
        }
    }
}
