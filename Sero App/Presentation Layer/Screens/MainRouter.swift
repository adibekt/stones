//
//  MainRouter.swift
//  Sero App
//
//  Created by Таир Адибек on 12.11.2021.
//

import SwiftUI

protocol MainRouterProtocol {
    func startFlow() -> AnyView
    func toRequirementsView() -> RequirementsView
    func toAuthTabView(isLogin: Bool) -> AuthTabView
    func toAuthView() -> AuthorizationView
    func toLoginScreen() -> LoginView
    func toForgotPasswordScreen() -> ForgotPasswordView
    func toLetsPairView() -> LetsPairView
    func toMainView() -> MainView
    func toSignupScreen() -> SignupView
}

class MainRouter {

    let mainFactory: MainFactory

    init(factory: MainFactory) {
        mainFactory = factory
    }

    func toRoleScreen() -> RoleView {
        return mainFactory.getRoleView(router: self)
    }

    func toRequirementsView() -> RequirementsView {
        return mainFactory.getRequirementsView(router: self)
    }

    func toAuthTabView(isLogin: Bool) -> AuthTabView {
        return mainFactory.getAuthTabView(router: self, isLogin: isLogin)
    }

    func toAuthView() -> AuthorizationView {
        return mainFactory.getAuthView(router: self)
    }

    func toLoginScreen() -> LoginView {
        return mainFactory.getLoginView(router: self)
    }

    func toForgotPasswordScreen() -> ForgotPasswordView {
        return mainFactory.getForgotPasswordView(router: self)
    }

    func toTherapistOptionScreen() -> TherapistOptionView {
        return mainFactory.getTherapistOptionView(router: self)
    }

    func toSuccessfullPairScreen() -> SuccessfulPairView {
        return mainFactory.getSuccessfulPairView(router: self)
    }

    func toClientDashboardScreen(therapistName: String) -> ClientDashboardView {
        return mainFactory.getClientDashboardView(router: self, therapistName: therapistName)
    }

    func toLetsPairView() -> LetsPairView {
        return mainFactory.getLetsPairView(router: self)
    }

    func toRoleMainView(role: UserRole,
                        with connectionResult: ConnectionResult? = nil) -> AnyView {
        return mainFactory.getRoleMainView(router: self, role: role, with: connectionResult)
    }

    func toConnectToClientView() -> ConnectToClientView {
        return mainFactory.getConnectToClientView(router: self)
    }

    func toConnectToTherapistView() -> ConnectToTherapistView {
        return mainFactory.getConnectToTherapistView(router: self)
    }

    func toSignupScreen() -> SignupView {
        return mainFactory.getSignupView(router: self)
    }
}
